<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'App\Controller\UserController' shared autowired service.

include_once $this->targetDirs[3].'/src/Controller/UserController.php';

$this->services['App\\Controller\\UserController'] = $instance = new \App\Controller\UserController();

$instance->setContainer(($this->privates['.service_locator.KTVqfp6'] ?? $this->get_ServiceLocator_KTVqfp6Service())->withContext('App\\Controller\\UserController', $this));

return $instance;
