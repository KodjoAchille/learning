<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/register' => [[['_route' => 'register', '_controller' => 'App\\Controller\\AccountController::register'], null, null, null, false, false, null]],
            '/login' => [[['_route' => 'login', '_controller' => 'App\\Controller\\AccountController::login'], null, null, null, false, false, null]],
            '/logout' => [[['_route' => 'logout', '_controller' => 'App\\Controller\\AccountController::logout'], null, null, null, false, false, null]],
            '/admin' => [[['_route' => 'admin_index', '_controller' => 'App\\Controller\\AdminController::index'], null, null, null, true, false, null]],
            '/admin/course/validate' => [[['_route' => 'admin_course_validate_or_not', '_controller' => 'App\\Controller\\AdminController::courseValidateOrNot'], null, null, null, false, false, null]],
            '/admin/course/edit' => [[['_route' => 'admin_course_edit', '_controller' => 'App\\Controller\\AdminController::courseEdit'], null, null, null, false, false, null]],
            '/admin/course/delete' => [[['_route' => 'admin_course_delete', '_controller' => 'App\\Controller\\AdminController::courseDelete'], null, null, null, false, false, null]],
            '/admin/course/list' => [[['_route' => 'admin_course_list', '_controller' => 'App\\Controller\\AdminController::courseList'], null, null, null, false, false, null]],
            '/admin/course/list/update' => [[['_route' => 'admin_course_list_update', '_controller' => 'App\\Controller\\AdminController::courseListUpdate'], null, null, null, false, false, null]],
            '/admin/course/valide' => [[['_route' => 'admin_course_valide', '_controller' => 'App\\Controller\\AdminController::courseValide'], null, null, null, false, false, null]],
            '/admin/course/notvalide' => [[['_route' => 'admin_course_notvalide', '_controller' => 'App\\Controller\\AdminController::courseNotvalide'], null, null, null, false, false, null]],
            '/admin/user/list' => [
                [['_route' => 'admin_user_list', '_controller' => 'App\\Controller\\AdminController::userList'], null, null, null, false, false, null],
                [['_route' => 'admin_user_teacher_list', '_controller' => 'App\\Controller\\AdminController::userTeacherList'], null, null, null, false, false, null],
                [['_route' => 'admin_user_student_list', '_controller' => 'App\\Controller\\AdminController::userStudentList'], null, null, null, false, false, null],
            ],
            '/admin/room/list' => [[['_route' => 'admin_room_list', '_controller' => 'App\\Controller\\AdminController::roomList'], null, null, null, false, false, null]],
            '/admin/room/list/update' => [[['_route' => 'admin_room_list_update', '_controller' => 'App\\Controller\\AdminController::roomListUpdate'], null, null, null, false, false, null]],
            '/admin/room/edit' => [[['_route' => 'admin_room_edit', '_controller' => 'App\\Controller\\AdminController::roomEdit'], null, null, null, false, false, null]],
            '/admin/room/delete' => [[['_route' => 'admin_room_delete', '_controller' => 'App\\Controller\\AdminController::roomDelete'], null, null, null, false, false, null]],
            '/admin/user/room/list' => [[['_route' => 'admin_user_room_list', '_controller' => 'App\\Controller\\AdminController::roomUserList'], null, null, null, false, false, null]],
            '/admin/room/add' => [[['_route' => 'admin_room_add', '_controller' => 'App\\Controller\\AdminController::roomAdd'], null, null, null, false, false, null]],
            '/admin/user/validate' => [[['_route' => 'admin_user_validate_or_not', '_controller' => 'App\\Controller\\AdminController::userValidateOrNot'], null, null, null, false, false, null]],
            '/admin/user/list/update' => [[['_route' => 'admin_user_list_update', '_controller' => 'App\\Controller\\AdminController::userListUpdate'], null, null, null, false, false, null]],
            '/annexes/new' => [[['_route' => 'annexe_new', '_controller' => 'App\\Controller\\AnnexeController::new'], null, null, null, false, false, null]],
            '/courses' => [[['_route' => 'course_index', '_controller' => 'App\\Controller\\CourseController::index'], null, null, null, true, false, null]],
            '/courses/byUser' => [[['_route' => 'course_list_by_user', '_controller' => 'App\\Controller\\CourseController::listByUser'], null, null, null, false, false, null]],
            '/courses/lite' => [[['_route' => 'course_index_lite', '_controller' => 'App\\Controller\\CourseController::indexLite'], null, null, null, false, false, null]],
            '/courses/new' => [[['_route' => 'course_new', '_controller' => 'App\\Controller\\CourseController::new'], null, null, null, false, false, null]],
            '/courses/take' => [[['_route' => 'course_take', '_controller' => 'App\\Controller\\CourseController::take'], null, null, null, false, false, null]],
            '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
            '/room' => [[['_route' => 'room_index', '_controller' => 'App\\Controller\\RoomController::index'], null, null, null, true, false, null]],
            '/room/user' => [[['_route' => 'room_user_index', '_controller' => 'App\\Controller\\RoomController::roomUserIndex'], null, null, null, false, false, null]],
            '/teachers' => [[['_route' => 'teacher_index', '_controller' => 'App\\Controller\\TeacherController::index'], null, null, null, true, false, null]],
            '/teachersroom/display/name' => [[['_route' => 'teacher_room_display_name', '_controller' => 'App\\Controller\\TeacherController::roomDisplayName'], null, null, null, false, false, null]],
            '/teachers/course/delete' => [[['_route' => 'teacher_course_delete', '_controller' => 'App\\Controller\\TeacherController::courseDelete'], null, null, null, false, false, null]],
            '/teachers/course/list/update' => [[['_route' => 'teacher_course_list_update', '_controller' => 'App\\Controller\\TeacherController::courseListUpdate'], null, null, null, false, false, null]],
            '/user/new' => [[['_route' => 'user_new', '_controller' => 'App\\Controller\\UserController::new'], null, null, null, false, false, null]],
            '/user/show' => [[['_route' => 'user_show', '_controller' => 'App\\Controller\\UserController::show'], null, null, null, false, false, null]],
            '/user/edit' => [[['_route' => 'user_edit', '_controller' => 'App\\Controller\\UserController::edit'], null, null, null, false, false, null]],
            '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
            '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
            '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
            '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
            '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/a(?'
                        .'|dmin/course/([^/]++)/(?'
                            .'|editForm(*:44)'
                            .'|s(?'
                                .'|how(*:58)'
                                .'|tudents(*:72)'
                            .')'
                        .')'
                        .'|nnexes/([^/]++)/newForm(*:104)'
                    .')'
                    .'|/courses/([^/]++)/show(*:135)'
                    .'|/teachers/(?'
                        .'|course/(?'
                            .'|byRoom/([^/]++)(*:181)'
                            .'|([^/]++)/(?'
                                .'|show(*:205)'
                                .'|editForm(*:221)'
                            .')'
                        .')'
                        .'|room/([^/]++)/user/list(*:254)'
                    .')'
                    .'|/js/routing(?:\\.(js|json))?(*:290)'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:329)'
                        .'|wdt/([^/]++)(*:349)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:395)'
                                .'|router(*:409)'
                                .'|exception(?'
                                    .'|(*:429)'
                                    .'|\\.css(*:442)'
                                .')'
                            .')'
                            .'|(*:452)'
                        .')'
                    .')'
                .')/?$}sDu',
        ];
        $this->dynamicRoutes = [
            44 => [[['_route' => 'admin_course_edit_form', '_controller' => 'App\\Controller\\AdminController::courseEditForm'], ['id'], null, null, false, false, null]],
            58 => [[['_route' => 'admin_course_show', '_controller' => 'App\\Controller\\AdminController::courseShow'], ['id'], null, null, false, false, null]],
            72 => [[['_route' => 'admin_course_students', '_controller' => 'App\\Controller\\AdminController::courseStudents'], ['id'], null, null, false, false, null]],
            104 => [[['_route' => 'annexe_new_form', '_controller' => 'App\\Controller\\AnnexeController::newForm'], ['id'], null, null, false, false, null]],
            135 => [[['_route' => 'course_show', '_controller' => 'App\\Controller\\CourseController::show'], ['id'], null, null, false, false, null]],
            181 => [[['_route' => 'teacher_course_by_room', '_controller' => 'App\\Controller\\TeacherController::courseByRoom'], ['id'], null, null, false, true, null]],
            205 => [[['_route' => 'teacher_course_show', '_controller' => 'App\\Controller\\TeacherController::courseShow'], ['id'], null, null, false, false, null]],
            221 => [[['_route' => 'teacher_course_edit_form', '_controller' => 'App\\Controller\\TeacherController::courseEditForm'], ['id'], null, null, false, false, null]],
            254 => [[['_route' => 'teacher_room_user_list', '_controller' => 'App\\Controller\\TeacherController::roomUserlist'], ['id'], null, null, false, false, null]],
            290 => [[['_route' => 'fos_js_routing_js', '_controller' => 'fos_js_routing.controller::indexAction', '_format' => 'js'], ['_format'], ['GET' => 0], null, false, true, null]],
            329 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
            349 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
            395 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
            409 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
            429 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
            442 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
            452 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        ];
    }
}
