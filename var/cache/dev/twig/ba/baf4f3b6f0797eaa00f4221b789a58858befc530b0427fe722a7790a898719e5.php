<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* annexes/new.html.twig */
class __TwigTemplate_c9e28e90bdee74ea80a975cbdd1710557e5b19a953ae1eca27035d8b8b4ee3b0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'banner' => [$this, 'block_banner'],
            'header' => [$this, 'block_header'],
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base-home.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "annexes/new.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "annexes/new.html.twig"));

        $this->parent = $this->loadTemplate("base-home.html.twig", "annexes/new.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 4
        $this->displayBlock('css', $context, $blocks);
        // line 24
        echo "<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 5
        echo "\t<link rel=\"stylesheet\" id=\"myCss\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/css/dropify.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/image-uploader.css"), "html", null, true);
        echo "\">\t
\t<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/bootstrap.css"), "html", null, true);
        echo "\">\t
\t<style>
\t\t.modal {
\t\t\tdisplay:    none;
\t\t\tposition:   fixed;
\t\t\tz-index:    1000;
\t\t\ttop:        0;
\t\t\tleft:       0;
\t\t\theight:     100%;
\t\t\twidth:      100%;
\t\t\tbackground: rgba( 255, 255, 255, .8 ) 
\t\t\t\t\t\turl('";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/loader.gif"), "html", null, true);
        echo "') 
\t\t\t\t\t\t50% 50% 
\t\t\t\t\t\tno-repeat;
\t\t}
\t</style>\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 52
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 53
        echo "<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div id=\"loading-indicator\" class=\"modal\"></div>\t
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-2\">
\t\t\t</div>
\t\t\t<div class=\"col-8\">
                <h3><b> Ajouter des documents annexes</b></h3><br>
\t\t\t\t<div id=\"responseMessage\" class=\"d-none\"></div>
\t\t\t\t<br/><br/><br/>
                <span id=\"courseId\" class=\"d-none\">";
        // line 64
        echo twig_escape_filter($this->env, (isset($context["courseId"]) || array_key_exists("courseId", $context) ? $context["courseId"] : (function () { throw new RuntimeError('Variable "courseId" does not exist.', 64, $this->source); })()), "html", null, true);
        echo "</span>
                <form  id=\"addForm\" action=\"";
        // line 65
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
        echo "\"  method=\"POST\" enctype=\"multipart/form-data\">
                    <div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"title\"  placeholder=\"Titre du document\" required=\"true\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Description</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<textarea  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\"  name=\"description\" id=\"description\" placeholder=\"description du document\"></textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                    <div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"annexe\" class=\"col-sm-2 col-form-label\">Contenu</label>
                        <div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input  type=\"file\" required=\"true\" name=\"annexe\" class=\"dropify-fr\" id=\"annexe\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t<button type=\"submit\" id=\"submit\"  class=\"btn btn-primary\">Soumettre</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t
    </div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 96
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 97
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/js/image-uploader.js"), "html", null, true);
        echo "\"></script>
<!-- jQuery file upload -->
<script id=\"myScript\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/js/dropify.min.js"), "html", null, true);
        echo "\"></script>
";
        // line 101
        echo "<script>
    \$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar addForm=document.getElementById('addForm')
    var number=0;
\tvar responseMessage=document.getElementById(\"responseMessage\");
\t\$('#submit').click(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar fd=new FormData();
\t\tif(\$(\"#title\").val()){
\t\t\tif(\$(\"#annexe\").val())
\t\t\t{
\t\t\t\tfd.append('file',document.getElementById(\"annexe\").files[0]);
\t\t\t\tfd.append('title',\$('#title').val());
\t\t\t\tfd.append('description',\$('#description').val());
\t\t\t\tfd.append('id',\$('#courseId').html());
\t\t\t\tAjaxFileFunction(fd);
\t\t\t}
\t\t\telse
\t\t\t{
\t\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu\";
\t\t\t\tresponseMessage.setAttribute('class','alert alert-danger');\t\t
\t\t\t}
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le titre\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t}
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"";
        // line 172
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annexe_new");
        echo "\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
\t\t\t//console.log('the result',result);
\t\t\tswal({
\t\t\t\ttext: result.data.message,
\t\t\t\ticon: \"success\",
\t\t\t\tbuttons: \"ok\",
\t\t\t});
\t\t\t\$('#title').val('');
\t\t\t\$('#description').val('');
\t\t\t\$('#annexe').val(null);

\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
<script>
\t\$(document).ready(function(){
\t\t\$('.input-images-1').imageUploader();
\t});
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "annexes/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 172,  272 => 101,  268 => 99,  262 => 97,  252 => 96,  211 => 65,  207 => 64,  194 => 53,  184 => 52,  168 => 18,  154 => 7,  150 => 6,  145 => 5,  135 => 4,  118 => 45,  110 => 40,  92 => 24,  90 => 4,  80 => 3,  62 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base-home.html.twig\" %}
{% block banner %}{% endblock %}
{% block header %}
{% block css %}
\t<link rel=\"stylesheet\" id=\"myCss\" href=\"{{asset('admin/plugins/bower_components/dropify/dist/css/dropify.min.css')}}\">
\t<link rel=\"stylesheet\" href=\"{{asset('web/css/image-uploader.css')}}\">\t
\t<link rel=\"stylesheet\" href=\"{{asset('web/css/bootstrap.css')}}\">\t
\t<style>
\t\t.modal {
\t\t\tdisplay:    none;
\t\t\tposition:   fixed;
\t\t\tz-index:    1000;
\t\t\ttop:        0;
\t\t\tleft:       0;
\t\t\theight:     100%;
\t\t\twidth:      100%;
\t\t\tbackground: rgba( 255, 255, 255, .8 ) 
\t\t\t\t\t\turl('{{asset(\"web/images/loader.gif\")}}') 
\t\t\t\t\t\t50% 50% 
\t\t\t\t\t\tno-repeat;
\t\t}
\t</style>\t
{% endblock %}
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"{{asset('web/images/s2.png')}}\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"{{path('index')}}\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
{% endblock %}
{% block content %}
<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div id=\"loading-indicator\" class=\"modal\"></div>\t
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-2\">
\t\t\t</div>
\t\t\t<div class=\"col-8\">
                <h3><b> Ajouter des documents annexes</b></h3><br>
\t\t\t\t<div id=\"responseMessage\" class=\"d-none\"></div>
\t\t\t\t<br/><br/><br/>
                <span id=\"courseId\" class=\"d-none\">{{courseId}}</span>
                <form  id=\"addForm\" action=\"{{path('course_new')}}\"  method=\"POST\" enctype=\"multipart/form-data\">
                    <div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"title\"  placeholder=\"Titre du document\" required=\"true\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Description</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<textarea  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\"  name=\"description\" id=\"description\" placeholder=\"description du document\"></textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                    <div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"annexe\" class=\"col-sm-2 col-form-label\">Contenu</label>
                        <div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input  type=\"file\" required=\"true\" name=\"annexe\" class=\"dropify-fr\" id=\"annexe\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t<button type=\"submit\" id=\"submit\"  class=\"btn btn-primary\">Soumettre</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t
    </div>
</section>
{% endblock %}
{% block script %}
<script type=\"text/javascript\" src=\"{{asset('web/js/image-uploader.js')}}\"></script>
<!-- jQuery file upload -->
<script id=\"myScript\" src=\"{{asset('admin/plugins/bower_components/dropify/dist/js/dropify.min.js')}}\"></script>
{# <script src=\"{{asset('web/js/dropzone.js')}}\"></script> #}
<script>
    \$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar addForm=document.getElementById('addForm')
    var number=0;
\tvar responseMessage=document.getElementById(\"responseMessage\");
\t\$('#submit').click(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar fd=new FormData();
\t\tif(\$(\"#title\").val()){
\t\t\tif(\$(\"#annexe\").val())
\t\t\t{
\t\t\t\tfd.append('file',document.getElementById(\"annexe\").files[0]);
\t\t\t\tfd.append('title',\$('#title').val());
\t\t\t\tfd.append('description',\$('#description').val());
\t\t\t\tfd.append('id',\$('#courseId').html());
\t\t\t\tAjaxFileFunction(fd);
\t\t\t}
\t\t\telse
\t\t\t{
\t\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu\";
\t\t\t\tresponseMessage.setAttribute('class','alert alert-danger');\t\t
\t\t\t}
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le titre\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t}
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"{{path('annexe_new')}}\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
\t\t\t//console.log('the result',result);
\t\t\tswal({
\t\t\t\ttext: result.data.message,
\t\t\t\ticon: \"success\",
\t\t\t\tbuttons: \"ok\",
\t\t\t});
\t\t\t\$('#title').val('');
\t\t\t\$('#description').val('');
\t\t\t\$('#annexe').val(null);

\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
<script>
\t\$(document).ready(function(){
\t\t\$('.input-images-1').imageUploader();
\t});
</script>
{% endblock %}", "annexes/new.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/annexes/new.html.twig");
    }
}
