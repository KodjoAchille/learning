<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* annexes/oldNew.html.twig */
class __TwigTemplate_ea9dbf2de3c8ed58acfca42ed6b7277266573529ae12723a7a6e6f0bd6c5066a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'banner' => [$this, 'block_banner'],
            'header' => [$this, 'block_header'],
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base-home.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "annexes/oldNew.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "annexes/oldNew.html.twig"));

        $this->parent = $this->loadTemplate("base-home.html.twig", "annexes/oldNew.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 4
        $this->displayBlock('css', $context, $blocks);
        // line 24
        echo "<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 5
        echo "\t<link rel=\"stylesheet\" id=\"myCss\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/css/dropify.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/image-uploader.css"), "html", null, true);
        echo "\">\t
\t<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/bootstrap.css"), "html", null, true);
        echo "\">\t
\t<style>
\t\t.modal {
\t\t\tdisplay:    none;
\t\t\tposition:   fixed;
\t\t\tz-index:    1000;
\t\t\ttop:        0;
\t\t\tleft:       0;
\t\t\theight:     100%;
\t\t\twidth:      100%;
\t\t\tbackground: rgba( 255, 255, 255, .8 ) 
\t\t\t\t\t\turl('";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/loader.gif"), "html", null, true);
        echo "') 
\t\t\t\t\t\t50% 50% 
\t\t\t\t\t\tno-repeat;
\t\t}
\t</style>\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 52
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 53
        echo "<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div id=\"loading-indicator\" class=\"modal\"></div>\t
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-2\">
\t\t\t</div>
\t\t\t<div class=\"col-8\">
                <h3><b> Ajouter des documents annexes</b></h3><br>
\t\t\t\t<div class=\"row\">
                    <div class=\"col-6\">
                        <button onclick=\"addRow()\" class=\"btn btn-default\"><span class=\"fa fa-plus-square\"></span> Ajouter un nouveau champ</button>
                    </div>
                    <div class=\"col-6\" style=\"padding-left:175px\">
\t\t\t\t\t\t<a href=\"javascript:void(0)\" type=\"submit\" id=\"submit\"  ><button class=\"btn btn-info\">Soumettre</button></a>
                    </div>
                </div>
               
\t\t\t\t<div id=\"responseMessage\" class=\"d-none\"></div>
\t\t\t\t<br/><br/><br/>
                <span id=\"courseId\" class=\"d-none\">";
        // line 73
        echo twig_escape_filter($this->env, (isset($context["courseId"]) || array_key_exists("courseId", $context) ? $context["courseId"] : (function () { throw new RuntimeError('Variable "courseId" does not exist.', 73, $this->source); })()), "html", null, true);
        echo "</span>
                <form  id=\"addForm\" action=\"";
        // line 74
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
        echo "\"  method=\"POST\" enctype=\"multipart/form-data\">
                    <div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"title\"  placeholder=\"Titre du cours\" required=\"true\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Description</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<textarea   rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"abstract\" placeholder=\"Bref résumé du cours\"></textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"z\" style=\"display:none\" class=\"form-group row\">
                        <div class=\"col-sm-10\">
\t\t\t\t\t\t\t<div id=\"pdfMessage_z\" class=\"d-none\"></div>
\t\t\t\t\t\t\t<input  onchange=\"filechecker(this.id)\" type=\"file\" required=\"true\" name=\"annexe_z\" class=\"dropify-fr\" id=\"annexe_z\" />
\t\t\t\t\t\t</div>
                        <div class=\"col-sm-2\">
                            <a  href=\"javascript:void(0)\" id=\"delete_z\" onclick=\"removeRow(this.parentElement.parentElement.id)\" ><span style=\"color:red \"class=\"fa fa-trash\">supprimer</span></a>
                        </div>
\t\t\t\t\t</div>
                    <div id=\"0\" class=\"form-group row\">
                        <div class=\"col-sm-10\">
\t\t\t\t\t\t\t<div id=\"pdfMessage_0\" class=\"d-none\"></div>
\t\t\t\t\t\t\t<input  onchange=\"filechecker(this.id)\" type=\"file\" required=\"true\" name=\"annexe_0\" class=\"dropify-fr\" id=\"annexe_0\" />
\t\t\t\t\t\t</div>
                        <div class=\"col-sm-2\">
                            <a  href=\"javascript:void(0)\" id=\"delete_0\" onclick=\"removeRow(this.parentElement.parentElement.id)\" ><span style=\"color:red\" class=\"fa fa-trash\">supprimer</span></a>
                        </div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t
    </div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 112
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 113
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/js/image-uploader.js"), "html", null, true);
        echo "\"></script>
<!-- jQuery file upload -->
<script id=\"myScript\" src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/js/dropify.min.js"), "html", null, true);
        echo "\"></script>
";
        // line 117
        echo "<script>
    \$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar fd=new FormData();
\tvar addForm=document.getElementById('addForm')
    var number=0;
\tvar responseMessage=document.getElementById(\"responseMessage\");
    function filechecker(id)
    {
        var file=document.getElementById(id).files[0];
\t    var pdfMessage=document.getElementById(\"pdfMessage_\"+id.slice(-1));

\t\t//var file = this.files[0];
\t\tif(isPdf(file['name']))
\t\t{
            var current='file_'+id.slice(-1);
            if(fd.has(current))
            {
                fd.delete(current);
            }
            fd.append(current,file);
\t\t\tpdfMessage.innerHTML=\"\";
\t\t\tpdfMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tpdfMessage.innerHTML=\"Veuillez ajouter  un fichier pdf\";
\t\t\tpdfMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#annexe_\"+id.slice(-1)).val('');
\t\t}
    }
    function removeRow(id)
    {
        number-=1;
        fd.delete('file_'+id);
        addForm.removeChild(document.getElementById(id));
    }
    function addRow()
    {
        \$(document).ready(function(){
            var currentNumber=number+1
            var cloned=\$('#z').clone().attr('id',currentNumber);
            if(document.getElementById(''+0)!=null)
            {
                \$('#'+number).after(cloned);
            }
            else
            {
                \$('#z').after(cloned);
            }
            var currentCloned=document.getElementById(''+currentNumber);
            currentCloned.setAttribute('style','');
            \$('#'+currentNumber).find('#pdfMessage_z').attr('id','pdfMessage_'+currentNumber);
            \$('#'+currentNumber).find('#annexe_z').attr('id','annexe_'+currentNumber);
            var currentInput=document.getElementById('annexe_'+currentNumber);
            currentInput.setAttribute('name','annexe_'+currentNumber);
            \$('#'+currentNumber).find('#annexe_'+number).attr('name','annexe_'+currentNumber);
            \$('#'+currentNumber).find('#delete_'+number).attr('id','delete_'+currentNumber);
            //console.log('the cloned',cloned);
            number+=1;
        });
    }
\tfunction isImage(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'jpg':
\t\t\tcase 'gif':
\t\t\tcase 'bmp':
\t\t\tcase 'png':
\t\t\t//etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isPdf(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'pdf':
\t\t\t// etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\t";
        // line 242
        echo "\t \$('#submit').click(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
        fd.append('number',number);
        fd.append('id',\$('#courseId').html());
\t\tAjaxFileFunction(fd);
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"";
        // line 254
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annexe_new");
        echo "\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
\t\t\t//console.log('the result',result);
\t\t\tswal({
\t\t\t\ttext: result.data.message,
\t\t\t\ticon: \"success\",
\t\t\t\tbuttons: \"ok\",
\t\t\t});
\t\t\taddForm.innerHTML=\"\";
\t\t\t//responseMessage.innerHTML=result.data.message;
\t\t\t//responseMessage.setAttribute('class','alert alert-success');
\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
<script>
\t\$(document).ready(function(){
\t\t\$('.input-images-1').imageUploader();
\t});
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "annexes/oldNew.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  426 => 254,  412 => 242,  288 => 117,  284 => 115,  278 => 113,  268 => 112,  220 => 74,  216 => 73,  194 => 53,  184 => 52,  168 => 18,  154 => 7,  150 => 6,  145 => 5,  135 => 4,  118 => 45,  110 => 40,  92 => 24,  90 => 4,  80 => 3,  62 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base-home.html.twig\" %}
{% block banner %}{% endblock %}
{% block header %}
{% block css %}
\t<link rel=\"stylesheet\" id=\"myCss\" href=\"{{asset('admin/plugins/bower_components/dropify/dist/css/dropify.min.css')}}\">
\t<link rel=\"stylesheet\" href=\"{{asset('web/css/image-uploader.css')}}\">\t
\t<link rel=\"stylesheet\" href=\"{{asset('web/css/bootstrap.css')}}\">\t
\t<style>
\t\t.modal {
\t\t\tdisplay:    none;
\t\t\tposition:   fixed;
\t\t\tz-index:    1000;
\t\t\ttop:        0;
\t\t\tleft:       0;
\t\t\theight:     100%;
\t\t\twidth:      100%;
\t\t\tbackground: rgba( 255, 255, 255, .8 ) 
\t\t\t\t\t\turl('{{asset(\"web/images/loader.gif\")}}') 
\t\t\t\t\t\t50% 50% 
\t\t\t\t\t\tno-repeat;
\t\t}
\t</style>\t
{% endblock %}
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"{{asset('web/images/s2.png')}}\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"{{path('index')}}\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
{% endblock %}
{% block content %}
<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div id=\"loading-indicator\" class=\"modal\"></div>\t
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-2\">
\t\t\t</div>
\t\t\t<div class=\"col-8\">
                <h3><b> Ajouter des documents annexes</b></h3><br>
\t\t\t\t<div class=\"row\">
                    <div class=\"col-6\">
                        <button onclick=\"addRow()\" class=\"btn btn-default\"><span class=\"fa fa-plus-square\"></span> Ajouter un nouveau champ</button>
                    </div>
                    <div class=\"col-6\" style=\"padding-left:175px\">
\t\t\t\t\t\t<a href=\"javascript:void(0)\" type=\"submit\" id=\"submit\"  ><button class=\"btn btn-info\">Soumettre</button></a>
                    </div>
                </div>
               
\t\t\t\t<div id=\"responseMessage\" class=\"d-none\"></div>
\t\t\t\t<br/><br/><br/>
                <span id=\"courseId\" class=\"d-none\">{{courseId}}</span>
                <form  id=\"addForm\" action=\"{{path('course_new')}}\"  method=\"POST\" enctype=\"multipart/form-data\">
                    <div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"title\"  placeholder=\"Titre du cours\" required=\"true\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Description</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<textarea   rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"abstract\" placeholder=\"Bref résumé du cours\"></textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"z\" style=\"display:none\" class=\"form-group row\">
                        <div class=\"col-sm-10\">
\t\t\t\t\t\t\t<div id=\"pdfMessage_z\" class=\"d-none\"></div>
\t\t\t\t\t\t\t<input  onchange=\"filechecker(this.id)\" type=\"file\" required=\"true\" name=\"annexe_z\" class=\"dropify-fr\" id=\"annexe_z\" />
\t\t\t\t\t\t</div>
                        <div class=\"col-sm-2\">
                            <a  href=\"javascript:void(0)\" id=\"delete_z\" onclick=\"removeRow(this.parentElement.parentElement.id)\" ><span style=\"color:red \"class=\"fa fa-trash\">supprimer</span></a>
                        </div>
\t\t\t\t\t</div>
                    <div id=\"0\" class=\"form-group row\">
                        <div class=\"col-sm-10\">
\t\t\t\t\t\t\t<div id=\"pdfMessage_0\" class=\"d-none\"></div>
\t\t\t\t\t\t\t<input  onchange=\"filechecker(this.id)\" type=\"file\" required=\"true\" name=\"annexe_0\" class=\"dropify-fr\" id=\"annexe_0\" />
\t\t\t\t\t\t</div>
                        <div class=\"col-sm-2\">
                            <a  href=\"javascript:void(0)\" id=\"delete_0\" onclick=\"removeRow(this.parentElement.parentElement.id)\" ><span style=\"color:red\" class=\"fa fa-trash\">supprimer</span></a>
                        </div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t
    </div>
</section>
{% endblock %}
{% block script %}
<script type=\"text/javascript\" src=\"{{asset('web/js/image-uploader.js')}}\"></script>
<!-- jQuery file upload -->
<script id=\"myScript\" src=\"{{asset('admin/plugins/bower_components/dropify/dist/js/dropify.min.js')}}\"></script>
{# <script src=\"{{asset('web/js/dropzone.js')}}\"></script> #}
<script>
    \$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar fd=new FormData();
\tvar addForm=document.getElementById('addForm')
    var number=0;
\tvar responseMessage=document.getElementById(\"responseMessage\");
    function filechecker(id)
    {
        var file=document.getElementById(id).files[0];
\t    var pdfMessage=document.getElementById(\"pdfMessage_\"+id.slice(-1));

\t\t//var file = this.files[0];
\t\tif(isPdf(file['name']))
\t\t{
            var current='file_'+id.slice(-1);
            if(fd.has(current))
            {
                fd.delete(current);
            }
            fd.append(current,file);
\t\t\tpdfMessage.innerHTML=\"\";
\t\t\tpdfMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tpdfMessage.innerHTML=\"Veuillez ajouter  un fichier pdf\";
\t\t\tpdfMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#annexe_\"+id.slice(-1)).val('');
\t\t}
    }
    function removeRow(id)
    {
        number-=1;
        fd.delete('file_'+id);
        addForm.removeChild(document.getElementById(id));
    }
    function addRow()
    {
        \$(document).ready(function(){
            var currentNumber=number+1
            var cloned=\$('#z').clone().attr('id',currentNumber);
            if(document.getElementById(''+0)!=null)
            {
                \$('#'+number).after(cloned);
            }
            else
            {
                \$('#z').after(cloned);
            }
            var currentCloned=document.getElementById(''+currentNumber);
            currentCloned.setAttribute('style','');
            \$('#'+currentNumber).find('#pdfMessage_z').attr('id','pdfMessage_'+currentNumber);
            \$('#'+currentNumber).find('#annexe_z').attr('id','annexe_'+currentNumber);
            var currentInput=document.getElementById('annexe_'+currentNumber);
            currentInput.setAttribute('name','annexe_'+currentNumber);
            \$('#'+currentNumber).find('#annexe_'+number).attr('name','annexe_'+currentNumber);
            \$('#'+currentNumber).find('#delete_'+number).attr('id','delete_'+currentNumber);
            //console.log('the cloned',cloned);
            number+=1;
        });
    }
\tfunction isImage(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'jpg':
\t\t\tcase 'gif':
\t\t\tcase 'bmp':
\t\t\tcase 'png':
\t\t\t//etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isPdf(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'pdf':
\t\t\t// etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\t{# function _(el){
\t\treturn document.getElementById(el);
\t} #}
\t \$('#submit').click(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
        fd.append('number',number);
        fd.append('id',\$('#courseId').html());
\t\tAjaxFileFunction(fd);
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"{{path('annexe_new')}}\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
\t\t\t//console.log('the result',result);
\t\t\tswal({
\t\t\t\ttext: result.data.message,
\t\t\t\ticon: \"success\",
\t\t\t\tbuttons: \"ok\",
\t\t\t});
\t\t\taddForm.innerHTML=\"\";
\t\t\t//responseMessage.innerHTML=result.data.message;
\t\t\t//responseMessage.setAttribute('class','alert alert-success');
\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
<script>
\t\$(document).ready(function(){
\t\t\$('.input-images-1').imageUploader();
\t});
</script>
{% endblock %}", "annexes/oldNew.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/annexes/oldNew.html.twig");
    }
}
