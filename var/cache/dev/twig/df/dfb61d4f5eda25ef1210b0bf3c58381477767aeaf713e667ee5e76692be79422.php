<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/courses/indexLite.html.twig */
class __TwigTemplate_04908f6b4ceff5ddb5fa6c0a8a4ee7a5bae6cc28631755a2edacb97eb59f01cf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/indexLite.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/indexLite.html.twig"));

        // line 1
        if (((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 1, $this->source); })()) != null)) {
            echo " 
    ";
            // line 2
            $context["compteur"] = 0;
            // line 3
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 3, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
                // line 4
                echo "    ";
                if ((twig_get_attribute($this->env, $this->source, $context["course"], "deleted", [], "any", false, false, false, 4) != "true")) {
                    // line 5
                    echo "        ";
                    $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 5, $this->source); })()) + 1);
                    // line 6
                    echo "         <tr>
            <td style=\"color:black\">";
                    // line 7
                    echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 7, $this->source); })()), "html", null, true);
                    echo "</td>
            <td class=\"text-center\">";
                    // line 8
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 8), "firstName", [], "any", false, false, false, 8), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 8), "name", [], "any", false, false, false, 8), "html", null, true);
                    echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#teacherModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 8), "html", null, true);
                    echo "\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a></td>
            <td style=\"color:black\">";
                    // line 9
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "title", [], "any", false, false, false, 9), "html", null, true);
                    echo "</td>
            <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal";
                    // line 10
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 10), "html", null, true);
                    echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
            <td class=\"text-center\"> 
                ";
                    // line 12
                    if (twig_get_attribute($this->env, $this->source, $context["course"], "comment", [], "any", false, false, false, 12)) {
                        // line 13
                        echo "                    <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 13), "html", null, true);
                        echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                ";
                    } else {
                        // line 15
                        echo "                ";
                    }
                    // line 16
                    echo "            </td>
            <td class=\"text-center\"> <a href=\"";
                    // line 17
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_show", ["id" => twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 17)]), "html", null, true);
                    echo "\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
            <td style=\"color:black\">
                ";
                    // line 19
                    if ((twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 19) != null)) {
                        // line 20
                        echo "                    ";
                        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 20)), "html", null, true);
                        echo "
                    ";
                        // line 21
                        $context["break"] = false;
                        // line 22
                        echo "                    ";
                        $context["i"] = 0;
                        // line 23
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 23));
                        foreach ($context['_seq'] as $context["_key"] => $context["annexe"]) {
                            if ( !(isset($context["break"]) || array_key_exists("break", $context) ? $context["break"] : (function () { throw new RuntimeError('Variable "break" does not exist.', 23, $this->source); })())) {
                                // line 24
                                echo "                        ";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "title", [], "any", false, false, false, 24), "html", null, true);
                                echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 24), "html", null, true);
                                echo "\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                            <div id=\"annexeModal";
                                // line 25
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 25), "html", null, true);
                                echo "\" class=\"modal fade\" role=\"dialog\">
                            <div i class=\"modal-dialog\" >
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                        <span class=\"modal-title\" id=\"myModalLabel\">
                                            <div class=\"row\">
                                                <div class=\"col-md-2\">
                                                </div>
                                                <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                            </div> 
                                        </span> 
                                    </div>
                                    <div class=\"modal-body\">
                                        ";
                                // line 39
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 39));
                                foreach ($context['_seq'] as $context["_key"] => $context["currentAnnexe"]) {
                                    // line 40
                                    echo "                                        <div class=\"row\">
                                            <div class=\"col-6\">
                                                ";
                                    // line 42
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "title", [], "any", false, false, false, 42), "html", null, true);
                                    echo "
                                            </div>
                                            <div class=\"col-6\">
                                                ";
                                    // line 45
                                    if (twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 45)) {
                                        // line 46
                                        echo "                                                    <a href=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 46))), "html", null, true);
                                        echo "\"  download=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 46))), "html", null, true);
                                        echo "\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                ";
                                    }
                                    // line 48
                                    echo "                                            </div>
                                        </div>
                                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currentAnnexe'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 51
                                echo "                                    </div>
                                    <div class=\"modal-footer\">
                                        <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                        <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                        ";
                                // line 60
                                $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 60, $this->source); })()) + 1);
                                // line 61
                                echo "                        ";
                                if (((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 61, $this->source); })()) == 1)) {
                                    // line 62
                                    echo "                            ";
                                    $context["break"] = true;
                                    // line 63
                                    echo "                        ";
                                }
                                // line 64
                                echo "                    ";
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annexe'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 65
                        echo "                ";
                    } else {
                        // line 66
                        echo "                    0
                ";
                    }
                    // line 68
                    echo "            </td>
            <td class=\"text-nowrap\">   
                <a href=\"";
                    // line 70
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_edit_form", ["id" => twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 70)]), "html", null, true);
                    echo "\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal";
                    // line 71
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 71), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                ";
                    // line 72
                    if ((twig_get_attribute($this->env, $this->source, $context["course"], "validated", [], "any", false, false, false, 72) != "true")) {
                        // line 73
                        echo "                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 73), "html", null, true);
                        echo "\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                ";
                    } else {
                        // line 75
                        echo "                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 75), "html", null, true);
                        echo "\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                ";
                    }
                    // line 77
                    echo "                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#commentModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 77), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-comment\"></i></span> </a>
            </td>
        </tr>
    ";
                }
                // line 80
                echo "    
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 83
            echo "    ";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/courses/indexLite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 83,  247 => 80,  239 => 77,  233 => 75,  227 => 73,  225 => 72,  221 => 71,  217 => 70,  213 => 68,  209 => 66,  206 => 65,  199 => 64,  196 => 63,  193 => 62,  190 => 61,  188 => 60,  177 => 51,  169 => 48,  161 => 46,  159 => 45,  153 => 42,  149 => 40,  145 => 39,  128 => 25,  121 => 24,  115 => 23,  112 => 22,  110 => 21,  105 => 20,  103 => 19,  98 => 17,  95 => 16,  92 => 15,  86 => 13,  84 => 12,  79 => 10,  75 => 9,  67 => 8,  63 => 7,  60 => 6,  57 => 5,  54 => 4,  49 => 3,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if courses != null %} 
    {% set compteur = 0 %}
    {% for course in courses  %}
    {% if course.deleted != \"true\" %}
        {% set compteur=compteur+1%}
         <tr>
            <td style=\"color:black\">{{compteur}}</td>
            <td class=\"text-center\">{{course.teacher.firstName}}{{\" \"}}{{course.teacher.name}}<a href=\"#\" data-toggle=\"modal\" data-target=\"#teacherModal{{course.id}}\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a></td>
            <td style=\"color:black\">{{course.title}}</td>
            <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal{{course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
            <td class=\"text-center\"> 
                {% if course.comment %}
                    <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal{{course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                {% else %}
                {% endif %}
            </td>
            <td class=\"text-center\"> <a href=\"{{path('admin_course_show',{'id':course.id})}}\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
            <td style=\"color:black\">
                {% if course.annexes != null %}
                    {{course.annexes|length }}
                    {% set break = false %}
                    {% set i = 0 %}
                    {% for annexe in course.annexes if not break %}
                        {{annexe.title}}<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal{{annexe.id}}\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                            <div id=\"annexeModal{{annexe.id}}\" class=\"modal fade\" role=\"dialog\">
                            <div i class=\"modal-dialog\" >
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                        <span class=\"modal-title\" id=\"myModalLabel\">
                                            <div class=\"row\">
                                                <div class=\"col-md-2\">
                                                </div>
                                                <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                            </div> 
                                        </span> 
                                    </div>
                                    <div class=\"modal-body\">
                                        {% for currentAnnexe in course.annexes %}
                                        <div class=\"row\">
                                            <div class=\"col-6\">
                                                {{currentAnnexe.title}}
                                            </div>
                                            <div class=\"col-6\">
                                                {% if currentAnnexe.file %}
                                                    <a href=\"{{asset('data/' ~ currentAnnexe.file)}}\"  download=\"{{asset('data/' ~ currentAnnexe.file)}}\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                {% endif %}
                                            </div>
                                        </div>
                                        {% endfor %}
                                    </div>
                                    <div class=\"modal-footer\">
                                        <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                        <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                        {% set i = i+1 %}
                        {% if i==1 %}
                            {% set break = true %}
                        {% endif %}
                    {% endfor %}
                {% else %}
                    0
                {% endif %}
            </td>
            <td class=\"text-nowrap\">   
                <a href=\"{{path('admin_course_edit_form',{'id':course.id})}}\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal{{course.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                {% if course.validated != \"true\" %}
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal{{course.id}}\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                {% else %}
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal{{course.id}}\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                {% endif %}
                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#commentModal{{course.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-comment\"></i></span> </a>
            </td>
        </tr>
    {% endif %}    
    {% endfor %}
{% else %}
    {# <tr>
        <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
    </tr> #}
{% endif %}
", "admin/courses/indexLite.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/courses/indexLite.html.twig");
    }
}
