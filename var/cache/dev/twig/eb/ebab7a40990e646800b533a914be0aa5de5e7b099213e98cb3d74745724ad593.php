<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* account/login.html.twig */
class __TwigTemplate_4ff6a4a096c33447d33ce0f980807d530e0302bb652845f7f0668579fd1add37 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "account/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "account/login.html.twig"));

        // line 1
        echo "<h5 class=\"mb-3\">Rejoignez notre école</h5>
<div color=\"\">
    <span>Vous n'avez pas de compte?  <a href=\"javascript:void(0)\" onclick=\"showRegisterOrConnectForm()\">s'inscrire</a></span>
</div>
";
        // line 5
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 5, $this->source); })())) {
            // line 6
            echo "    <div class=\"alert alert-danger m-b-30 font-13\">
        ";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 7, $this->source); })()), "messageKey", [], "any", false, false, false, 7), "html", null, true);
            echo "
    </div>
";
        }
        // line 10
        echo "<div class=\"form-style-w3ls\">
    <input type=\"text\" class=\"form-control\" id=\"exampleInputuname\"  required=\"true\" name=\"_username\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["lastUserName"]) || array_key_exists("lastUserName", $context) ? $context["lastUserName"] : (function () { throw new RuntimeError('Variable "lastUserName" does not exist.', 11, $this->source); })()), "html", null, true);
        echo "\" placeholder=\"Entrez votre nom d'utilisateur\"> 
    <input type=\"password\" required=\"true\" class=\"form-control\" id=\"exampleInputpwd1\" name=\"_password\"  placeholder=\"Entrer votre mot de passe\"> 

    ";
        // line 15
        echo "    ";
        // line 16
        echo "    
    <button type=\"submit\" Class=\"btn\"> Soumettre</button><br><br>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "account/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 16,  69 => 15,  63 => 11,  60 => 10,  54 => 7,  51 => 6,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<h5 class=\"mb-3\">Rejoignez notre école</h5>
<div color=\"\">
    <span>Vous n'avez pas de compte?  <a href=\"javascript:void(0)\" onclick=\"showRegisterOrConnectForm()\">s'inscrire</a></span>
</div>
{% if error %}
    <div class=\"alert alert-danger m-b-30 font-13\">
        {{error.messageKey }}
    </div>
{% endif %}
<div class=\"form-style-w3ls\">
    <input type=\"text\" class=\"form-control\" id=\"exampleInputuname\"  required=\"true\" name=\"_username\" value=\"{{lastUserName}}\" placeholder=\"Entrez votre nom d'utilisateur\"> 
    <input type=\"password\" required=\"true\" class=\"form-control\" id=\"exampleInputpwd1\" name=\"_password\"  placeholder=\"Entrer votre mot de passe\"> 

    {# <input placeholder=\"Identifiant\" id=\"cUsername\" name=\"username\" type=\"text\" required=\"true\"> #}
    {# <input placeholder=\"Mot de passe\" id=\"cPassword\" name=\"password\" type=\"password\" required=\"true\"> #}
    
    <button type=\"submit\" Class=\"btn\"> Soumettre</button><br><br>
</div>", "account/login.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/account/login.html.twig");
    }
}
