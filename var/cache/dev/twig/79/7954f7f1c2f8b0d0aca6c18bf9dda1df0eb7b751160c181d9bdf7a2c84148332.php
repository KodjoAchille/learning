<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* courses/show.html.twig */
class __TwigTemplate_3a1566cd7076e8e8c47b0402ca8cdcb7dd5e482fe3640a0e2ed4ff0179b35b01 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'banner' => [$this, 'block_banner'],
            'header' => [$this, 'block_header'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base-home.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/show.html.twig"));

        $this->parent = $this->loadTemplate("base-home.html.twig", "courses/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<style>
input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}
.search-container button:hover {
  background: #ccc;
}
.search-container {
  
}
</style>
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 46
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 47
        echo "<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
\t\t\t\t<h4 class=\"\"><img src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s1.png"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 52, $this->source); })()), "title", [], "any", false, false, false, 52), "html", null, true);
        echo "</h4>
            </div>
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"heading mb-sm-5 mb-4 search-container\">
                    <form action=\"\">
                        <input type=\"text\" placeholder=\"taper quelquechose..\" name=\"search\">
                        <button style=\"align:center\"  class=\"btn btn-dark\">Rechercher</button>                
                    </form>
                </div>
            </div>
        </div>
        ";
        // line 64
        echo "\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-2 col-md-2\">
\t\t\t</div>
\t\t\t<div class=\"col-lg-8 col-md-8\">
\t\t\t\t<div class=\"grid\">
\t\t\t\t\t<video width=\"900\" height=\"500\"  controls>
\t\t\t\t\t\t<source src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 70, $this->source); })()), "content", [], "any", false, false, false, 70))), "html", null, true);
        echo "\" type=\"video/mp4\"/>
\t\t\t\t\t\tLe fichier video ne peut pas être lu
\t\t\t\t\t</video>
\t\t\t    </div>
\t\t\t</div>
\t\t</div>
    </div>
</section>
<section class=\"services py-5\" id=\"why\">
\t<div class=\"container\">
\t\t<h3 class=\"heading mb-5\">les documents annexes</h3>
\t\t<div class=\"row ml-sm-5\">
\t\t\t";
        // line 82
        if ((twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 82, $this->source); })()), "annexes", [], "any", false, false, false, 82) != null)) {
            // line 83
            echo "\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 83, $this->source); })()), "annexes", [], "any", false, false, false, 83));
            foreach ($context['_seq'] as $context["_key"] => $context["annexe"]) {
                // line 84
                echo "\t\t\t\t\t<div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-12\">
\t\t\t\t\t\t<div class=\"our-services-wrapper mb-60\">
\t\t\t\t\t\t\t<div class=\"services-inner\">
\t\t\t\t\t\t\t\t<div class=\"our-services-img\">
\t\t\t\t\t\t\t\t\t<img src=\"";
                // line 88
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s1.png"), "html", null, true);
                echo "\" alt=\"\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"our-services-text\">
\t\t\t\t\t\t\t\t\t<h4>";
                // line 91
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "file", [], "any", false, false, false, 91), "html", null, true);
                echo "</h4>
\t\t\t\t\t\t\t\t\t<a style=\"align:center\" href=\"";
                // line 92
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["annexe"], "file", [], "any", false, false, false, 92))), "html", null, true);
                echo "\" download=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["annexe"], "file", [], "any", false, false, false, 92))), "html", null, true);
                echo "\" type=\"button\" class=\"btn btn-dark\">Télécharger</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annexe'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 98
            echo "\t\t\t";
        }
        // line 99
        echo "\t\t</div>
\t\t<!-- positioned image -->
\t\t<div class=\"position-image\">
\t\t\t<img src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/services.png"), "html", null, true);
        echo "\" alt=\"\" class=\"img-fluid\">
\t\t</div>
\t\t<!-- //positioned image -->
\t</div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "courses/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 102,  241 => 99,  238 => 98,  224 => 92,  220 => 91,  214 => 88,  208 => 84,  203 => 83,  201 => 82,  186 => 70,  178 => 64,  162 => 52,  155 => 47,  145 => 46,  128 => 39,  120 => 34,  88 => 4,  78 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base-home.html.twig\" %}
{% block banner %}{% endblock %}
{% block header %}
<style>
input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}
.search-container button:hover {
  background: #ccc;
}
.search-container {
  
}
</style>
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"{{asset('web/images/s2.png')}}\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"{{path('index')}}\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
{% endblock %}
{% block content %}
<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
\t\t\t\t<h4 class=\"\"><img src=\"{{asset('web/images/s1.png')}}\" class=\"img-fluid\" alt=\"\">{{course.title}}</h4>
            </div>
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"heading mb-sm-5 mb-4 search-container\">
                    <form action=\"\">
                        <input type=\"text\" placeholder=\"taper quelquechose..\" name=\"search\">
                        <button style=\"align:center\"  class=\"btn btn-dark\">Rechercher</button>                
                    </form>
                </div>
            </div>
        </div>
        {# <h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3> #}
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-2 col-md-2\">
\t\t\t</div>
\t\t\t<div class=\"col-lg-8 col-md-8\">
\t\t\t\t<div class=\"grid\">
\t\t\t\t\t<video width=\"900\" height=\"500\"  controls>
\t\t\t\t\t\t<source src=\"{{asset('data/' ~ course.content)}}\" type=\"video/mp4\"/>
\t\t\t\t\t\tLe fichier video ne peut pas être lu
\t\t\t\t\t</video>
\t\t\t    </div>
\t\t\t</div>
\t\t</div>
    </div>
</section>
<section class=\"services py-5\" id=\"why\">
\t<div class=\"container\">
\t\t<h3 class=\"heading mb-5\">les documents annexes</h3>
\t\t<div class=\"row ml-sm-5\">
\t\t\t{% if course.annexes != null %}
\t\t\t\t{% for annexe in course.annexes %}
\t\t\t\t\t<div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-12\">
\t\t\t\t\t\t<div class=\"our-services-wrapper mb-60\">
\t\t\t\t\t\t\t<div class=\"services-inner\">
\t\t\t\t\t\t\t\t<div class=\"our-services-img\">
\t\t\t\t\t\t\t\t\t<img src=\"{{asset('web/images/s1.png')}}\" alt=\"\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"our-services-text\">
\t\t\t\t\t\t\t\t\t<h4>{{annexe.file}}</h4>
\t\t\t\t\t\t\t\t\t<a style=\"align:center\" href=\"{{asset('data/' ~ annexe.file)}}\" download=\"{{asset('data/' ~ annexe.file)}}\" type=\"button\" class=\"btn btn-dark\">Télécharger</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t{% endfor %}
\t\t\t{% endif %}
\t\t</div>
\t\t<!-- positioned image -->
\t\t<div class=\"position-image\">
\t\t\t<img src=\"{{asset('web/images/services.png')}}\" alt=\"\" class=\"img-fluid\">
\t\t</div>
\t\t<!-- //positioned image -->
\t</div>
</section>
{% endblock %}", "courses/show.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/courses/show.html.twig");
    }
}
