<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/courses/notvalide.html.twig */
class __TwigTemplate_8f36b77b02dda36cf90fb61bf2c1c09a1719801753ad56a03214f3a5fceda5d3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/notvalide.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/notvalide.html.twig"));

        $this->parent = $this->loadTemplate("admin-base.html.twig", "admin/courses/notvalide.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des cours</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Auteur</th>
                                            <th style=\"color: white\" >Titre</th>
                                            <th style=\"color: white\" >Résumé</th>
                                            <th style=\"color: white\" >Contenu</th>
                                            <th style=\"color: white\" >Annexe</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        ";
        // line 30
        if (((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 30, $this->source); })()) != null)) {
            echo " 
                                            ";
            // line 31
            $context["compteur"] = 0;
            // line 32
            echo "                                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 32, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
                // line 33
                echo "                                                ";
                if ((twig_get_attribute($this->env, $this->source, $context["course"], "deleted", [], "any", false, false, false, 33) != "true")) {
                    // line 34
                    echo "                                                    ";
                    $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 34, $this->source); })()) + 1);
                    // line 35
                    echo "                                                    <tr>
                                                        <td style=\"color:black\">";
                    // line 36
                    echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 36, $this->source); })()), "html", null, true);
                    echo "</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#teacherModal";
                    // line 37
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 37), "html", null, true);
                    echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td style=\"color:black\">";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "title", [], "any", false, false, false, 38), "html", null, true);
                    echo "</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal";
                    // line 39
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 39), "html", null, true);
                    echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td class=\"text-center\"> <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#contentModal";
                    // line 40
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 40), "html", null, true);
                    echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td style=\"color:black\">";
                    // line 41
                    echo "#";
                    echo "</td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"unValidateModal";
                    // line 44
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 44), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 50
                    echo "Voulez vous vraiment désactiver le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse(";
                    // line 55
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 55), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal";
                    // line 62
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 62), "html", null, true);
                    echo "\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 68
                    echo "Voulez vous vraiment activer le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse(";
                    // line 73
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 73), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"deleteModal";
                    // line 80
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 80), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 86
                    echo "Voulez vous vraiment supprimer le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteCourse(";
                    // line 91
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 91), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"contentModal";
                    // line 100
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 100), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Contenu du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"embed-responsive embed-responsive-16by9\">
                                                                        <iframe class=\"embed-responsive-item\" src=\"";
                    // line 115
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["course"], "content", [], "any", false, false, false, 115))), "html", null, true);
                    echo "\" ></iframe>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"abstractModal";
                    // line 127
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 127), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Résumé du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 143
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "abstract", [], "any", false, false, false, 143), "html", null, true);
                    echo "
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"teacherModal";
                    // line 158
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 158), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Information sur le créateur du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Email
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 179
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 179), "account", [], "any", false, false, false, 179), "email", [], "any", false, false, false, 179), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Nom
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 187
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 187), "name", [], "any", false, false, false, 187), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Prenoms
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 195
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 195), "firstName", [], "any", false, false, false, 195), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Telephone
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 203
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 203), "phone", [], "any", false, false, false, 203), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Type de compte
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 211
                    if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
                        // line 212
                        echo "                                                                                        ";
                        echo "ENSEIGNANT";
                        echo "
                                                                                    ";
                    } else {
                        // line 214
                        echo "                                                                                        ";
                        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_STUDENT")) {
                            // line 215
                            echo "                                                                                            ";
                            echo "ETUDIANT";
                            echo "
                                                                                        ";
                        }
                        // line 217
                        echo "                                                                                    ";
                    }
                    // line 218
                    echo "                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal --> 
                                                ";
                }
                // line 233
                echo "                                                        
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 235
            echo "                                        ";
        } else {
            // line 236
            echo "                                            ";
            // line 239
            echo "                                        ";
        }
        // line 240
        echo "
                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 252
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 253
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script src=\"";
        // line 254
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <!--Style Switcher -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/courses/notvalide.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  475 => 254,  470 => 253,  460 => 252,  439 => 240,  436 => 239,  434 => 236,  431 => 235,  424 => 233,  406 => 218,  403 => 217,  397 => 215,  394 => 214,  388 => 212,  386 => 211,  375 => 203,  364 => 195,  353 => 187,  342 => 179,  318 => 158,  300 => 143,  281 => 127,  266 => 115,  248 => 100,  236 => 91,  228 => 86,  219 => 80,  209 => 73,  201 => 68,  192 => 62,  182 => 55,  174 => 50,  165 => 44,  159 => 41,  155 => 40,  151 => 39,  147 => 38,  143 => 37,  139 => 36,  136 => 35,  133 => 34,  130 => 33,  125 => 32,  123 => 31,  119 => 30,  93 => 6,  83 => 5,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin-base.html.twig' %}
{%  block css %}
    <link href=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.css')}}\" rel=\"stylesheet\" type=\"text/css\" />
{% endblock %}
{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des cours</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Auteur</th>
                                            <th style=\"color: white\" >Titre</th>
                                            <th style=\"color: white\" >Résumé</th>
                                            <th style=\"color: white\" >Contenu</th>
                                            <th style=\"color: white\" >Annexe</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        {% if courses != null %} 
                                            {% set compteur = 0 %}
                                            {% for course in courses  %}
                                                {% if course.deleted != \"true\" %}
                                                    {% set compteur=compteur+1%}
                                                    <tr>
                                                        <td style=\"color:black\">{{compteur}}</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#teacherModal{{course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td style=\"color:black\">{{course.title}}</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal{{course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td class=\"text-center\"> <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#contentModal{{course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td style=\"color:black\">{{\"#\"}}</td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"unValidateModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment désactiver le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal{{course.id}}\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment activer le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"deleteModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment supprimer le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"contentModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Contenu du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"embed-responsive embed-responsive-16by9\">
                                                                        <iframe class=\"embed-responsive-item\" src=\"{{asset('data/' ~ course.content)}}\" ></iframe>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"abstractModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Résumé du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{course.abstract}}
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"teacherModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Information sur le créateur du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Email
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.account.email}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Nom
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.name}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Prenoms
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.firstName}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Telephone
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.phone}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Type de compte
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {% if is_granted('ROLE_TEACHER') %}
                                                                                        {{'ENSEIGNANT'}}
                                                                                    {% else %}
                                                                                        {% if is_granted('ROLE_STUDENT')%}
                                                                                            {{'ETUDIANT'}}
                                                                                        {% endif %}
                                                                                    {% endif %}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal --> 
                                                {% endif %}                                                        
                                            {% endfor %}
                                        {% else %}
                                            {# <tr>
                                                <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
                                            </tr> #}
                                        {% endif %}

                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script src=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.js')}}\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <!--Style Switcher -->
{% endblock %}", "admin/courses/notvalide.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/courses/notvalide.html.twig");
    }
}
