<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/teacherCourses.html.twig */
class __TwigTemplate_72873fbc487b4c45ad25cb4eca342a601e9d4467f2b19ef7cfd8d1cd4ec5a0f2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'banner' => [$this, 'block_banner'],
            'underHeader' => [$this, 'block_underHeader'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base-home.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/teacherCourses.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/teacherCourses.html.twig"));

        $this->parent = $this->loadTemplate("base-home.html.twig", "user/teacherCourses.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_underHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "underHeader"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "underHeader"));

        // line 4
        echo "\t<style>
\t\tinput[type=text] {
\t\tpadding: 6px;
\t\tmargin-top: 8px;
\t\tfont-size: 17px;
\t\tborder: none;
\t\t}
\t\t.search-container button:hover {
\t\tbackground: #ccc;
\t\t}
\t\t.search-container {
\t\t
\t\t}
\t</style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 19
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 20
        echo "<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
        \t\t<h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3>
            </div>
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"heading mb-sm-5 mb-4 search-container\">
                    <form action=\"\">
                        <input type=\"text\" placeholder=\"taper quelquechose..\" name=\"search\">
                        <button style=\"align:center\"  class=\"btn btn-dark\">Rechercher</button>                
                    </form>
                </div>
            </div>
        </div>
        ";
        // line 37
        echo "\t\t<div class=\"row\">
\t\t";
        // line 38
        if (((isset($context["userCourses"]) || array_key_exists("userCourses", $context) ? $context["userCourses"] : (function () { throw new RuntimeError('Variable "userCourses" does not exist.', 38, $this->source); })()) != null)) {
            echo " 
   \t\t\t";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["userCourses"]) || array_key_exists("userCourses", $context) ? $context["userCourses"] : (function () { throw new RuntimeError('Variable "userCourses" does not exist.', 39, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["userCourse"]) {
                // line 40
                echo "    \t\t\t";
                // line 41
                echo "\t\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"grid\">
\t\t\t\t\t\t<img src=\"";
                // line 43
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/choose1.jpg"), "html", null, true);
                echo "\" alt=\"\" class=\"img-fluid\" />
\t\t\t\t\t\t<div class=\"info p-4\">
\t\t\t\t\t\t\t<h4 class=\"\"><img src=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s1.png"), "html", null, true);
                echo "\" class=\"img-fluid\" alt=\"\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["userCourse"], "course", [], "any", false, false, false, 45), "title", [], "any", false, false, false, 45), "html", null, true);
                echo "</h4>
\t\t\t\t\t\t\t";
                // line 46
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
                    // line 47
                    echo "\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["userCourse"], "course", [], "any", false, false, false, 47), "id", [], "any", false, false, false, 47)]), "html", null, true);
                    echo "\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t";
                } else {
                    // line 49
                    echo "\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" title=\"veuilez vous connecter ou créer un compte\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t";
                }
                // line 51
                echo "\t\t\t\t\t\t\t<p class=\"mt-3\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["userCourse"], "course", [], "any", false, false, false, 51), "abstract", [], "any", false, false, false, 51), "html", null, true);
                echo "</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
                // line 56
                echo "\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userCourse'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "\t\t";
        }
        // line 58
        echo "\t\t</div>
    </div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 62
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 63
        echo "<script>
\t\$(document).ready(function() {
\t\tdocument.getElementById('header').setAttribute('style','background-color:black');
\t});
</script>
\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/teacherCourses.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 63,  211 => 62,  198 => 58,  195 => 57,  189 => 56,  181 => 51,  177 => 49,  171 => 47,  169 => 46,  163 => 45,  158 => 43,  154 => 41,  152 => 40,  148 => 39,  144 => 38,  141 => 37,  123 => 20,  113 => 19,  89 => 4,  79 => 3,  61 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base-home.html.twig\" %}
{% block banner %}{% endblock %}
{% block underHeader %}
\t<style>
\t\tinput[type=text] {
\t\tpadding: 6px;
\t\tmargin-top: 8px;
\t\tfont-size: 17px;
\t\tborder: none;
\t\t}
\t\t.search-container button:hover {
\t\tbackground: #ccc;
\t\t}
\t\t.search-container {
\t\t
\t\t}
\t</style>
{% endblock %}
{% block content %}
<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
        \t\t<h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3>
            </div>
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"heading mb-sm-5 mb-4 search-container\">
                    <form action=\"\">
                        <input type=\"text\" placeholder=\"taper quelquechose..\" name=\"search\">
                        <button style=\"align:center\"  class=\"btn btn-dark\">Rechercher</button>                
                    </form>
                </div>
            </div>
        </div>
        {# <h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3> #}
\t\t<div class=\"row\">
\t\t{% if userCourses != null %} 
   \t\t\t{% for userCourse in userCourses  %}
    \t\t\t{# {% if (userCourse.deleted != \"true\") and (course.validated==\"true\") %} #}
\t\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"grid\">
\t\t\t\t\t\t<img src=\"{{asset('web/images/choose1.jpg')}}\" alt=\"\" class=\"img-fluid\" />
\t\t\t\t\t\t<div class=\"info p-4\">
\t\t\t\t\t\t\t<h4 class=\"\"><img src=\"{{asset('web/images/s1.png')}}\" class=\"img-fluid\" alt=\"\">{{userCourse.course.title}}</h4>
\t\t\t\t\t\t\t{% if is_granted('ROLE_USER')%}
\t\t\t\t\t\t\t\t<a href=\"{{path('course_show',{'id':userCourse.course.id})}}\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" title=\"veuilez vous connecter ou créer un compte\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t<p class=\"mt-3\">{{userCourse.course.abstract}}</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t{# {% endif %} #}
\t\t\t{% endfor %}
\t\t{% endif %}
\t\t</div>
    </div>
</section>
{% endblock %}
{% block script %}
<script>
\t\$(document).ready(function() {
\t\tdocument.getElementById('header').setAttribute('style','background-color:black');
\t});
</script>
\t
{% endblock %}", "user/teacherCourses.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/user/teacherCourses.html.twig");
    }
}
