<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin-base.html.twig */
class __TwigTemplate_7facd8ae04f07b44e7fce93c46af5e1db8b7b396d595ad3119e0f36ebaf2e78c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'header' => [$this, 'block_header'],
            'banner' => [$this, 'block_banner'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin-base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin-base.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html>
\t<head>
    ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 31
        echo "\t</head>

<body >
";
        // line 34
        $this->displayBlock('body', $context, $blocks);
        // line 185
        echo "</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 5
        echo "\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<meta name=\"description\" content=\"\">
\t\t<meta name=\"author\" content=\"\">
\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\">
\t\t<title>E-learning</title>
\t\t<!-- Bootstrap Core CSS -->
\t\t<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/personnel_swt.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- Menu CSS -->
\t\t<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- toast CSS -->
\t\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/toast-master/css/jquery.toast.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- morris CSS -->
\t\t<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/morrisjs/morris.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- animation CSS -->
\t\t<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- Custom CSS -->
\t\t<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<link href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/personnel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- color CSS -->
\t\t<link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/colors/blue.css"), "html", null, true);
        echo "\" id=\"theme\" rel=\"stylesheet\">
\t\t";
        // line 29
        $this->displayBlock('css', $context, $blocks);
        // line 30
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 34
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 35
        echo "<!-- header -->
";
        // line 36
        $this->displayBlock('header', $context, $blocks);
        // line 131
        echo "\t";
        $this->displayBlock('banner', $context, $blocks);
        // line 133
        echo "    <div id=\"contenu-base-admin\">
\t";
        // line 134
        $this->displayBlock('content', $context, $blocks);
        // line 136
        echo "    </div>
\t<!-- footer -->
\t";
        // line 138
        $this->displayBlock('footer', $context, $blocks);
        // line 181
        echo "\t
\t<!-- footer -->
</body>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 37
        echo "\t<!-- Preloader -->
    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div> 
    <style>
        .navbar-header {
            border-bottom: 2px solid #03a9f3;
        }
    </style>
    <div id=\"wrapper\">
\t\t<!-- Navigation -->
        <nav class=\"navbar  navbar-default navbar-static-top m-b-0\">
            <div class=\"navbar-header\"> 
\t\t\t\t<a class=\"navbar-toggle hidden-sm hidden-md hidden-lg\" id=\"myMenu\" href=\"\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t<i class=\"ti-menu\"></i>
\t\t\t\t</a>\t
                <span class=\"top-left-part\">
\t\t\t\t\t<a class=\"logo\" href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_index");
        echo "\">
\t\t\t\t\t\t<h3>
\t\t\t\t\t\t<img src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/learn-logo.png"), "html", null, true);
        echo "\" alt=\"home\" style=\"height: 6vh;\" />
\t\t\t\t\t</a>
\t\t\t\t</span>
                <ul class=\"nav navbar-top-links navbar-right pull-right\">
\t\t\t\t\t";
        // line 60
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 61
            echo "\t\t\t\t\t<li><a  href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
            echo "\"> <span class=\"fa fa-home\"></span> <b class=\"hidden-xs\">Acceuil</b> </a></li>
\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"\"> <span class=\"img img-circle glyphicon glyphicon-user\"></span> <b class=\"hidden-xs\">";
            // line 63
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 63, $this->source); })()), "user", [], "any", false, false, false, 63), "username", [], "any", false, false, false, 63), "html", null, true);
            echo "</b> </a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated flipInY\">
\t\t\t\t\t\t\t<li><a href=\"#\" ><i class=\"ti-user\"></i> Mon Profile</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 67
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_show");
            echo "\"><i class=\"ti-settings\"></i> Parametres de compte</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 69
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\"><i class=\"fa fa-power-off\"></i> Se déconnecter</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t\t</li>
\t\t\t\t\t";
        }
        // line 74
        echo "                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
\t\t <!-- Left navbar-header -->
        ";
        // line 82
        echo " 
        <div class=\"navbar-default sidebar\" role=\"navigation\">
            <div class=\"sidebar-nav navbar-collapse slimscrollsidebar\">
                <ul class=\"nav\" id=\"side-menu\">
                    <li class=\"sidebar-search hidden-sm hidden-md hidden-lg\">
                        <!-- input-group -->
                        <div class=\"input-group custom-search-form\">
                            <input type=\"text\" class=\"form-control\" placeholder=\"Search...\"> 
                            <span class=\"input-group-btn\">
                                <button class=\"btn btn-default\" type=\"button\"> <i class=\"fa fa-search\"></i> </button>
                            </span>
                         </div>
                        <!-- /input-group -->
                    </li>
\t\t\t\t\t<br/>
\t\t\t\t\t<br/>
                
                    ";
        // line 99
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 100
            echo "                        <li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-cog\" data-icon=\"v\"></i> Tableau de bord</a>
                        </li>

                        <li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-users\" data-icon=\"v\"></i> <span class=\"hide-menu\"> Utilisateurs <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\">1</span></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li><a href=\"";
            // line 105
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list");
            echo "\"><i class=\"fa fa-tasks\"></i> Liste</a></li>
                            </ul>
                        </li>

                        <li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-book\" data-icon=\"v\"></i> <span class=\"hide-menu\"> Cours <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\">4</span></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li><a href=\"";
            // line 111
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_list");
            echo "\"><i class=\"fa fa-download\"></i> Liste</a></li>
                                <li role=\"separator\" class=\"divider\"></li>
                                <li><a href=\"";
            // line 113
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_valide");
            echo "\"><i class=\"fa fa-plus-circle\"></i>Actives</a></li>
                                <li role=\"separator\" class=\"divider\"></li>
                                <li><a href=\"";
            // line 115
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_notvalide");
            echo "\"><i class=\"fa fa-check-square-o\"></i>Non actives</a></li>
                            </ul>
                        </li>

\t\t\t\t\t\t<li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-book\" data-icon=\"v\"></i> <span class=\"hide-menu\"> Classes de cours <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\">1</span></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li><a href=\"";
            // line 121
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_room_list");
            echo "\"><i class=\"fa fa-download\"></i> Liste</a></li>
                                <li role=\"separator\" class=\"divider\"></li>
                            </ul>
                        </li>       
                    ";
        }
        // line 126
        echo "            </div>
        </div>
        <!-- Left navbar-header end -->
\t</div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 131
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        // line 132
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 134
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 135
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 138
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 139
        echo "        <footer class=\"footer text-center\" style=\"background: #232323; font-size: 12px; color: #03a9f3; \">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"widget-body\">
                        <p class=\"text-center\">
                            Copyright &copy; E-learning 2020
                        </p>
                    </div>
                </div>
            </div>
        </footer>

\t\t<!-- jQuery -->
\t\t<script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/sweetalert.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Bootstrap Core JavaScript -->
\t\t<script src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/js/tether.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Menu Plugin JavaScript -->
\t\t<script src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!--slimscroll JavaScript -->
\t\t<script src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Wave Effects -->
\t\t<script src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/waves.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Counter js -->
\t\t<script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/waypoints/lib/jquery.waypoints.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/counterup/jquery.counterup.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Morris JavaScript -->
\t\t<script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/raphael/raphael-min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/morrisjs/morris.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Custom Theme JavaScript -->
\t\t<script src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/dashboard1.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Sparkline chart JavaScript -->
\t\t<script src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/toast-master/js/jquery.toast.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Style Switcher -->
\t\t<script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"), "html", null, true);
        echo "\"></script>
\t    ";
        // line 179
        $this->displayBlock('script', $context, $blocks);
        // line 180
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 179
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin-base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  531 => 179,  521 => 180,  519 => 179,  515 => 178,  510 => 176,  506 => 175,  502 => 174,  497 => 172,  493 => 171,  488 => 169,  484 => 168,  479 => 166,  475 => 165,  470 => 163,  465 => 161,  460 => 159,  455 => 157,  451 => 156,  447 => 155,  442 => 153,  438 => 152,  423 => 139,  413 => 138,  403 => 135,  393 => 134,  383 => 132,  373 => 131,  359 => 126,  351 => 121,  342 => 115,  337 => 113,  332 => 111,  323 => 105,  316 => 100,  314 => 99,  295 => 82,  285 => 74,  277 => 69,  272 => 67,  265 => 63,  259 => 61,  257 => 60,  250 => 56,  245 => 54,  226 => 37,  216 => 36,  203 => 181,  201 => 138,  197 => 136,  195 => 134,  192 => 133,  189 => 131,  187 => 36,  184 => 35,  174 => 34,  156 => 29,  146 => 30,  144 => 29,  140 => 28,  135 => 26,  131 => 25,  126 => 23,  121 => 21,  116 => 19,  111 => 17,  106 => 15,  102 => 14,  98 => 13,  92 => 10,  85 => 5,  75 => 4,  65 => 185,  63 => 34,  58 => 31,  56 => 4,  51 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE HTML>
<html>
\t<head>
    {%  block head %}
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<meta name=\"description\" content=\"\">
\t\t<meta name=\"author\" content=\"\">
\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"{{asset('web/images/s2.png')}}\">
\t\t<title>E-learning</title>
\t\t<!-- Bootstrap Core CSS -->
\t\t<link href=\"{{asset('admin/bootstrap/dist/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
\t\t<link href=\"{{asset('admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css')}}\" rel=\"stylesheet\">
\t\t<link href=\"{{asset('admin/css/personnel_swt.css')}}\" rel=\"stylesheet\">
\t\t<!-- Menu CSS -->
\t\t<link href=\"{{asset('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}\" rel=\"stylesheet\">
\t\t<!-- toast CSS -->
\t\t<link href=\"{{asset('admin/plugins/bower_components/toast-master/css/jquery.toast.css')}}\" rel=\"stylesheet\">
\t\t<!-- morris CSS -->
\t\t<link href=\"{{asset('admin/plugins/bower_components/morrisjs/morris.css')}}\" rel=\"stylesheet\">
\t\t<!-- animation CSS -->
\t\t<link href=\"{{asset('admin/css/animate.css')}}\" rel=\"stylesheet\">
\t\t<!-- Custom CSS -->
\t\t<link href=\"{{asset('admin/css/style.css')}}\" rel=\"stylesheet\">
\t\t<link href=\"{{asset('admin/css/personnel.css')}}\" rel=\"stylesheet\">
\t\t<!-- color CSS -->
\t\t<link href=\"{{asset('admin/css/colors/blue.css')}}\" id=\"theme\" rel=\"stylesheet\">
\t\t{%  block css %}{% endblock %}
\t{% endblock %}
\t</head>

<body >
{% block body %}
<!-- header -->
{% block header %}
\t<!-- Preloader -->
    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div> 
    <style>
        .navbar-header {
            border-bottom: 2px solid #03a9f3;
        }
    </style>
    <div id=\"wrapper\">
\t\t<!-- Navigation -->
        <nav class=\"navbar  navbar-default navbar-static-top m-b-0\">
            <div class=\"navbar-header\"> 
\t\t\t\t<a class=\"navbar-toggle hidden-sm hidden-md hidden-lg\" id=\"myMenu\" href=\"\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t<i class=\"ti-menu\"></i>
\t\t\t\t</a>\t
                <span class=\"top-left-part\">
\t\t\t\t\t<a class=\"logo\" href=\"{{path('admin_index')}}\">
\t\t\t\t\t\t<h3>
\t\t\t\t\t\t<img src=\"{{asset('web/images/learn-logo.png')}}\" alt=\"home\" style=\"height: 6vh;\" />
\t\t\t\t\t</a>
\t\t\t\t</span>
                <ul class=\"nav navbar-top-links navbar-right pull-right\">
\t\t\t\t\t{% if is_granted('ROLE_USER') %}
\t\t\t\t\t<li><a  href=\"{{path('index')}}\"> <span class=\"fa fa-home\"></span> <b class=\"hidden-xs\">Acceuil</b> </a></li>
\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"\"> <span class=\"img img-circle glyphicon glyphicon-user\"></span> <b class=\"hidden-xs\">{{app.user.username}}</b> </a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated flipInY\">
\t\t\t\t\t\t\t<li><a href=\"#\" ><i class=\"ti-user\"></i> Mon Profile</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"{{path('user_show')}}\"><i class=\"ti-settings\"></i> Parametres de compte</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"{{path('logout')}}\"><i class=\"fa fa-power-off\"></i> Se déconnecter</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t\t</li>
\t\t\t\t\t{% endif %}
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
\t\t <!-- Left navbar-header -->
        {# sidebar #} 
        <div class=\"navbar-default sidebar\" role=\"navigation\">
            <div class=\"sidebar-nav navbar-collapse slimscrollsidebar\">
                <ul class=\"nav\" id=\"side-menu\">
                    <li class=\"sidebar-search hidden-sm hidden-md hidden-lg\">
                        <!-- input-group -->
                        <div class=\"input-group custom-search-form\">
                            <input type=\"text\" class=\"form-control\" placeholder=\"Search...\"> 
                            <span class=\"input-group-btn\">
                                <button class=\"btn btn-default\" type=\"button\"> <i class=\"fa fa-search\"></i> </button>
                            </span>
                         </div>
                        <!-- /input-group -->
                    </li>
\t\t\t\t\t<br/>
\t\t\t\t\t<br/>
                
                    {% if is_granted('ROLE_USER') %}
                        <li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-cog\" data-icon=\"v\"></i> Tableau de bord</a>
                        </li>

                        <li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-users\" data-icon=\"v\"></i> <span class=\"hide-menu\"> Utilisateurs <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\">1</span></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li><a href=\"{{path('admin_user_list')}}\"><i class=\"fa fa-tasks\"></i> Liste</a></li>
                            </ul>
                        </li>

                        <li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-book\" data-icon=\"v\"></i> <span class=\"hide-menu\"> Cours <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\">4</span></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li><a href=\"{{path('admin_course_list')}}\"><i class=\"fa fa-download\"></i> Liste</a></li>
                                <li role=\"separator\" class=\"divider\"></li>
                                <li><a href=\"{{path('admin_course_valide')}}\"><i class=\"fa fa-plus-circle\"></i>Actives</a></li>
                                <li role=\"separator\" class=\"divider\"></li>
                                <li><a href=\"{{path('admin_course_notvalide')}}\"><i class=\"fa fa-check-square-o\"></i>Non actives</a></li>
                            </ul>
                        </li>

\t\t\t\t\t\t<li> <a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-book\" data-icon=\"v\"></i> <span class=\"hide-menu\"> Classes de cours <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\">1</span></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li><a href=\"{{path('admin_room_list')}}\"><i class=\"fa fa-download\"></i> Liste</a></li>
                                <li role=\"separator\" class=\"divider\"></li>
                            </ul>
                        </li>       
                    {% endif %}
            </div>
        </div>
        <!-- Left navbar-header end -->
\t</div>
    {% endblock %}
\t{% block banner %}
\t{% endblock %}
    <div id=\"contenu-base-admin\">
\t{% block content %}
\t{% endblock %}
    </div>
\t<!-- footer -->
\t{% block footer %}
        <footer class=\"footer text-center\" style=\"background: #232323; font-size: 12px; color: #03a9f3; \">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"widget-body\">
                        <p class=\"text-center\">
                            Copyright &copy; E-learning 2020
                        </p>
                    </div>
                </div>
            </div>
        </footer>

\t\t<!-- jQuery -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/jquery/dist/jquery.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/js/sweetalert.min.js')}}\"></script>
\t\t<!-- Bootstrap Core JavaScript -->
\t\t<script src=\"{{asset('admin/bootstrap/dist/js/tether.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/bootstrap/dist/js/bootstrap.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js')}}\"></script>
\t\t<!-- Menu Plugin JavaScript -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}\"></script>
\t\t<!--slimscroll JavaScript -->
\t\t<script src=\"{{asset('admin/js/jquery.slimscroll.js')}}\"></script>
\t\t<!--Wave Effects -->
\t\t<script src=\"{{asset('admin/js/waves.js')}}\"></script>
\t\t<!--Counter js -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/counterup/jquery.counterup.min.js')}}\"></script>
\t\t<!--Morris JavaScript -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/raphael/raphael-min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/morrisjs/morris.js')}}\"></script>
\t\t<!-- Custom Theme JavaScript -->
\t\t<script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/js/dashboard1.js')}}\"></script>
\t\t<!-- Sparkline chart JavaScript -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/toast-master/js/jquery.toast.js')}}\"></script>
\t\t<!--Style Switcher -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}\"></script>
\t    {% block script %}{% endblock %}
\t{% endblock %}
\t
\t<!-- footer -->
</body>
{% endblock %}
</html>", "admin-base.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin-base.html.twig");
    }
}
