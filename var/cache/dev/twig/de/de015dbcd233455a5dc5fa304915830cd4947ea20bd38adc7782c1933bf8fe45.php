<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* teacher/rooms/userList.html.twig */
class __TwigTemplate_30782c31f343e5415524972268c469996649237bd71bd761a8ba465b47f242c3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "teacher-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/rooms/userList.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/rooms/userList.html.twig"));

        $this->parent = $this->loadTemplate("teacher-base.html.twig", "teacher/rooms/userList.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des élèves de la classe ";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["room"]) || array_key_exists("room", $context) ? $context["room"] : (function () { throw new RuntimeError('Variable "room" does not exist.', 10, $this->source); })()), "designation", [], "any", false, false, false, 10), "html", null, true);
        echo "</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\"> 
                            <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_by_room", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["room"]) || array_key_exists("room", $context) ? $context["room"] : (function () { throw new RuntimeError('Variable "room" does not exist.', 17, $this->source); })()), "id", [], "any", false, false, false, 17)]), "html", null, true);
        echo "\"><button class=\"btn btn-info\"> Les cours</button></a>
                            <br><br><br>
                        </div>
                        <div class=\"row\" >
                            ";
        // line 21
        $context["compteur"] = 0;
        // line 22
        echo "                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Nom d'utilisateur</th>
                                            <th style=\"color: white\" >Email</th>
                                            <th style=\"color: white\" >Nom</th>
                                            <th style=\"color: white\" >Prenoms</th>
                                            <th style=\"color: white\" >telephone</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        ";
        // line 35
        if (((isset($context["roomUsers"]) || array_key_exists("roomUsers", $context) ? $context["roomUsers"] : (function () { throw new RuntimeError('Variable "roomUsers" does not exist.', 35, $this->source); })()) != null)) {
            echo "                 
                                            ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["roomUsers"]) || array_key_exists("roomUsers", $context) ? $context["roomUsers"] : (function () { throw new RuntimeError('Variable "roomUsers" does not exist.', 36, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["roomUser"]) {
                // line 37
                echo "                                            ";
                $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 37, $this->source); })()) + 1);
                // line 38
                echo "                                                <tr>
                                                    <td style=\"color:black\">";
                // line 39
                echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 39, $this->source); })()), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "user", [], "any", false, false, false, 40), "account", [], "any", false, false, false, 40), "username", [], "any", false, false, false, 40), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 41
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "user", [], "any", false, false, false, 41), "account", [], "any", false, false, false, 41), "email", [], "any", false, false, false, 41), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 42
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "user", [], "any", false, false, false, 42), "name", [], "any", false, false, false, 42), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "user", [], "any", false, false, false, 43), "firstName", [], "any", false, false, false, 43), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 44
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "user", [], "any", false, false, false, 44), "phone", [], "any", false, false, false, 44), "html", null, true);
                echo "</td>                                                    
                                                </tr>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['roomUser'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "                                        ";
        } else {
            // line 48
            echo "                                        ";
        }
        // line 49
        echo "
                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 61
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 62
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function getRooms(id){
            console.log('good');
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 143
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_room_list");
        echo "\",
                data:{'id':id}
            }).then(function(data){
                \$('#id').html(data);
            });
        }
        function validateUser(id){
            var path=\"";
        // line 150
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_validate_or_not");
        echo "\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#validateModal'+id).modal('show');
                    \$('#unValidateModal'+id).modal('show');
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 187
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list_update");
        echo "\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
    <script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "teacher/rooms/userList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 196,  351 => 187,  311 => 150,  301 => 143,  218 => 63,  213 => 62,  203 => 61,  182 => 49,  179 => 48,  176 => 47,  167 => 44,  163 => 43,  159 => 42,  155 => 41,  151 => 40,  147 => 39,  144 => 38,  141 => 37,  137 => 36,  133 => 35,  118 => 22,  116 => 21,  109 => 17,  99 => 10,  93 => 6,  83 => 5,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'teacher-base.html.twig' %}
{%  block css %}
    <link href=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.css')}}\" rel=\"stylesheet\" type=\"text/css\" />
{% endblock %}
{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des élèves de la classe {{room.designation}}</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\"> 
                            <a href=\"{{path('teacher_course_by_room',{'id':room.id})}}\"><button class=\"btn btn-info\"> Les cours</button></a>
                            <br><br><br>
                        </div>
                        <div class=\"row\" >
                            {% set compteur = 0 %}
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Nom d'utilisateur</th>
                                            <th style=\"color: white\" >Email</th>
                                            <th style=\"color: white\" >Nom</th>
                                            <th style=\"color: white\" >Prenoms</th>
                                            <th style=\"color: white\" >telephone</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        {% if roomUsers != null %}                 
                                            {% for roomUser in roomUsers %}
                                            {% set compteur=compteur+1%}
                                                <tr>
                                                    <td style=\"color:black\">{{compteur}}</td>
                                                    <td style=\"color:black\">{{roomUser.user.account.username}}</td>
                                                    <td style=\"color:black\">{{roomUser.user.account.email}}</td>
                                                    <td style=\"color:black\">{{roomUser.user.name}}</td>
                                                    <td style=\"color:black\">{{roomUser.user.firstName}}</td>
                                                    <td style=\"color:black\">{{roomUser.user.phone}}</td>                                                    
                                                </tr>
                                            {% endfor %}
                                        {% else %}
                                        {% endif %}

                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script src=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.js')}}\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function getRooms(id){
            console.log('good');
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_user_room_list')}}\",
                data:{'id':id}
            }).then(function(data){
                \$('#id').html(data);
            });
        }
        function validateUser(id){
            var path=\"{{path('admin_user_validate_or_not')}}\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#validateModal'+id).modal('show');
                    \$('#unValidateModal'+id).modal('show');
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_user_list_update')}}\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
    <script src=\"{{asset('admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}\"></script>
{% endblock %}", "teacher/rooms/userList.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/teacher/rooms/userList.html.twig");
    }
}
