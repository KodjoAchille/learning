<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* teacher/rooms/indexLite.html.twig */
class __TwigTemplate_f0bfd5ca42c69be3bfdfc8e5e89beb31804d9888c105afb73fca7816b7edebe7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/rooms/indexLite.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/rooms/indexLite.html.twig"));

        // line 1
        if (((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new RuntimeError('Variable "rooms" does not exist.', 1, $this->source); })()) != null)) {
            echo " 
    ";
            // line 2
            $context["compteur"] = 0;
            // line 3
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new RuntimeError('Variable "rooms" does not exist.', 3, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 4
                echo "        ";
                if ((twig_get_attribute($this->env, $this->source, $context["room"], "deleted", [], "any", false, false, false, 4) != "true")) {
                    // line 5
                    echo "            ";
                    $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 5, $this->source); })()) + 1);
                    // line 6
                    echo "            <tr>
                <td style=\"color:black\">";
                    // line 7
                    echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 7, $this->source); })()), "html", null, true);
                    echo "</td>
                <td class=\"text-center\">";
                    // line 8
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "designation", [], "any", false, false, false, 8), "html", null, true);
                    echo "</td>
                <td class=\"text-nowrap\">   
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#editModal";
                    // line 10
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 10), "html", null, true);
                    echo "\" title=\"Modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal";
                    // line 11
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 11), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                </td>
            </tr>
        ";
                }
                // line 14
                echo "                                                        
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 17
            echo "    ";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "teacher/rooms/indexLite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 17,  83 => 14,  76 => 11,  72 => 10,  67 => 8,  63 => 7,  60 => 6,  57 => 5,  54 => 4,  49 => 3,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if rooms != null %} 
    {% set compteur = 0 %}
    {% for room in rooms  %}
        {% if room.deleted != \"true\" %}
            {% set compteur=compteur+1%}
            <tr>
                <td style=\"color:black\">{{compteur}}</td>
                <td class=\"text-center\">{{room.designation}}</td>
                <td class=\"text-nowrap\">   
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#editModal{{room.id}}\" title=\"Modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal{{room.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                </td>
            </tr>
        {% endif %}                                                        
    {% endfor %}
{% else %}
    {# <tr>
        <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
    </tr> #}
{% endif %}", "teacher/rooms/indexLite.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/teacher/rooms/indexLite.html.twig");
    }
}
