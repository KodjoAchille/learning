<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* courses/new.html.twig */
class __TwigTemplate_4095745142e4b6e266026531e1a84fd388aeb05b6a145650e6151efff640a6b9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'banner' => [$this, 'block_banner'],
            'header' => [$this, 'block_header'],
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base-home.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/new.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/new.html.twig"));

        $this->parent = $this->loadTemplate("base-home.html.twig", "courses/new.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 4
        $this->displayBlock('css', $context, $blocks);
        // line 24
        echo "<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 5
        echo "\t<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/css/dropify.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/image-uploader.css"), "html", null, true);
        echo "\">\t
\t<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/bootstrap.css"), "html", null, true);
        echo "\">\t
\t<style>
\t\t.modal {
\t\t\tdisplay:    none;
\t\t\tposition:   fixed;
\t\t\tz-index:    1000;
\t\t\ttop:        0;
\t\t\tleft:       0;
\t\t\theight:     100%;
\t\t\twidth:      100%;
\t\t\tbackground: rgba( 255, 255, 255, .8 ) 
\t\t\t\t\t\turl('";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/loader.gif"), "html", null, true);
        echo "') 
\t\t\t\t\t\t50% 50% 
\t\t\t\t\t\tno-repeat;
\t\t}
\t</style>\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 52
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 53
        echo "<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div id=\"loading-indicator\" class=\"modal\"></div>\t
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-2\">
\t\t\t</div>
\t\t\t<div class=\"col-8\">
\t\t\t\t<h3><b> Ajouter un cours</b></h3><br/>
\t\t\t\t<div id=\"responseMessage\" class=\"d-none\"></div>
\t\t\t\t<div id=\"courseId\" class=\"d-none\"></div>
\t\t\t\t<form  id=\"addForm\" action=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
        echo "\" method=\"POST\" enctype=\"multipart/form-data\">
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input type=\"tyle\" class=\"form-control\" id=\"title\"  placeholder=\"Titre du cours\" required=\"true\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Resumé</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<textarea required=\"true\"  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"abstract\" placeholder=\"Bref résumé du cours\"></textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Contenu</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<div id=\"videoMessage\" class=\"d-none\"></div>
\t\t\t\t\t\t\t<input type=\"file\" required=\"true\" name=\"content\" class=\"dropify-fr\" id=\"content\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Classes</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<select id=\"rooms\" name=\"rooms[]\" class=\"js-example-basic-multiple\" placeholder=\"cliquer ici\" multiple=\"multiple\" required=\"true\">
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t<button type=\"submit\" id=\"submit\"  class=\"btn btn-primary\">Soumettre</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t
    </div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 103
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 104
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/js/image-uploader.js"), "html", null, true);
        echo "\"></script>
<!-- jQuery file upload -->
<script src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/js/dropify.min.js"), "html", null, true);
        echo "\"></script>
";
        // line 108
        echo "<script>
\t\$('#rooms').load(\"";
        // line 109
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("room_user_index");
        echo "\");    
\t\$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar responseMessage=document.getElementById(\"responseMessage\");
\tvar videoMessage=document.getElementById(\"videoMessage\");
\t\$(\"#content\").change(function() {
\t\tvar file = this.files[0];
\t\tif(isVideo(file['name']))
\t\t{
\t\t\tvideoMessage.innerHTML=\"\";
\t\t\tvideoMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tvideoMessage.innerHTML=\"Le contenu doit être une video\";
\t\t\tvideoMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#content\").val('');
\t\t}
\t\t//console.log('the files', this.files)
\t});
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isVideo(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'm4v':
\t\t\tcase 'avi':
\t\t\tcase 'mpg':
\t\t\tcase 'mp4':
\t\t\t// etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\t
\t\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar file=document.getElementById('content').files[0];
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\t\$rooms=\$('#rooms').val();
\t\tif(\$content)
\t\t{
\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\tfd.append('title',\$title);
\t\t\tfd.append('abstract',\$abstract);
\t\t\tfd.append('rooms',\$rooms);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"";
        // line 211
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
        echo "\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
\t\t\t
\t\t\tif(result.data.statut==200){
\t\t\t\tvar url = '";
        // line 220
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annexe_new_form", ["id" => "courseId"]);
        echo "'; 
\t\t\t\turl = url.replace(\"courseId\", result.data.courseId);
\t\t\t
\t\t\t\tconsole.log('the url',url);
\t\t\t\twindow.location.href=url;\t

\t\t\t}
\t\t\t
\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "courses/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  396 => 220,  384 => 211,  279 => 109,  276 => 108,  272 => 106,  266 => 104,  256 => 103,  207 => 64,  194 => 53,  184 => 52,  168 => 18,  154 => 7,  150 => 6,  145 => 5,  135 => 4,  118 => 45,  110 => 40,  92 => 24,  90 => 4,  80 => 3,  62 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base-home.html.twig\" %}
{% block banner %}{% endblock %}
{% block header %}
{% block css %}
\t<link rel=\"stylesheet\" href=\"{{asset('admin/plugins/bower_components/dropify/dist/css/dropify.min.css')}}\">
\t<link rel=\"stylesheet\" href=\"{{asset('web/css/image-uploader.css')}}\">\t
\t<link rel=\"stylesheet\" href=\"{{asset('web/css/bootstrap.css')}}\">\t
\t<style>
\t\t.modal {
\t\t\tdisplay:    none;
\t\t\tposition:   fixed;
\t\t\tz-index:    1000;
\t\t\ttop:        0;
\t\t\tleft:       0;
\t\t\theight:     100%;
\t\t\twidth:      100%;
\t\t\tbackground: rgba( 255, 255, 255, .8 ) 
\t\t\t\t\t\turl('{{asset(\"web/images/loader.gif\")}}') 
\t\t\t\t\t\t50% 50% 
\t\t\t\t\t\tno-repeat;
\t\t}
\t</style>\t
{% endblock %}
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"{{asset('web/images/s2.png')}}\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"{{path('index')}}\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
{% endblock %}
{% block content %}
<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div id=\"loading-indicator\" class=\"modal\"></div>\t
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-2\">
\t\t\t</div>
\t\t\t<div class=\"col-8\">
\t\t\t\t<h3><b> Ajouter un cours</b></h3><br/>
\t\t\t\t<div id=\"responseMessage\" class=\"d-none\"></div>
\t\t\t\t<div id=\"courseId\" class=\"d-none\"></div>
\t\t\t\t<form  id=\"addForm\" action=\"{{path('course_new')}}\" method=\"POST\" enctype=\"multipart/form-data\">
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<input type=\"tyle\" class=\"form-control\" id=\"title\"  placeholder=\"Titre du cours\" required=\"true\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Resumé</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<textarea required=\"true\"  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"abstract\" placeholder=\"Bref résumé du cours\"></textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Contenu</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<div id=\"videoMessage\" class=\"d-none\"></div>
\t\t\t\t\t\t\t<input type=\"file\" required=\"true\" name=\"content\" class=\"dropify-fr\" id=\"content\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Classes</label>
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t<select id=\"rooms\" name=\"rooms[]\" class=\"js-example-basic-multiple\" placeholder=\"cliquer ici\" multiple=\"multiple\" required=\"true\">
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t<button type=\"submit\" id=\"submit\"  class=\"btn btn-primary\">Soumettre</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t
    </div>
</section>
{% endblock %}
{% block script %}
<script type=\"text/javascript\" src=\"{{asset('web/js/image-uploader.js')}}\"></script>
<!-- jQuery file upload -->
<script src=\"{{asset('admin/plugins/bower_components/dropify/dist/js/dropify.min.js')}}\"></script>
{# <script src=\"{{asset('web/js/dropzone.js')}}\"></script> #}
<script>
\t\$('#rooms').load(\"{{path('room_user_index')}}\");    
\t\$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar responseMessage=document.getElementById(\"responseMessage\");
\tvar videoMessage=document.getElementById(\"videoMessage\");
\t\$(\"#content\").change(function() {
\t\tvar file = this.files[0];
\t\tif(isVideo(file['name']))
\t\t{
\t\t\tvideoMessage.innerHTML=\"\";
\t\t\tvideoMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tvideoMessage.innerHTML=\"Le contenu doit être une video\";
\t\t\tvideoMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#content\").val('');
\t\t}
\t\t//console.log('the files', this.files)
\t});
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isVideo(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'm4v':
\t\t\tcase 'avi':
\t\t\tcase 'mpg':
\t\t\tcase 'mp4':
\t\t\t// etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\t
\t\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar file=document.getElementById('content').files[0];
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\t\$rooms=\$('#rooms').val();
\t\tif(\$content)
\t\t{
\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\tfd.append('title',\$title);
\t\t\tfd.append('abstract',\$abstract);
\t\t\tfd.append('rooms',\$rooms);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"{{path('course_new')}}\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
\t\t\t
\t\t\tif(result.data.statut==200){
\t\t\t\tvar url = '{{ path(\"annexe_new_form\", {'id': 'courseId'}) }}'; 
\t\t\t\turl = url.replace(\"courseId\", result.data.courseId);
\t\t\t
\t\t\t\tconsole.log('the url',url);
\t\t\t\twindow.location.href=url;\t

\t\t\t}
\t\t\t
\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
{% endblock %}", "courses/new.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/courses/new.html.twig");
    }
}
