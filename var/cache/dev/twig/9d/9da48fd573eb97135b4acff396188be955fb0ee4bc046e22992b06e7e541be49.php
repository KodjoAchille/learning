<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base-home.html.twig */
class __TwigTemplate_42be071f597c6812270f1383cd6935474aacb91e00cfc7e89dfba055517aa9f0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'css' => [$this, 'block_css'],
            'header' => [$this, 'block_header'],
            'underHeader' => [$this, 'block_underHeader'],
            'banner' => [$this, 'block_banner'],
            'content' => [$this, 'block_content'],
            'loader' => [$this, 'block_loader'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base-home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base-home.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 37
        echo "<body>
<!-- header -->
";
        // line 39
        $this->displayBlock('header', $context, $blocks);
        // line 103
        echo "
<!-- //header -->
";
        // line 105
        $this->displayBlock('banner', $context, $blocks);
        // line 227
        $this->displayBlock('content', $context, $blocks);
        // line 229
        echo "
<!-- //footer -->
<!-- copyright -->
<section class=\"copyright\">
\t<div class=\"container py-4\">
\t\t<div class=\"row bottom\">
\t\t\t<ul class=\"col-lg-6 links p-0\">
\t\t\t\t<li><a href=\"#why\" class=\"\">Why Choose Us</a></li>
\t\t\t\t<li><a href=\"#services\" class=\"\">Services </a></li>
\t\t\t\t<li><a href=\"#team\" class=\"\">Teachers </a></li>
\t\t\t\t<li><a href=\"#testi\" class=\"\">Testimonials </a></li>
\t\t\t</ul>
\t\t\t<div class=\"col-lg-6 copy-right p-0\">
\t\t\t\t<p class=\"\">© 2019 Child Learn. All rights reserved | Design by
\t\t\t\t\t<a href=\"#\"> W3layouts.</a>
\t\t\t\t</p><section class=\"services py-5\" id=\"courses\">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
\t      \t</div>
    \t</div>
  </div>  
</section>
<div class=\"move-top text-right\">
\t<a href=\"#home\" class=\"move-top\"> 
\t\t<span class=\"fa fa-angle-up  mb-3\" aria-hidden=\"true\"></span>
\t</a>
</div>
<script src=\"";
        // line 254
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/vendor/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/select2/dist/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/js/tether.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 257
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 259
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/sweetalert.min.js"), "html", null, true);
        echo "\"></script>
<script>
\tlet \$register=document.getElementById('registerForm');
\tlet \$connect=document.getElementById('connectForm');
\tlet \$responseMessage=document.getElementById('responseMessage');
\tfunction jsLogin(){
\t\t\$.ajax({
\t\t\t'type':'GET',
\t\t\t'dataType':'html',
\t\t\t'url':'";
        // line 268
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
        echo "'
\t\t}).then(function(data){
\t\t\t\$('#connectForm').html(data);
\t\t});
\t}
\t";
        // line 273
        $this->displayBlock('loader', $context, $blocks);
        // line 276
        echo "\t\$('#rooms').load(\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("room_index");
        echo "\");
\t\$('#completeForm').submit(function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\t\$name=\$('#name').val();
\t\t\$firstName=\$('#firstName').val();
\t\t\$phone=\$('#phone').val();
\t\t\$rooms=\$('#rooms').val();
\t\t\$type=\$('#type').val();
\t\t
\t\t\$.ajax({
\t\t\t'type':'POST',
\t\t\t'dataType':'html',
\t\t\t'url':'";
        // line 289
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_new");
        echo "',
\t\t\t'data':{'name':\$name,'firstName':\$firstName,'phone':\$phone,'type':\$type,'rooms':\$rooms}
\t\t}).then(function(){
\t\t\tvar url=\"";
        // line 292
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\";
\t\t\twindow.location=url;
\t\t});
\t\t
\t\t
\t});
\t\$('#registerForm').submit(function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\t\$email=\$('#rEmail').val();
\t\t\$username=\$('#rUsername').val();
\t\t\$password=\$('#rPassword').val();
\t\t\$rPassword=\$('#rRpassword').val();
\t\t
\t\tif(\$email && \$username && \$password && \$rPassword)
\t\t{
\t\t\tif(\$password == \$rPassword)
\t\t\t{
\t\t\t\t\$.ajax({
\t\t\t\t\t'type':'POST',
\t\t\t\t\t'dataType':'html',
\t\t\t\t\t'url':\"";
        // line 313
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("register");
        echo "\",
\t\t\t\t\t'data':{'email':\$email,'password':\$password,'username':\$username,},
\t\t\t\t}).then(function(data){
\t\t\t\t\t\$result=JSON.parse(data);
\t\t\t\t\tif(\$result.data.statut==200)
\t\t\t\t\t{
\t\t\t\t\t\tconsole.log('good');
\t\t\t\t\t\tvar url=\"";
        // line 320
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\";
\t\t\t\t\t\twindow.location=url;
\t\t\t\t\t\t\$('#responseMessage').html(`<span style=\"color:green\">super vous êtes connectées</span>`);\t\t\t\t
\t\t\t\t\t\t\$responseMessage.setAttribute('class','');
\t\t\t\t\t}
\t\t\t\t\tif(\$result.data.statut==500)
\t\t\t\t\t{
\t\t\t\t\t\t\$('#responseMessage').html(`<span style=\"color:red\">`.concat(\$result.data.message,`</span>`));\t\t\t\t
\t\t\t\t\t\t\$responseMessage.setAttribute('class','');
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t\telse
\t\t\t{
\t\t\t\t\$('#responseMessage').html(`<span style=\"color:red\">les deux mot de passe doivent correspondre</span>`);
\t\t\t\t\$responseMessage.setAttribute('class','');
\t\t\t}

\t\t}
\t\telse
\t\t{
\t\t\t\$('#responseMessage').html(`<span style=\"color:red\">veuillez remplir tous les champs</span>`);
\t\t\t\$responseMessage.setAttribute('class','');
\t\t}
\t});
\t\$(document).ready(function() {
\t\t\$('#rooms').select2();
\t});
\tfunction showRegisterOrConnectForm(){
\t\tif(\$register)
\t\t{
\t\t\tif(\$register.getAttribute('class')==\"d-none\"){
\t\t\t\$register.setAttribute('class','')
\t\t\t\$connect.setAttribute('class','d-none')
\t\t\t}
\t\t\telse{
\t\t\t\t\$register.setAttribute('class','d-none')
\t\t\t\t\$connect.setAttribute('class','')
\t\t\t}
\t\t}
\t\telse if(\$connect)
\t\t{
\t\t\tif(\$connect.getAttribute('class')==\"d-none\"){
\t\t\t\$connect.setAttribute('class','')
\t\t\t\$register.setAttribute('class','d-none')
\t\t\t}
\t\t\telse{
\t\t\t\t\$connect.setAttribute('class','d-none')
\t\t\t\t\$register.setAttribute('class','')
\t\t\t}
\t\t}
\t\t
\t}
</script>
";
        // line 374
        $this->displayBlock('script', $context, $blocks);
        // line 376
        echo "</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "<head>
\t<title>Child Learn Education Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
\t<!-- for-mobile-apps -->
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
\t<meta name=\"keywords\" content=\"Child Learn Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
\tSmartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />

    <script>
        addEventListener(\"load\", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
\t<!-- css files -->
\t";
        // line 23
        echo "\t<link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/select2/dist/css/select2.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<!-- Menu CSS -->
\t<link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/bootstrap.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' /><!-- custom css -->
\t<link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/css_slider.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" media=\"all\">
    <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"><!-- fontawesome css -->
\t<!-- //css files -->
\t";
        // line 33
        $this->displayBlock('css', $context, $blocks);
        // line 35
        echo "</head>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 33
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 34
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 39
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 40
        $this->displayBlock('underHeader', $context, $blocks);
        // line 42
        echo "<header id=\"header\" style=\"\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"index.html\">Acceuil</a></li>
\t\t\t\t";
        // line 63
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 64
            echo "\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"javascript:void(0)\">Les cours</a>
\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated flipInY\">
\t\t\t\t\t\t<li><a href=\"";
            // line 67
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_list_by_user");
            echo "\"><i class=\"fa fa-navicon\"></i>Liste</a></li>
\t\t\t\t\t\t";
            // line 68
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
                // line 69
                echo "\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li> 
\t\t\t\t\t\t<li><a href=\"";
                // line 70
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
                echo "\"><i class=\"fa fa-plus-square\"></i>Ajouter</a></li>
\t\t\t\t\t\t";
            }
            // line 72
            echo "\t\t\t\t\t</ul>
\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t</li>
\t\t\t\t";
        }
        // line 76
        echo "\t\t\t\t";
        // line 77
        echo "\t\t\t\t";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 78
            echo "\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"\"> <span class=\"img img-circle glyphicon glyphicon-user\"></span> <b class=\"hidden-xs\">";
            // line 79
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 79, $this->source); })()), "user", [], "any", false, false, false, 79), "username", [], "any", false, false, false, 79), "html", null, true);
            echo "</b> </a>
\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated \">
\t\t\t\t\t\t<li><a href=\"";
            // line 81
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_show");
            echo "\"><i class=\"ti-user\"></i> Mon Profile</a></li>
\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li> 
\t\t\t\t\t\t<li><a href=\"";
            // line 83
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\"><i class=\"fa fa-power-off\"></i>déconecter</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t</li>
\t\t\t\t";
        } else {
            // line 88
            echo "\t\t\t\t<li><a href=\"#\" title=\"Se Connecter ou s'Inscrire\"><span class=\"glyphicon glyphicon-log-in\" aria-hidden=\"true\"></span></a></li>
\t\t\t\t";
        }
        // line 90
        echo "\t\t\t\t";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 91
            echo "\t\t\t\t\t<li><a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_index");
            echo "\" title=\"Se Connecter ou s'Inscrire\"><span class=\"fa fa-cog\" aria-hidden=\"true\"></span>paramètres</a></li>\t\t\t\t\t
\t\t\t\t";
        } else {
            // line 93
            echo "\t\t\t\t\t";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
                // line 94
                echo "\t\t\t\t\t\t<li><a href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_index");
                echo "\" title=\"Se Connecter ou s'Inscrire\"><span class=\"fa fa-cog\" aria-hidden=\"true\"></span>paramètres</a></li>\t\t\t\t\t
\t\t\t\t\t";
            }
            // line 96
            echo "\t\t\t\t";
        }
        // line 97
        echo "\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 40
    public function block_underHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "underHeader"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "underHeader"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 105
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        // line 106
        echo "<!-- banner -->
<div class=\"banner\" id=\"home\">
\t<div class=\"layer\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-7 banner-text-w3pvt\">
\t\t\t\t\t<!-- banner slider-->
\t\t\t\t\t<div class=\"csslider infinity\" id=\"slider1\">
\t\t\t\t\t\t<input type=\"radio\" name=\"slides\" checked=\"checked\" id=\"slides_1\" />
\t\t\t\t\t\t<input type=\"radio\" name=\"slides\" id=\"slides_2\" />
\t\t\t\t\t\t<input type=\"radio\" name=\"slides\" id=\"slides_3\" />
\t\t\t\t\t\t<ul class=\"banner_slide_bg\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"container-fluid\">
\t\t\t\t\t\t\t\t\t<div class=\"w3ls_banner_txt\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"b-w3ltxt text-capitalize mt-md-4\">Cours en ligne</h3>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"b-w3ltxt text-capitalize mt-md-2\">L'école du partage</h4>
\t\t\t\t\t\t\t\t\t\t<p class=\"w3ls_pvt-title my-3\">onec consequat sapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus semper Nullam dui mi.
\t\t\t\t\t\t\t\t Vestibulum ante. Morbi at dui nisl.</p>
\t\t\t\t\t\t\t\t\t\t<a href=\"#about\" class=\"btn btn-banner my-3\">Read More</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"container-fluid\">
\t\t\t\t\t\t\t\t\t<div class=\"w3ls_banner_txt\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"b-w3ltxt text-capitalize mt-md-4\">Education Courses.</h3>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"b-w3ltxt text-capitalize mt-md-2\">Study For Better Future</h4>
\t\t\t\t\t\t\t\t\t\t<p class=\"w3ls_pvt-title my-3\">onec consequat sapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus semper Nullam dui mi.
\t\t\t\t\t\t\t\t Vestibulum ante. Morbi at dui nisl.</p>
\t\t\t\t\t\t\t\t\t\t<a href=\"#about\" class=\"btn btn-banner my-3\">Read More</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"container-fluid\">
\t\t\t\t\t\t\t\t\t<div class=\"w3ls_banner_txt\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"b-w3ltxt text-capitalize mt-md-4\">Education Courses.</h3>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"b-w3ltxt text-capitalize mt-md-2\">Study For Better Future</h4>
\t\t\t\t\t\t\t\t\t\t<p class=\"w3ls_pvt-title my-3\">onec consequat sapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus semper Nullam dui mi.
\t\t\t\t\t\t\t\t Vestibulum ante. Morbi at dui nisl.</p>
\t\t\t\t\t\t\t\t\t\t<a href=\"#about\" class=\"btn btn-banner my-3\">Read More</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"navigation\">
\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<label for=\"slides_1\"></label>
\t\t\t\t\t\t\t\t<label for=\"slides_2\"></label>
\t\t\t\t\t\t\t\t<label for=\"slides_3\"></label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- //banner slider-->
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-5 col-md-8 px-lg-3 px-0\">
\t\t\t\t\t<div class=\"banner-form-w3 ml-lg-5\">
\t\t\t\t\t\t\t<!-- banner form -->
\t\t\t\t\t";
        // line 165
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
            // line 166
            echo "\t\t\t\t\t";
        } else {
            // line 167
            echo "\t\t\t\t\t\t";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_STUDENT")) {
                // line 168
                echo "\t\t\t\t\t\t";
            } else {
                // line 169
                echo "\t\t\t\t\t\t\t<div class=\"padding\" id=\"formZone\">
\t\t\t\t\t\t\t";
                // line 170
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
                    // line 171
                    echo "\t\t\t\t\t\t\t\t<form id=\"completeForm\" action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t\t<h5 class=\"mb-3\">Rejoignez notre école</h5>
\t\t\t\t\t\t\t\t\t<div id=\"responseMessage\" class=\"d-none\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<span>Veuillez completer votre profil</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-style-w3ls\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Nom\" id=\"name\" name=\"name\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Prenoms\" id=\"firstName\" name=\"firstName\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Telephone\" id=\"phone\" name=\"phone\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<select id=\"type\" name=\"type\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Type de compte</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"ROLE_STUDENT\">Etudiant</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"ROLE_TEACHER\">Enseignant</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t<label>Choisissez votre classe</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"rooms\" name=\"rooms[]\" class=\"js-example-basic-multiple\" placeholder=\"cliquer ici\" multiple=\"multiple\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" Class=\"btn\"> Soumettre</button><br><br>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 195
                    echo "\t\t\t\t\t\t\t\t<form id=\"registerForm\" class=\"d-none\" action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t\t<h5 class=\"mb-3\">Rejoignez notre école</h5>
\t\t\t\t\t\t\t\t\t<div id=\"responseMessage\" class=\"d-none\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<span>Vous avez déja un compte?  <a href=\"javascript:void(0)\" onclick=\"showRegisterOrConnectForm()\">Se connecter</a></span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-style-w3ls\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Identifiant\" id=\"rUsername\" name=\"username\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Email\"  id=\"rEmail\" name=\"email\" type=\"email\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Mot de passe\" id=\"rPassword\" name=\"password\" type=\"password\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Répéter le mot de passe\" id=\"rRpassword\" name=\"rpassword\" type=\"password\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" Class=\"btn\"> Soumettre</button><br><br>
\t\t\t\t\t\t\t\t\t\t<span>En vous inscrivant, vous acceptez nos  <a href=\"#\">Conditions d'utilisations.</a></span>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t<form id=\"connectForm\" action=\"";
                    // line 211
                    echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
                    echo "\" method=\"POST\" autoComplation=\"false\" class=\"\" action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t<!-- //banner form -->
\t\t\t\t\t\t\t";
                }
                // line 215
                echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 217
            echo "\t\t\t\t\t";
        }
        // line 218
        echo "\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<!-- //banner -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 227
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 273
    public function block_loader($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "loader"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "loader"));

        // line 274
        echo "\t\twindow.onload=jsLogin;
\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 374
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base-home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  716 => 374,  705 => 274,  695 => 273,  677 => 227,  659 => 218,  656 => 217,  652 => 215,  645 => 211,  627 => 195,  601 => 171,  599 => 170,  596 => 169,  593 => 168,  590 => 167,  587 => 166,  585 => 165,  524 => 106,  514 => 105,  496 => 40,  481 => 97,  478 => 96,  472 => 94,  469 => 93,  463 => 91,  460 => 90,  456 => 88,  448 => 83,  443 => 81,  438 => 79,  435 => 78,  432 => 77,  430 => 76,  424 => 72,  419 => 70,  416 => 69,  414 => 68,  410 => 67,  405 => 64,  403 => 63,  394 => 57,  377 => 42,  375 => 40,  365 => 39,  355 => 34,  345 => 33,  334 => 35,  332 => 33,  327 => 31,  323 => 30,  319 => 29,  315 => 28,  311 => 27,  306 => 25,  302 => 24,  297 => 23,  277 => 4,  267 => 3,  256 => 376,  254 => 374,  197 => 320,  187 => 313,  163 => 292,  157 => 289,  140 => 276,  138 => 273,  130 => 268,  118 => 259,  114 => 258,  110 => 257,  106 => 256,  102 => 255,  98 => 254,  71 => 229,  69 => 227,  67 => 105,  63 => 103,  61 => 39,  57 => 37,  55 => 3,  51 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
{% block head %}
<head>
\t<title>Child Learn Education Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
\t<!-- for-mobile-apps -->
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
\t<meta name=\"keywords\" content=\"Child Learn Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
\tSmartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />

    <script>
        addEventListener(\"load\", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
\t<!-- css files -->
\t{# <link href=\"{{asset('web/css/dropzone.css')}}\" rel=\"stylesheet\"> #}
\t<link href=\"{{asset('web/select2/dist/css/select2.min.css')}}\" rel=\"stylesheet\" />
\t<link href=\"{{asset('admin/bootstrap/dist/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
\t<link href=\"{{asset('admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css')}}\" rel=\"stylesheet\">
\t<!-- Menu CSS -->
\t<link href=\"{{asset('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}\" rel=\"stylesheet\">
    <link href=\"{{asset('web/css/bootstrap.css')}}\" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href=\"{{asset('web/css/style.css')}}\" rel='stylesheet' type='text/css' /><!-- custom css -->
\t<link href=\"{{asset('web/css/css_slider.css')}}\" type=\"text/css\" rel=\"stylesheet\" media=\"all\">
    <link href=\"{{asset('web/css/font-awesome.min.css')}}\" rel=\"stylesheet\"><!-- fontawesome css -->
\t<!-- //css files -->
\t{% block css %}
\t{% endblock %}
</head>
{% endblock %}
<body>
<!-- header -->
{% block header %}
{% block underHeader %}
{% endblock %}
<header id=\"header\" style=\"\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"{{asset('web/images/s2.png')}}\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"index.html\">Acceuil</a></li>
\t\t\t\t{% if is_granted('ROLE_USER') %}
\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"javascript:void(0)\">Les cours</a>
\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated flipInY\">
\t\t\t\t\t\t<li><a href=\"{{path('course_list_by_user')}}\"><i class=\"fa fa-navicon\"></i>Liste</a></li>
\t\t\t\t\t\t{% if is_granted('ROLE_TEACHER')%}
\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li> 
\t\t\t\t\t\t<li><a href=\"{{path('course_new')}}\"><i class=\"fa fa-plus-square\"></i>Ajouter</a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t</ul>
\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t</li>
\t\t\t\t{% endif %}
\t\t\t\t{# <li class=\"\"><a href=\"#subscribe\">Souscrire</a></li> #}
\t\t\t\t{% if is_granted('ROLE_USER') %}
\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"\"> <span class=\"img img-circle glyphicon glyphicon-user\"></span> <b class=\"hidden-xs\">{{app.user.username}}</b> </a>
\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated \">
\t\t\t\t\t\t<li><a href=\"{{path('user_show')}}\"><i class=\"ti-user\"></i> Mon Profile</a></li>
\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li> 
\t\t\t\t\t\t<li><a href=\"{{path('logout')}}\"><i class=\"fa fa-power-off\"></i>déconecter</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t</li>
\t\t\t\t{% else %}
\t\t\t\t<li><a href=\"#\" title=\"Se Connecter ou s'Inscrire\"><span class=\"glyphicon glyphicon-log-in\" aria-hidden=\"true\"></span></a></li>
\t\t\t\t{% endif %}
\t\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t\t<li><a href=\"{{path('admin_index')}}\" title=\"Se Connecter ou s'Inscrire\"><span class=\"fa fa-cog\" aria-hidden=\"true\"></span>paramètres</a></li>\t\t\t\t\t
\t\t\t\t{% else %}
\t\t\t\t\t{% if is_granted('ROLE_TEACHER') %}
\t\t\t\t\t\t<li><a href=\"{{path('teacher_index')}}\" title=\"Se Connecter ou s'Inscrire\"><span class=\"fa fa-cog\" aria-hidden=\"true\"></span>paramètres</a></li>\t\t\t\t\t
\t\t\t\t\t{% endif %}
\t\t\t\t{% endif %}
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
{% endblock %}

<!-- //header -->
{% block banner %}
<!-- banner -->
<div class=\"banner\" id=\"home\">
\t<div class=\"layer\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-7 banner-text-w3pvt\">
\t\t\t\t\t<!-- banner slider-->
\t\t\t\t\t<div class=\"csslider infinity\" id=\"slider1\">
\t\t\t\t\t\t<input type=\"radio\" name=\"slides\" checked=\"checked\" id=\"slides_1\" />
\t\t\t\t\t\t<input type=\"radio\" name=\"slides\" id=\"slides_2\" />
\t\t\t\t\t\t<input type=\"radio\" name=\"slides\" id=\"slides_3\" />
\t\t\t\t\t\t<ul class=\"banner_slide_bg\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"container-fluid\">
\t\t\t\t\t\t\t\t\t<div class=\"w3ls_banner_txt\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"b-w3ltxt text-capitalize mt-md-4\">Cours en ligne</h3>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"b-w3ltxt text-capitalize mt-md-2\">L'école du partage</h4>
\t\t\t\t\t\t\t\t\t\t<p class=\"w3ls_pvt-title my-3\">onec consequat sapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus semper Nullam dui mi.
\t\t\t\t\t\t\t\t Vestibulum ante. Morbi at dui nisl.</p>
\t\t\t\t\t\t\t\t\t\t<a href=\"#about\" class=\"btn btn-banner my-3\">Read More</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"container-fluid\">
\t\t\t\t\t\t\t\t\t<div class=\"w3ls_banner_txt\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"b-w3ltxt text-capitalize mt-md-4\">Education Courses.</h3>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"b-w3ltxt text-capitalize mt-md-2\">Study For Better Future</h4>
\t\t\t\t\t\t\t\t\t\t<p class=\"w3ls_pvt-title my-3\">onec consequat sapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus semper Nullam dui mi.
\t\t\t\t\t\t\t\t Vestibulum ante. Morbi at dui nisl.</p>
\t\t\t\t\t\t\t\t\t\t<a href=\"#about\" class=\"btn btn-banner my-3\">Read More</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"container-fluid\">
\t\t\t\t\t\t\t\t\t<div class=\"w3ls_banner_txt\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"b-w3ltxt text-capitalize mt-md-4\">Education Courses.</h3>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"b-w3ltxt text-capitalize mt-md-2\">Study For Better Future</h4>
\t\t\t\t\t\t\t\t\t\t<p class=\"w3ls_pvt-title my-3\">onec consequat sapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus semper Nullam dui mi.
\t\t\t\t\t\t\t\t Vestibulum ante. Morbi at dui nisl.</p>
\t\t\t\t\t\t\t\t\t\t<a href=\"#about\" class=\"btn btn-banner my-3\">Read More</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"navigation\">
\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<label for=\"slides_1\"></label>
\t\t\t\t\t\t\t\t<label for=\"slides_2\"></label>
\t\t\t\t\t\t\t\t<label for=\"slides_3\"></label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- //banner slider-->
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-5 col-md-8 px-lg-3 px-0\">
\t\t\t\t\t<div class=\"banner-form-w3 ml-lg-5\">
\t\t\t\t\t\t\t<!-- banner form -->
\t\t\t\t\t{% if is_granted('ROLE_TEACHER')%}
\t\t\t\t\t{% else %}
\t\t\t\t\t\t{% if is_granted('ROLE_STUDENT') %}
\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t<div class=\"padding\" id=\"formZone\">
\t\t\t\t\t\t\t{% if is_granted('ROLE_USER')%}
\t\t\t\t\t\t\t\t<form id=\"completeForm\" action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t\t<h5 class=\"mb-3\">Rejoignez notre école</h5>
\t\t\t\t\t\t\t\t\t<div id=\"responseMessage\" class=\"d-none\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<span>Veuillez completer votre profil</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-style-w3ls\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Nom\" id=\"name\" name=\"name\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Prenoms\" id=\"firstName\" name=\"firstName\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Telephone\" id=\"phone\" name=\"phone\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<select id=\"type\" name=\"type\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Type de compte</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"ROLE_STUDENT\">Etudiant</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"ROLE_TEACHER\">Enseignant</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t<label>Choisissez votre classe</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"rooms\" name=\"rooms[]\" class=\"js-example-basic-multiple\" placeholder=\"cliquer ici\" multiple=\"multiple\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" Class=\"btn\"> Soumettre</button><br><br>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<form id=\"registerForm\" class=\"d-none\" action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t\t<h5 class=\"mb-3\">Rejoignez notre école</h5>
\t\t\t\t\t\t\t\t\t<div id=\"responseMessage\" class=\"d-none\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<span>Vous avez déja un compte?  <a href=\"javascript:void(0)\" onclick=\"showRegisterOrConnectForm()\">Se connecter</a></span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-style-w3ls\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Identifiant\" id=\"rUsername\" name=\"username\" type=\"text\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Email\"  id=\"rEmail\" name=\"email\" type=\"email\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Mot de passe\" id=\"rPassword\" name=\"password\" type=\"password\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<input placeholder=\"Répéter le mot de passe\" id=\"rRpassword\" name=\"rpassword\" type=\"password\" required=\"true\">
\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" Class=\"btn\"> Soumettre</button><br><br>
\t\t\t\t\t\t\t\t\t\t<span>En vous inscrivant, vous acceptez nos  <a href=\"#\">Conditions d'utilisations.</a></span>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t<form id=\"connectForm\" action=\"{{path('login')}}\" method=\"POST\" autoComplation=\"false\" class=\"\" action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t<!-- //banner form -->
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t{% endif %}
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<!-- //banner -->
{% endblock %}
{% block content %}
{% endblock %}

<!-- //footer -->
<!-- copyright -->
<section class=\"copyright\">
\t<div class=\"container py-4\">
\t\t<div class=\"row bottom\">
\t\t\t<ul class=\"col-lg-6 links p-0\">
\t\t\t\t<li><a href=\"#why\" class=\"\">Why Choose Us</a></li>
\t\t\t\t<li><a href=\"#services\" class=\"\">Services </a></li>
\t\t\t\t<li><a href=\"#team\" class=\"\">Teachers </a></li>
\t\t\t\t<li><a href=\"#testi\" class=\"\">Testimonials </a></li>
\t\t\t</ul>
\t\t\t<div class=\"col-lg-6 copy-right p-0\">
\t\t\t\t<p class=\"\">© 2019 Child Learn. All rights reserved | Design by
\t\t\t\t\t<a href=\"#\"> W3layouts.</a>
\t\t\t\t</p><section class=\"services py-5\" id=\"courses\">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
\t      \t</div>
    \t</div>
  </div>  
</section>
<div class=\"move-top text-right\">
\t<a href=\"#home\" class=\"move-top\"> 
\t\t<span class=\"fa fa-angle-up  mb-3\" aria-hidden=\"true\"></span>
\t</a>
</div>
<script src=\"{{asset('admin/vendor/jquery/jquery.min.js')}}\"></script>
<script src=\"{{asset('web/select2/dist/js/select2.min.js')}}\"></script>
<script src=\"{{asset('admin/bootstrap/dist/js/tether.min.js')}}\"></script>
<script src=\"{{asset('admin/bootstrap/dist/js/bootstrap.min.js')}}\"></script>
<script src=\"{{asset('admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js')}}\"></script>
<script src=\"{{asset('admin/js/sweetalert.min.js')}}\"></script>
<script>
\tlet \$register=document.getElementById('registerForm');
\tlet \$connect=document.getElementById('connectForm');
\tlet \$responseMessage=document.getElementById('responseMessage');
\tfunction jsLogin(){
\t\t\$.ajax({
\t\t\t'type':'GET',
\t\t\t'dataType':'html',
\t\t\t'url':'{{path('login')}}'
\t\t}).then(function(data){
\t\t\t\$('#connectForm').html(data);
\t\t});
\t}
\t{% block loader %}
\t\twindow.onload=jsLogin;
\t{% endblock %}
\t\$('#rooms').load(\"{{path('room_index')}}\");
\t\$('#completeForm').submit(function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\t\$name=\$('#name').val();
\t\t\$firstName=\$('#firstName').val();
\t\t\$phone=\$('#phone').val();
\t\t\$rooms=\$('#rooms').val();
\t\t\$type=\$('#type').val();
\t\t
\t\t\$.ajax({
\t\t\t'type':'POST',
\t\t\t'dataType':'html',
\t\t\t'url':'{{path('user_new')}}',
\t\t\t'data':{'name':\$name,'firstName':\$firstName,'phone':\$phone,'type':\$type,'rooms':\$rooms}
\t\t}).then(function(){
\t\t\tvar url=\"{{path('index')}}\";
\t\t\twindow.location=url;
\t\t});
\t\t
\t\t
\t});
\t\$('#registerForm').submit(function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\t\$email=\$('#rEmail').val();
\t\t\$username=\$('#rUsername').val();
\t\t\$password=\$('#rPassword').val();
\t\t\$rPassword=\$('#rRpassword').val();
\t\t
\t\tif(\$email && \$username && \$password && \$rPassword)
\t\t{
\t\t\tif(\$password == \$rPassword)
\t\t\t{
\t\t\t\t\$.ajax({
\t\t\t\t\t'type':'POST',
\t\t\t\t\t'dataType':'html',
\t\t\t\t\t'url':\"{{path('register')}}\",
\t\t\t\t\t'data':{'email':\$email,'password':\$password,'username':\$username,},
\t\t\t\t}).then(function(data){
\t\t\t\t\t\$result=JSON.parse(data);
\t\t\t\t\tif(\$result.data.statut==200)
\t\t\t\t\t{
\t\t\t\t\t\tconsole.log('good');
\t\t\t\t\t\tvar url=\"{{path('index')}}\";
\t\t\t\t\t\twindow.location=url;
\t\t\t\t\t\t\$('#responseMessage').html(`<span style=\"color:green\">super vous êtes connectées</span>`);\t\t\t\t
\t\t\t\t\t\t\$responseMessage.setAttribute('class','');
\t\t\t\t\t}
\t\t\t\t\tif(\$result.data.statut==500)
\t\t\t\t\t{
\t\t\t\t\t\t\$('#responseMessage').html(`<span style=\"color:red\">`.concat(\$result.data.message,`</span>`));\t\t\t\t
\t\t\t\t\t\t\$responseMessage.setAttribute('class','');
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t\telse
\t\t\t{
\t\t\t\t\$('#responseMessage').html(`<span style=\"color:red\">les deux mot de passe doivent correspondre</span>`);
\t\t\t\t\$responseMessage.setAttribute('class','');
\t\t\t}

\t\t}
\t\telse
\t\t{
\t\t\t\$('#responseMessage').html(`<span style=\"color:red\">veuillez remplir tous les champs</span>`);
\t\t\t\$responseMessage.setAttribute('class','');
\t\t}
\t});
\t\$(document).ready(function() {
\t\t\$('#rooms').select2();
\t});
\tfunction showRegisterOrConnectForm(){
\t\tif(\$register)
\t\t{
\t\t\tif(\$register.getAttribute('class')==\"d-none\"){
\t\t\t\$register.setAttribute('class','')
\t\t\t\$connect.setAttribute('class','d-none')
\t\t\t}
\t\t\telse{
\t\t\t\t\$register.setAttribute('class','d-none')
\t\t\t\t\$connect.setAttribute('class','')
\t\t\t}
\t\t}
\t\telse if(\$connect)
\t\t{
\t\t\tif(\$connect.getAttribute('class')==\"d-none\"){
\t\t\t\$connect.setAttribute('class','')
\t\t\t\$register.setAttribute('class','d-none')
\t\t\t}
\t\t\telse{
\t\t\t\t\$connect.setAttribute('class','d-none')
\t\t\t\t\$register.setAttribute('class','')
\t\t\t}
\t\t}
\t\t
\t}
</script>
{% block script %}
{% endblock %}
</body>
</html>", "base-home.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/base-home.html.twig");
    }
}
