<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* teacher-base.html.twig */
class __TwigTemplate_5dcd52f8487bd904a018219f321bd517d4e15f9989d4d4456491d137c2c29e9e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'header' => [$this, 'block_header'],
            'banner' => [$this, 'block_banner'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher-base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher-base.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html>
\t<head>
    ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 31
        echo "\t</head>

<body >
";
        // line 34
        $this->displayBlock('body', $context, $blocks);
        // line 167
        echo "</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 5
        echo "\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<meta name=\"description\" content=\"\">
\t\t<meta name=\"author\" content=\"\">
\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\">
\t\t<title>E-learning</title>
\t\t<!-- Bootstrap Core CSS -->
\t\t<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/personnel_swt.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- Menu CSS -->
\t\t<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- toast CSS -->
\t\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/toast-master/css/jquery.toast.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- morris CSS -->
\t\t<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/morrisjs/morris.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- animation CSS -->
\t\t<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- Custom CSS -->
\t\t<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<link href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/personnel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t<!-- color CSS -->
\t\t<link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/css/colors/gray.css"), "html", null, true);
        echo "\" id=\"theme\" rel=\"stylesheet\">
\t\t";
        // line 29
        $this->displayBlock('css', $context, $blocks);
        // line 30
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 34
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 35
        echo "<!-- header -->
";
        // line 36
        $this->displayBlock('header', $context, $blocks);
        // line 109
        echo "\t";
        $this->displayBlock('banner', $context, $blocks);
        // line 111
        echo "    <div id=\"contenu-base-admin\">
\t";
        // line 112
        $this->displayBlock('content', $context, $blocks);
        // line 114
        echo "    </div>
\t<!-- footer -->
\t";
        // line 116
        $this->displayBlock('footer', $context, $blocks);
        // line 163
        echo "\t
\t<!-- footer -->
</body>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 37
        echo "\t<!-- Preloader -->
    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div> 
    ";
        // line 46
        echo "    <div id=\"wrapper\">
\t\t<!-- Navigation -->
        <nav class=\"navbar  navbar-default navbar-static-top m-b-0\">
            <div class=\"navbar-header\"> 
\t\t\t\t<a class=\"navbar-toggle hidden-sm hidden-md hidden-lg\" id=\"myMenu\" href=\"\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t<i class=\"ti-menu\"></i>
\t\t\t\t</a>\t
                <span class=\"top-left-part\">
\t\t\t\t\t<a class=\"logo\" href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_index");
        echo "\">
\t\t\t\t\t\t<h3>
\t\t\t\t\t\t<img src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/learn-logo.png"), "html", null, true);
        echo "\" alt=\"home\" style=\"height: 6vh;\" />
\t\t\t\t\t</a>
\t\t\t\t</span>
                <ul class=\"nav navbar-top-links navbar-right pull-right\">
\t\t\t\t\t";
        // line 60
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 61
            echo "\t\t\t\t\t<li><a  href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
            echo "\"> <span class=\"fa fa-home\"></span> <b class=\"hidden-xs\">Acceuil</b> </a></li>
\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"\"> <span class=\"img img-circle glyphicon glyphicon-user\"></span> <b class=\"hidden-xs\">";
            // line 63
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 63, $this->source); })()), "user", [], "any", false, false, false, 63), "username", [], "any", false, false, false, 63), "html", null, true);
            echo "</b> </a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated flipInY\">
\t\t\t\t\t\t\t<li><a href=\"#\" ><i class=\"ti-user\"></i> Mon Profile</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 67
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_show");
            echo "\"><i class=\"ti-settings\"></i> Parametres de compte</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 69
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\"><i class=\"fa fa-power-off\"></i> Se déconnecter</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t\t</li>
\t\t\t\t\t";
        }
        // line 74
        echo "                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
\t\t <!-- Left navbar-header -->
        ";
        // line 82
        echo " 
        <div class=\"navbar-default sidebar\" role=\"navigation\">
            <div class=\"sidebar-nav navbar-collapse slimscrollsidebar\">
                <ul class=\"nav\" id=\"side-menu\">
                    <li class=\"sidebar-search hidden-sm hidden-md hidden-lg\">
                        <!-- input-group -->
                        <div class=\"input-group custom-search-form\">
                            <input type=\"text\" class=\"form-control\" placeholder=\"Search...\"> 
                            <span class=\"input-group-btn\">
                                <button class=\"btn btn-default\" type=\"button\"> <i class=\"fa fa-search\"></i> </button>
                            </span>
                         </div>
                        <!-- /input-group -->
                    </li>
\t\t\t\t\t<br/>
\t\t\t\t\t<br/>
                    ";
        // line 98
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
            // line 99
            echo "\t\t\t\t\t\t<li> <a href=\"#\" class=\"waves-effect\"> Classes de cours <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\"></span></span></a>
                            <ul id=\"rooms\" class=\"nav nav-second-level\">
                            </ul>
                        </li>       
                    ";
        }
        // line 104
        echo "            </div>
        </div>
        <!-- Left navbar-header end -->
\t</div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 109
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        // line 110
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 112
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 113
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 116
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 117
        echo "        <footer class=\"footer text-center\" style=\"background: #232323; font-size: 12px; color: #03a9f3; \">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"widget-body\">
                        <p class=\"text-center\">
                            Copyright &copy; E-learning 2020
                        </p>
                    </div>
                </div>
            </div>
        </footer>

\t\t<!-- jQuery -->
\t\t<script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/sweetalert.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Bootstrap Core JavaScript -->
\t\t<script src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/js/tether.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Menu Plugin JavaScript -->
\t\t<script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!--slimscroll JavaScript -->
\t\t<script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Wave Effects -->
\t\t<script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/waves.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Counter js -->
\t\t<script src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/waypoints/lib/jquery.waypoints.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/counterup/jquery.counterup.min.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Morris JavaScript -->
\t\t<script src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/raphael/raphael-min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/morrisjs/morris.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Custom Theme JavaScript -->
\t\t<script src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/dashboard1.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- Sparkline chart JavaScript -->
\t\t<script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/toast-master/js/jquery.toast.js"), "html", null, true);
        echo "\"></script>
\t\t<!--Style Switcher -->
\t\t<script src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"), "html", null, true);
        echo "\"></script>
\t\t<script>
\t\t\t\$('#rooms').load(\"";
        // line 158
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_room_display_name");
        echo "\");
\t\t</script>
\t    ";
        // line 160
        $this->displayBlock('script', $context, $blocks);
        // line 162
        echo "\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 160
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 161
        echo "\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "teacher-base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  507 => 161,  497 => 160,  487 => 162,  485 => 160,  480 => 158,  475 => 156,  470 => 154,  466 => 153,  462 => 152,  457 => 150,  453 => 149,  448 => 147,  444 => 146,  439 => 144,  435 => 143,  430 => 141,  425 => 139,  420 => 137,  415 => 135,  411 => 134,  407 => 133,  402 => 131,  398 => 130,  383 => 117,  373 => 116,  363 => 113,  353 => 112,  343 => 110,  333 => 109,  319 => 104,  312 => 99,  310 => 98,  292 => 82,  282 => 74,  274 => 69,  269 => 67,  262 => 63,  256 => 61,  254 => 60,  247 => 56,  242 => 54,  232 => 46,  226 => 37,  216 => 36,  203 => 163,  201 => 116,  197 => 114,  195 => 112,  192 => 111,  189 => 109,  187 => 36,  184 => 35,  174 => 34,  156 => 29,  146 => 30,  144 => 29,  140 => 28,  135 => 26,  131 => 25,  126 => 23,  121 => 21,  116 => 19,  111 => 17,  106 => 15,  102 => 14,  98 => 13,  92 => 10,  85 => 5,  75 => 4,  65 => 167,  63 => 34,  58 => 31,  56 => 4,  51 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE HTML>
<html>
\t<head>
    {%  block head %}
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<meta name=\"description\" content=\"\">
\t\t<meta name=\"author\" content=\"\">
\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"{{asset('web/images/s2.png')}}\">
\t\t<title>E-learning</title>
\t\t<!-- Bootstrap Core CSS -->
\t\t<link href=\"{{asset('admin/bootstrap/dist/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
\t\t<link href=\"{{asset('admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css')}}\" rel=\"stylesheet\">
\t\t<link href=\"{{asset('admin/css/personnel_swt.css')}}\" rel=\"stylesheet\">
\t\t<!-- Menu CSS -->
\t\t<link href=\"{{asset('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}\" rel=\"stylesheet\">
\t\t<!-- toast CSS -->
\t\t<link href=\"{{asset('admin/plugins/bower_components/toast-master/css/jquery.toast.css')}}\" rel=\"stylesheet\">
\t\t<!-- morris CSS -->
\t\t<link href=\"{{asset('admin/plugins/bower_components/morrisjs/morris.css')}}\" rel=\"stylesheet\">
\t\t<!-- animation CSS -->
\t\t<link href=\"{{asset('admin/css/animate.css')}}\" rel=\"stylesheet\">
\t\t<!-- Custom CSS -->
\t\t<link href=\"{{asset('admin/css/style.css')}}\" rel=\"stylesheet\">
\t\t<link href=\"{{asset('admin/css/personnel.css')}}\" rel=\"stylesheet\">
\t\t<!-- color CSS -->
\t\t<link href=\"{{asset('admin/css/colors/gray.css')}}\" id=\"theme\" rel=\"stylesheet\">
\t\t{%  block css %}{% endblock %}
\t{% endblock %}
\t</head>

<body >
{% block body %}
<!-- header -->
{% block header %}
\t<!-- Preloader -->
    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div> 
    {# <style>
        .navbar-header {
            border-bottom: 2px solid #03a9f3;
        }
    </style> #}
    <div id=\"wrapper\">
\t\t<!-- Navigation -->
        <nav class=\"navbar  navbar-default navbar-static-top m-b-0\">
            <div class=\"navbar-header\"> 
\t\t\t\t<a class=\"navbar-toggle hidden-sm hidden-md hidden-lg\" id=\"myMenu\" href=\"\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t<i class=\"ti-menu\"></i>
\t\t\t\t</a>\t
                <span class=\"top-left-part\">
\t\t\t\t\t<a class=\"logo\" href=\"{{path('teacher_index')}}\">
\t\t\t\t\t\t<h3>
\t\t\t\t\t\t<img src=\"{{asset('web/images/learn-logo.png')}}\" alt=\"home\" style=\"height: 6vh;\" />
\t\t\t\t\t</a>
\t\t\t\t</span>
                <ul class=\"nav navbar-top-links navbar-right pull-right\">
\t\t\t\t\t{% if is_granted('ROLE_USER') %}
\t\t\t\t\t<li><a  href=\"{{path('index')}}\"> <span class=\"fa fa-home\"></span> <b class=\"hidden-xs\">Acceuil</b> </a></li>
\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t<a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"\"> <span class=\"img img-circle glyphicon glyphicon-user\"></span> <b class=\"hidden-xs\">{{app.user.username}}</b> </a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user animated flipInY\">
\t\t\t\t\t\t\t<li><a href=\"#\" ><i class=\"ti-user\"></i> Mon Profile</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"{{path('user_show')}}\"><i class=\"ti-settings\"></i> Parametres de compte</a></li>
\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"{{path('logout')}}\"><i class=\"fa fa-power-off\"></i> Se déconnecter</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<!-- /.dropdown-user -->
\t\t\t\t\t</li>
\t\t\t\t\t{% endif %}
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
\t\t <!-- Left navbar-header -->
        {# sidebar #} 
        <div class=\"navbar-default sidebar\" role=\"navigation\">
            <div class=\"sidebar-nav navbar-collapse slimscrollsidebar\">
                <ul class=\"nav\" id=\"side-menu\">
                    <li class=\"sidebar-search hidden-sm hidden-md hidden-lg\">
                        <!-- input-group -->
                        <div class=\"input-group custom-search-form\">
                            <input type=\"text\" class=\"form-control\" placeholder=\"Search...\"> 
                            <span class=\"input-group-btn\">
                                <button class=\"btn btn-default\" type=\"button\"> <i class=\"fa fa-search\"></i> </button>
                            </span>
                         </div>
                        <!-- /input-group -->
                    </li>
\t\t\t\t\t<br/>
\t\t\t\t\t<br/>
                    {% if is_granted('ROLE_TEACHER') %}
\t\t\t\t\t\t<li> <a href=\"#\" class=\"waves-effect\"> Classes de cours <span class=\"fa arrow\"></span> <span class=\"label label-rouded label-custom pull-right\"></span></span></a>
                            <ul id=\"rooms\" class=\"nav nav-second-level\">
                            </ul>
                        </li>       
                    {% endif %}
            </div>
        </div>
        <!-- Left navbar-header end -->
\t</div>
    {% endblock %}
\t{% block banner %}
\t{% endblock %}
    <div id=\"contenu-base-admin\">
\t{% block content %}
\t{% endblock %}
    </div>
\t<!-- footer -->
\t{% block footer %}
        <footer class=\"footer text-center\" style=\"background: #232323; font-size: 12px; color: #03a9f3; \">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"widget-body\">
                        <p class=\"text-center\">
                            Copyright &copy; E-learning 2020
                        </p>
                    </div>
                </div>
            </div>
        </footer>

\t\t<!-- jQuery -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/jquery/dist/jquery.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/js/sweetalert.min.js')}}\"></script>
\t\t<!-- Bootstrap Core JavaScript -->
\t\t<script src=\"{{asset('admin/bootstrap/dist/js/tether.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/bootstrap/dist/js/bootstrap.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js')}}\"></script>
\t\t<!-- Menu Plugin JavaScript -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}\"></script>
\t\t<!--slimscroll JavaScript -->
\t\t<script src=\"{{asset('admin/js/jquery.slimscroll.js')}}\"></script>
\t\t<!--Wave Effects -->
\t\t<script src=\"{{asset('admin/js/waves.js')}}\"></script>
\t\t<!--Counter js -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/counterup/jquery.counterup.min.js')}}\"></script>
\t\t<!--Morris JavaScript -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/raphael/raphael-min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/morrisjs/morris.js')}}\"></script>
\t\t<!-- Custom Theme JavaScript -->
\t\t<script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/js/dashboard1.js')}}\"></script>
\t\t<!-- Sparkline chart JavaScript -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js')}}\"></script>
\t\t<script src=\"{{asset('admin/plugins/bower_components/toast-master/js/jquery.toast.js')}}\"></script>
\t\t<!--Style Switcher -->
\t\t<script src=\"{{asset('admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}\"></script>
\t\t<script>
\t\t\t\$('#rooms').load(\"{{path('teacher_room_display_name')}}\");
\t\t</script>
\t    {% block script %}
\t\t{% endblock %}
\t{% endblock %}
\t
\t<!-- footer -->
</body>
{% endblock %}
</html>", "teacher-base.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/teacher-base.html.twig");
    }
}
