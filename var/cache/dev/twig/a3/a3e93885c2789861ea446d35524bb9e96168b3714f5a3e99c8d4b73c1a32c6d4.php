<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/courses/index.html.twig */
class __TwigTemplate_2036e20be05caae521d5e639dd34e00d6fa3f4a696b9f2dadffd8f3c20af2620 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/index.html.twig"));

        $this->parent = $this->loadTemplate("admin-base.html.twig", "admin/courses/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des cours</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Auteur</th>
                                            <th style=\"color: white\" >Titre</th>
                                            <th style=\"color: white\" >Résumé</th>
                                            <th style=\"color: white\" >Classe</th>
                                            <th style=\"color: white\" >Commentaire</th>
                                            <th style=\"color: white\" >Contenu</th>
                                            <th style=\"color: white\" >Annexe</th>
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th> 
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        ";
        // line 34
        if (((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 34, $this->source); })()) != null)) {
            echo " 
                                            ";
            // line 35
            $context["compteur"] = 0;
            // line 36
            echo "                                            
                                            ";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 37, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
                // line 38
                echo "                                                ";
                if ((twig_get_attribute($this->env, $this->source, $context["course"], "deleted", [], "any", false, false, false, 38) != "true")) {
                    // line 39
                    echo "                                                    ";
                    $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 39, $this->source); })()) + 1);
                    // line 40
                    echo "                                                    <tr>
                                                        <td style=\"color:black\">";
                    // line 41
                    echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 41, $this->source); })()), "html", null, true);
                    echo "</td>
                                                        <td class=\"text-center\">";
                    // line 42
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 42), "firstName", [], "any", false, false, false, 42), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 42), "name", [], "any", false, false, false, 42), "html", null, true);
                    echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#teacherModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 42), "html", null, true);
                    echo "\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a></td>
                                                        <td style=\"color:black\">";
                    // line 43
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "title", [], "any", false, false, false, 43), "html", null, true);
                    echo "</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal";
                    // line 44
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 44), "html", null, true);
                    echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td style=\"color:black\">
                                                            ";
                    // line 46
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "courseRooms", [], "any", false, false, false, 46));
                    foreach ($context['_seq'] as $context["_key"] => $context["courseRoom"]) {
                        // line 47
                        echo "                                                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "room", [], "any", false, false, false, 47), "designation", [], "any", false, false, false, 47), "html", null, true);
                        echo "
                                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['courseRoom'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 49
                    echo "                                                        </td>
                                                        <td class=\"text-center\"> 
                                                            ";
                    // line 51
                    if (twig_get_attribute($this->env, $this->source, $context["course"], "comment", [], "any", false, false, false, 51)) {
                        // line 52
                        echo "                                                                <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 52), "html", null, true);
                        echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                                                            ";
                    } else {
                        // line 54
                        echo "                                                            ";
                    }
                    // line 55
                    echo "                                                        </td>
                                                        <td class=\"text-center\"> <a href=\"";
                    // line 56
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_show", ["id" => twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 56)]), "html", null, true);
                    echo "\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
                                                        <td style=\"color:black\">
                                                            ";
                    // line 58
                    if ((twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 58) != null)) {
                        // line 59
                        echo "                                                                ";
                        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 59)), "html", null, true);
                        echo "
                                                                ";
                        // line 60
                        $context["break"] = false;
                        // line 61
                        echo "                                                                ";
                        $context["i"] = 0;
                        // line 62
                        echo "                                                                ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 62));
                        foreach ($context['_seq'] as $context["_key"] => $context["annexe"]) {
                            if ( !(isset($context["break"]) || array_key_exists("break", $context) ? $context["break"] : (function () { throw new RuntimeError('Variable "break" does not exist.', 62, $this->source); })())) {
                                // line 63
                                echo "                                                                    ";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "title", [], "any", false, false, false, 63), "html", null, true);
                                echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 63), "html", null, true);
                                echo "\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                                                                     <div id=\"annexeModal";
                                // line 64
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 64), "html", null, true);
                                echo "\" class=\"modal fade\" role=\"dialog\">
                                                                        <div i class=\"modal-dialog\" >
                                                                            <div class=\"modal-content\">
                                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                                        <div class=\"row\">
                                                                                            <div class=\"col-md-2\">
                                                                                            </div>
                                                                                            <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                                                                        </div> 
                                                                                    </span> 
                                                                                </div>
                                                                                <div class=\"modal-body\">
                                                                                    ";
                                // line 78
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "annexes", [], "any", false, false, false, 78));
                                foreach ($context['_seq'] as $context["_key"] => $context["currentAnnexe"]) {
                                    // line 79
                                    echo "                                                                                    <div class=\"row\">
                                                                                        <div class=\"col-6\">
                                                                                            ";
                                    // line 81
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "title", [], "any", false, false, false, 81), "html", null, true);
                                    echo "
                                                                                        </div>
                                                                                        <div class=\"col-6\">
                                                                                            ";
                                    // line 84
                                    if (twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 84)) {
                                        // line 85
                                        echo "                                                                                                <a href=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 85))), "html", null, true);
                                        echo "\"  download=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 85))), "html", null, true);
                                        echo "\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                                                            ";
                                    }
                                    // line 87
                                    echo "                                                                                        </div>
                                                                                    </div>
                                                                                   ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currentAnnexe'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 90
                                echo "                                                                                </div>
                                                                                <div class=\"modal-footer\">
                                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                        </div>
                                                                    </div>
                                                                    ";
                                // line 99
                                $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 99, $this->source); })()) + 1);
                                // line 100
                                echo "                                                                    ";
                                if (((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 100, $this->source); })()) == 1)) {
                                    // line 101
                                    echo "                                                                        ";
                                    $context["break"] = true;
                                    // line 102
                                    echo "                                                                    ";
                                }
                                // line 103
                                echo "                                                                ";
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annexe'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 104
                        echo "                                                            ";
                    } else {
                        // line 105
                        echo "                                                                0
                                                            ";
                    }
                    // line 107
                    echo "                                                        </td>
                                                        <td class=\"text-nowrap\">   
                                                            <a href=\"";
                    // line 109
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_edit_form", ["id" => twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 109)]), "html", null, true);
                    echo "\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal";
                    // line 110
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 110), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                                                            ";
                    // line 111
                    if ((twig_get_attribute($this->env, $this->source, $context["course"], "validated", [], "any", false, false, false, 111) != "true")) {
                        // line 112
                        echo "                                                                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 112), "html", null, true);
                        echo "\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                                                            ";
                    } else {
                        // line 114
                        echo "                                                                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 114), "html", null, true);
                        echo "\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                                                            ";
                    }
                    // line 116
                    echo "                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#commentModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 116), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-comment\"></i></span> </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"commentModal";
                    // line 120
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 120), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div id=\"responseMessage";
                    // line 134
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 134), "html", null, true);
                    echo "\" class=\"d-none\"></div>
                                                                            <form id=\"commentForm\" method=\"POST\">    
                                                                                <textarea required=\"true\"  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"comment";
                    // line 136
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 136), "html", null, true);
                    echo "\" placeholder=\"Ajouter un commentaire sur le cours\"></textarea>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"beforeAddComment(";
                    // line 142
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 142), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"unValidateModal";
                    // line 149
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 149), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 155
                    echo "Voulez vous vraiment désactiver le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse(";
                    // line 160
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 160), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal";
                    // line 167
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 167), "html", null, true);
                    echo "\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 173
                    echo "Voulez vous vraiment activer le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse(";
                    // line 178
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 178), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"deleteModal";
                    // line 185
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 185), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 191
                    echo "Voulez vous vraiment supprimer le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteCourse(";
                    // line 196
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 196), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"abstractModal";
                    // line 205
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 205), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Résumé du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            ";
                    // line 221
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "abstract", [], "any", false, false, false, 221), "html", null, true);
                    echo "
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>

                                                    <div id=\"showCommentModal";
                    // line 235
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 235), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            ";
                    // line 251
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "comment", [], "any", false, false, false, 251), "html", null, true);
                    echo "
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                   
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"teacherModal";
                    // line 267
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 267), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Information sur le créateur du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Email
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 288
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 288), "account", [], "any", false, false, false, 288), "email", [], "any", false, false, false, 288), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Nom
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 296
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 296), "name", [], "any", false, false, false, 296), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Prenoms
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 304
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 304), "firstName", [], "any", false, false, false, 304), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Telephone
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 312
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "teacher", [], "any", false, false, false, 312), "phone", [], "any", false, false, false, 312), "html", null, true);
                    echo "
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Type de compte
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    ";
                    // line 320
                    if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
                        // line 321
                        echo "                                                                                        ";
                        echo "ENSEIGNANT";
                        echo "
                                                                                    ";
                    } else {
                        // line 323
                        echo "                                                                                        ";
                        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_STUDENT")) {
                            // line 324
                            echo "                                                                                            ";
                            echo "ETUDIANT";
                            echo "
                                                                                        ";
                        }
                        // line 326
                        echo "                                                                                    ";
                    }
                    // line 327
                    echo "                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal --> 
                                                ";
                }
                // line 342
                echo "                                                        
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 344
            echo "                                        ";
        } else {
            // line 345
            echo "                                            ";
            // line 348
            echo "                                        ";
        }
        // line 349
        echo "
                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 361
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 362
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script src=\"";
        // line 363
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function beforeAddComment(id)
        {
            var comment=\$(\"#comment\"+id).val()
            if(comment!=\"\")
            {
                addComment(comment,id);
            }
            else
            {
                document.getElementById('responseMessage'+id).innerHTML=\"veuillez remplir le champ\";
                document.getElementById('responseMessage'+id).setAttribute('class','alert alert-danger');
                //affichage de message d'erreur
            }
        }
        function addComment(comment,id)
        {
            var currentId=id;
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:\"";
        // line 458
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_edit");
        echo "\",
                data:{'comment':comment,'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#commentModal'+currentId).modal('hide');
                    swal({
                        text: \"commentaire ajoutéé avec succes\",
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#commentModal'+currentId).modal('show');
                }
                else
                {
                    console.log('the message',result.data.message);
                }
            });
        }
        function deleteCourse(id){
            var path=\"";
        // line 480
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_delete");
        echo "\";
            AjaxFunction(id,path);
        }
        function validateCourse(id){
            var path=\"";
        // line 484
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_validate_or_not");
        echo "\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    console.log('good modal');
                    showModals();
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function showModals()
        {
            \$('#validateModal'+id).modal('show');
            \$('#unValidateModal'+id).modal('show');
            console.log(\"showModals called\")
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 527
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_list_update");
        echo "\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/courses/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  876 => 527,  830 => 484,  823 => 480,  798 => 458,  700 => 363,  695 => 362,  685 => 361,  664 => 349,  661 => 348,  659 => 345,  656 => 344,  649 => 342,  631 => 327,  628 => 326,  622 => 324,  619 => 323,  613 => 321,  611 => 320,  600 => 312,  589 => 304,  578 => 296,  567 => 288,  543 => 267,  524 => 251,  505 => 235,  488 => 221,  469 => 205,  457 => 196,  449 => 191,  440 => 185,  430 => 178,  422 => 173,  413 => 167,  403 => 160,  395 => 155,  386 => 149,  376 => 142,  367 => 136,  362 => 134,  345 => 120,  337 => 116,  331 => 114,  325 => 112,  323 => 111,  319 => 110,  315 => 109,  311 => 107,  307 => 105,  304 => 104,  297 => 103,  294 => 102,  291 => 101,  288 => 100,  286 => 99,  275 => 90,  267 => 87,  259 => 85,  257 => 84,  251 => 81,  247 => 79,  243 => 78,  226 => 64,  219 => 63,  213 => 62,  210 => 61,  208 => 60,  203 => 59,  201 => 58,  196 => 56,  193 => 55,  190 => 54,  184 => 52,  182 => 51,  178 => 49,  169 => 47,  165 => 46,  160 => 44,  156 => 43,  148 => 42,  144 => 41,  141 => 40,  138 => 39,  135 => 38,  131 => 37,  128 => 36,  126 => 35,  122 => 34,  93 => 7,  83 => 6,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin-base.html.twig' %}
{%  block css %}
    <link href=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.css')}}\" rel=\"stylesheet\" type=\"text/css\" />
{% endblock %}

{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des cours</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Auteur</th>
                                            <th style=\"color: white\" >Titre</th>
                                            <th style=\"color: white\" >Résumé</th>
                                            <th style=\"color: white\" >Classe</th>
                                            <th style=\"color: white\" >Commentaire</th>
                                            <th style=\"color: white\" >Contenu</th>
                                            <th style=\"color: white\" >Annexe</th>
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th> 
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        {% if courses != null %} 
                                            {% set compteur = 0 %}
                                            
                                            {% for course in courses  %}
                                                {% if course.deleted != \"true\" %}
                                                    {% set compteur=compteur+1%}
                                                    <tr>
                                                        <td style=\"color:black\">{{compteur}}</td>
                                                        <td class=\"text-center\">{{course.teacher.firstName}}{{\" \"}}{{course.teacher.name}}<a href=\"#\" data-toggle=\"modal\" data-target=\"#teacherModal{{course.id}}\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a></td>
                                                        <td style=\"color:black\">{{course.title}}</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal{{course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td style=\"color:black\">
                                                            {% for courseRoom in course.courseRooms %}
                                                                {{courseRoom.room.designation}}
                                                            {% endfor %}
                                                        </td>
                                                        <td class=\"text-center\"> 
                                                            {% if course.comment %}
                                                                <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal{{course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                                                            {% else %}
                                                            {% endif %}
                                                        </td>
                                                        <td class=\"text-center\"> <a href=\"{{path('admin_course_show',{'id':course.id})}}\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
                                                        <td style=\"color:black\">
                                                            {% if course.annexes != null %}
                                                                {{course.annexes|length }}
                                                                {% set break = false %}
                                                                {% set i = 0 %}
                                                                {% for annexe in course.annexes if not break %}
                                                                    {{annexe.title}}<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal{{annexe.id}}\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                                                                     <div id=\"annexeModal{{annexe.id}}\" class=\"modal fade\" role=\"dialog\">
                                                                        <div i class=\"modal-dialog\" >
                                                                            <div class=\"modal-content\">
                                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                                        <div class=\"row\">
                                                                                            <div class=\"col-md-2\">
                                                                                            </div>
                                                                                            <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                                                                        </div> 
                                                                                    </span> 
                                                                                </div>
                                                                                <div class=\"modal-body\">
                                                                                    {% for currentAnnexe in course.annexes %}
                                                                                    <div class=\"row\">
                                                                                        <div class=\"col-6\">
                                                                                            {{currentAnnexe.title}}
                                                                                        </div>
                                                                                        <div class=\"col-6\">
                                                                                            {% if currentAnnexe.file %}
                                                                                                <a href=\"{{asset('data/' ~ currentAnnexe.file)}}\"  download=\"{{asset('data/' ~ currentAnnexe.file)}}\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                                                            {% endif %}
                                                                                        </div>
                                                                                    </div>
                                                                                   {% endfor %}
                                                                                </div>
                                                                                <div class=\"modal-footer\">
                                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                        </div>
                                                                    </div>
                                                                    {% set i = i+1 %}
                                                                    {% if i==1 %}
                                                                        {% set break = true %}
                                                                    {% endif %}
                                                                {% endfor %}
                                                            {% else %}
                                                                0
                                                            {% endif %}
                                                        </td>
                                                        <td class=\"text-nowrap\">   
                                                            <a href=\"{{path('admin_course_edit_form',{'id':course.id})}}\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal{{course.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                                                            {% if course.validated != \"true\" %}
                                                                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal{{course.id}}\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                                                            {% else %}
                                                                <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal{{course.id}}\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                                                            {% endif %}
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#commentModal{{course.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-comment\"></i></span> </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"commentModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div id=\"responseMessage{{course.id}}\" class=\"d-none\"></div>
                                                                            <form id=\"commentForm\" method=\"POST\">    
                                                                                <textarea required=\"true\"  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"comment{{course.id}}\" placeholder=\"Ajouter un commentaire sur le cours\"></textarea>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"beforeAddComment({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"unValidateModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment désactiver le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal{{course.id}}\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment activer le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"deleteModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment supprimer le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"abstractModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Résumé du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            {{course.abstract}}
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>

                                                    <div id=\"showCommentModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            {{course.comment}}
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                   
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"teacherModal{{course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Information sur le créateur du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Email
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.account.email}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Nom
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.name}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Prenoms
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.firstName}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Telephone
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {{course.teacher.phone}}
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"row\">
                                                                                <div class=\"col-6\">
                                                                                    Type de compte
                                                                                </div>
                                                                                <div class=\"col-6\">
                                                                                    {% if is_granted('ROLE_TEACHER') %}
                                                                                        {{'ENSEIGNANT'}}
                                                                                    {% else %}
                                                                                        {% if is_granted('ROLE_STUDENT')%}
                                                                                            {{'ETUDIANT'}}
                                                                                        {% endif %}
                                                                                    {% endif %}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal --> 
                                                {% endif %}                                                        
                                            {% endfor %}
                                        {% else %}
                                            {# <tr>
                                                <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
                                            </tr> #}
                                        {% endif %}

                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script src=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.js')}}\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function beforeAddComment(id)
        {
            var comment=\$(\"#comment\"+id).val()
            if(comment!=\"\")
            {
                addComment(comment,id);
            }
            else
            {
                document.getElementById('responseMessage'+id).innerHTML=\"veuillez remplir le champ\";
                document.getElementById('responseMessage'+id).setAttribute('class','alert alert-danger');
                //affichage de message d'erreur
            }
        }
        function addComment(comment,id)
        {
            var currentId=id;
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:\"{{path('admin_course_edit')}}\",
                data:{'comment':comment,'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#commentModal'+currentId).modal('hide');
                    swal({
                        text: \"commentaire ajoutéé avec succes\",
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#commentModal'+currentId).modal('show');
                }
                else
                {
                    console.log('the message',result.data.message);
                }
            });
        }
        function deleteCourse(id){
            var path=\"{{path('admin_course_delete')}}\";
            AjaxFunction(id,path);
        }
        function validateCourse(id){
            var path=\"{{path('admin_course_validate_or_not')}}\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    console.log('good modal');
                    showModals();
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function showModals()
        {
            \$('#validateModal'+id).modal('show');
            \$('#unValidateModal'+id).modal('show');
            console.log(\"showModals called\")
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_course_list_update')}}\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
{% endblock %}", "admin/courses/index.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/courses/index.html.twig");
    }
}
