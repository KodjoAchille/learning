<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/courses/show.html.twig */
class __TwigTemplate_31aa8df57c8df14a47e0f2c7950239865e084396df13121d6e2250cc5172a9e0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/show.html.twig"));

        $this->parent = $this->loadTemplate("admin-base.html.twig", "admin/courses/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Contenu du cours/<a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_list");
        echo "\" class=\"btn btn-default\">Retour</a></h4> 
                </div>
            </div>
            <section class=\"other_services py-5\">
                <div id=\"courseId\" class=\"d-none\">";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 13, $this->source); })()), "id", [], "any", false, false, false, 13), "html", null, true);
        echo "</div>                
                <div class=\"white-box\">
                    <div class=\"container py-lg-5 py-3\">
                        <div class=\"row\">
                            <div class=\"col-lg-8 col-md-12\">
                                <h4 class=\"\"><img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s1.png"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 18, $this->source); })()), "title", [], "any", false, false, false, 18), "html", null, true);
        echo "</h4>
                            </div>
                            <div class=\"col-lg-4 col-md-6\">
                                <div class=\"row\">
                                    <div class=\"col-6\">
                                    <div class=\"heading mb-sm-5 mb-4 search-container\">
                                        <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#commentModal\"  class=\"btn btn-primary\">Donner votre avis</button>                
                                    </div>
                                    </div>
                                    <div id=\"validateOrNot\" class=\"col-6\">
                                        ";
        // line 28
        if ((twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 28, $this->source); })()), "validated", [], "any", false, false, false, 28) != "true")) {
            // line 29
            echo "                                            <div class=\"heading mb-sm-5 mb-4 search-container\">
                                                <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#validateModal\"  class=\"btn btn-success\">Activer le cours</button>                
                                            </div>
                                        ";
        } else {
            // line 33
            echo "                                            <div class=\"heading mb-sm-5 mb-4 search-container\">
                                                <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#unValidateModal\"  class=\"btn btn-danger\">Désactiver le cours</button>                
                                            </div>
                                        ";
        }
        // line 37
        echo "                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        ";
        // line 43
        echo "                        <div class=\"row\">
                            <div class=\"col-lg-2 col-md-2\">
                            </div>
                            <div class=\"col-lg-8 col-md-8\">
                                <div class=\"grid\">
                                    <video width=\"800\" height=\"400\"  controls>
                                        <source src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 49, $this->source); })()), "content", [], "any", false, false, false, 49))), "html", null, true);
        echo "\" type=\"video/mp4\"/>
                                        Le fichier video ne peut pas être lu
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class=\"services py-5\">
                <div class=\"white-box\">
                    <div class=\"container\">
                        <h3 class=\"heading mb-5\">Résumé</h3>
                        <div class=\"row ml-sm-5\">
                            <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-12\">
                                <div class=\"our-services-wrapper mb-60\">
                                    <div class=\"services-inner\">
                                        <div class=\"our-services-text\">
                                            ";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 67, $this->source); })()), "abstract", [], "any", false, false, false, 67), "html", null, true);
        echo "
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
            <section class=\"services py-5\">
                <div class=\"white-box\">
                    <div class=\"container\">
                        <h3 class=\"heading mb-5\">les documents annexes</h3>
                        <div class=\"row ml-sm-5\">
                            ";
        // line 81
        if ((twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 81, $this->source); })()), "annexes", [], "any", false, false, false, 81) != null)) {
            // line 82
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 82, $this->source); })()), "annexes", [], "any", false, false, false, 82));
            foreach ($context['_seq'] as $context["_key"] => $context["annexe"]) {
                // line 83
                echo "                                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-12\">
                                        <div class=\"our-services-wrapper mb-60\">
                                            <div class=\"services-inner\">
                                                <div class=\"our-services-img\">
                                                    <img src=\"";
                // line 87
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s1.png"), "html", null, true);
                echo "\" alt=\"\">
                                                </div>
                                                <div class=\"our-services-text\">
                                                    <h4>";
                // line 90
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "title", [], "any", false, false, false, 90), "html", null, true);
                echo "</h4>
                                                    <a style=\"align:center\" href=\"";
                // line 91
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["annexe"], "file", [], "any", false, false, false, 91))), "html", null, true);
                echo "\" download=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["annexe"], "file", [], "any", false, false, false, 91))), "html", null, true);
                echo "\" type=\"button\" ><i class=\"fa fa-download\"></i>Télécharger</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annexe'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 97
            echo "                            ";
        }
        // line 98
        echo "                        </div>
                        <!-- positioned image -->
                        <div class=\"position-image\">
                            <img src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/services.png"), "html", null, true);
        echo "\" alt=\"\" class=\"img-fluid\">
                        </div>
                        <!-- //positioned image -->
                    </div>
                </div>
            </section> 
        </div>
    </div>
    <div id=\"commentModal\" class=\"modal fade\" role=\"dialog\">
        <div i class=\"modal-dialog\" >
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                    <span class=\"modal-title\" id=\"myModalLabel\">
                        <div class=\"row\">
                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                        </div> 
                    </span> 
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <div id=\"responseMessage\" class=\"d-none\"></div>
                            <form id=\"commentForm\" method=\"POST\">    
                                <textarea required=\"true\"  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"comment\" placeholder=\"Ajouter un commentaire sur le cours\"></textarea>
                            </form>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"addCommentBtn\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- Modal -->
    <div id=\"unValidateModal\" class=\"modal fade\" role=\"dialog\">
        <div class=\"modal-dialog modal-dialog-centered\" >
            <div class=\"modal-header\" style=\"background-color:red\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                <span class=\"modal-title\" id=\"myModalLabel\">
                    <div class=\"row\">
                        <h4 style=\"color:white\" ><strong >confirmation de désactivation</strong></h4>
                    </div> 
                </span> 
            </div>
            <div class=\"modal-content\"  >
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            ";
        // line 153
        echo "Voulez vous vraiment désactiver le cours ?";
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" onclick=\"validateCourse(";
        // line 158
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 158, $this->source); })()), "id", [], "any", false, false, false, 158), "html", null, true);
        echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div id=\"validateModal\"  class=\"modal fade\" role=\"dialog\">
        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
             <div class=\"modal-header\" style=\"background-color:#32D7AA\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                <span class=\"modal-title\" id=\"myModalLabel\">
                    <div class=\"row\">
                        <h4 style=\"color:white\" ><strong >confirmation d'activation</strong></h4>
                    </div> 
                </span> 
            </div>
            <div class=\"modal-content\" >
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            ";
        // line 179
        echo "Voulez vous vraiment activer le cours ?";
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" onclick=\"validateCourse(";
        // line 184
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 184, $this->source); })()), "id", [], "any", false, false, false, 184), "html", null, true);
        echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
     ";
        // line 192
        echo "    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 194
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 195
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
<script>
    \$(\"#addCommentBtn\").click(function(e){
        var comment=\$(\"#comment\").val()
        if(comment!=\"\")
        {
            var id=\$('#courseId').html();
            addComment(comment,id);
        }
        else
        {
            responseMessage.innerHTML=\"veuillez remplir le champ\";
            responseMessage.setAttribute('class','alert alert-danger');
            //affichage de message d'erreur
        }
    });
    function addComment(comment,id)
    {
        \$.ajax({
            type:'POST',
            dataType:'html',
            url:\"";
        // line 216
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_edit");
        echo "\",
            data:{'comment':comment,'id':id},
        }).then(function(data){
            var result=JSON.parse(data);
            if(result.data.statut==200)
            {
                \$('#commentModal').modal('hide');
                swal({
                    text: \"commentaire ajoutéé avec succes\",
                    icon: \"success\",
                    buttons: \"ok\",
                });
            }
            else
            {
                console.log('the message',result.data.message);
            }
        });
    }
    function deleteCourse(id){
        var path=\"";
        // line 236
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_delete");
        echo "\";
        AjaxFunction(id,path);
    }
    function validateCourse(id){
        var path=\"";
        // line 240
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_validate_or_not");
        echo "\";
        AjaxFunction(id,path);
    }
    function AjaxFunction(id,path){
        \$.ajax({
            type:'POST',
            dataType:'html',
            url:path,
            data:{'id':id},
        }).then(function(data){
            var result=JSON.parse(data);
            if(result.data.statut==200)
            {
                \$('#unValidateModal').modal('hide');
                \$('#validateModal').modal('hide');
                swal({
                    text: result.data.message,
                    icon: \"success\",
                    buttons: \"ok\",
                });
                console.log(result);
                \$('#unValidateModal').modal('show');
                \$('#validateModal').modal('show');
                if(result.data.validated==true)
                {
                    \$('#validateOrNot').html(`
                        <div class=\"heading mb-sm-5 mb-4 search-container\">
                            <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#unValidateModal\"  class=\"btn btn-danger\">Désactiver le cours</button>                
                        </div>
                    `);    
                }
                else
                {
                    \$('#validateOrNot').html(`
                        <div class=\"heading mb-sm-5 mb-4 search-container\">
                            <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#validateModal\"  class=\"btn btn-success\">Activer le cours</button>                
                        </div>
                    `);  
                }
            }
            else
            {
                console.log(result.data.message);
            }
        });
    }
</script>
    <!--Style Switcher -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/courses/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 240,  409 => 236,  386 => 216,  361 => 195,  351 => 194,  341 => 192,  331 => 184,  323 => 179,  299 => 158,  291 => 153,  236 => 101,  231 => 98,  228 => 97,  214 => 91,  210 => 90,  204 => 87,  198 => 83,  193 => 82,  191 => 81,  174 => 67,  153 => 49,  145 => 43,  138 => 37,  132 => 33,  126 => 29,  124 => 28,  109 => 18,  101 => 13,  94 => 9,  88 => 5,  78 => 4,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin-base.html.twig' %}
{%  block css %}
{% endblock %}
{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Contenu du cours/<a href=\"{{path('admin_course_list')}}\" class=\"btn btn-default\">Retour</a></h4> 
                </div>
            </div>
            <section class=\"other_services py-5\">
                <div id=\"courseId\" class=\"d-none\">{{course.id}}</div>                
                <div class=\"white-box\">
                    <div class=\"container py-lg-5 py-3\">
                        <div class=\"row\">
                            <div class=\"col-lg-8 col-md-12\">
                                <h4 class=\"\"><img src=\"{{asset('web/images/s1.png')}}\" class=\"img-fluid\" alt=\"\">{{course.title}}</h4>
                            </div>
                            <div class=\"col-lg-4 col-md-6\">
                                <div class=\"row\">
                                    <div class=\"col-6\">
                                    <div class=\"heading mb-sm-5 mb-4 search-container\">
                                        <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#commentModal\"  class=\"btn btn-primary\">Donner votre avis</button>                
                                    </div>
                                    </div>
                                    <div id=\"validateOrNot\" class=\"col-6\">
                                        {% if course.validated != 'true'%}
                                            <div class=\"heading mb-sm-5 mb-4 search-container\">
                                                <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#validateModal\"  class=\"btn btn-success\">Activer le cours</button>                
                                            </div>
                                        {% else %}
                                            <div class=\"heading mb-sm-5 mb-4 search-container\">
                                                <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#unValidateModal\"  class=\"btn btn-danger\">Désactiver le cours</button>                
                                            </div>
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        {# <h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3> #}
                        <div class=\"row\">
                            <div class=\"col-lg-2 col-md-2\">
                            </div>
                            <div class=\"col-lg-8 col-md-8\">
                                <div class=\"grid\">
                                    <video width=\"800\" height=\"400\"  controls>
                                        <source src=\"{{asset('data/' ~ course.content)}}\" type=\"video/mp4\"/>
                                        Le fichier video ne peut pas être lu
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class=\"services py-5\">
                <div class=\"white-box\">
                    <div class=\"container\">
                        <h3 class=\"heading mb-5\">Résumé</h3>
                        <div class=\"row ml-sm-5\">
                            <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-12\">
                                <div class=\"our-services-wrapper mb-60\">
                                    <div class=\"services-inner\">
                                        <div class=\"our-services-text\">
                                            {{course.abstract}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
            <section class=\"services py-5\">
                <div class=\"white-box\">
                    <div class=\"container\">
                        <h3 class=\"heading mb-5\">les documents annexes</h3>
                        <div class=\"row ml-sm-5\">
                            {% if course.annexes != null %}
                                {% for annexe in course.annexes %}
                                    <div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-12\">
                                        <div class=\"our-services-wrapper mb-60\">
                                            <div class=\"services-inner\">
                                                <div class=\"our-services-img\">
                                                    <img src=\"{{asset('web/images/s1.png')}}\" alt=\"\">
                                                </div>
                                                <div class=\"our-services-text\">
                                                    <h4>{{annexe.title}}</h4>
                                                    <a style=\"align:center\" href=\"{{asset('data/' ~ annexe.file)}}\" download=\"{{asset('data/' ~ annexe.file)}}\" type=\"button\" ><i class=\"fa fa-download\"></i>Télécharger</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {% endfor %}
                            {% endif %}
                        </div>
                        <!-- positioned image -->
                        <div class=\"position-image\">
                            <img src=\"{{asset('web/images/services.png')}}\" alt=\"\" class=\"img-fluid\">
                        </div>
                        <!-- //positioned image -->
                    </div>
                </div>
            </section> 
        </div>
    </div>
    <div id=\"commentModal\" class=\"modal fade\" role=\"dialog\">
        <div i class=\"modal-dialog\" >
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                    <span class=\"modal-title\" id=\"myModalLabel\">
                        <div class=\"row\">
                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                        </div> 
                    </span> 
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <div id=\"responseMessage\" class=\"d-none\"></div>
                            <form id=\"commentForm\" method=\"POST\">    
                                <textarea required=\"true\"  rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"comment\" placeholder=\"Ajouter un commentaire sur le cours\"></textarea>
                            </form>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" id=\"addCommentBtn\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- Modal -->
    <div id=\"unValidateModal\" class=\"modal fade\" role=\"dialog\">
        <div class=\"modal-dialog modal-dialog-centered\" >
            <div class=\"modal-header\" style=\"background-color:red\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                <span class=\"modal-title\" id=\"myModalLabel\">
                    <div class=\"row\">
                        <h4 style=\"color:white\" ><strong >confirmation de désactivation</strong></h4>
                    </div> 
                </span> 
            </div>
            <div class=\"modal-content\"  >
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            {{\"Voulez vous vraiment désactiver le cours ?\"}}
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" onclick=\"validateCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div id=\"validateModal\"  class=\"modal fade\" role=\"dialog\">
        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
             <div class=\"modal-header\" style=\"background-color:#32D7AA\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                <span class=\"modal-title\" id=\"myModalLabel\">
                    <div class=\"row\">
                        <h4 style=\"color:white\" ><strong >confirmation d'activation</strong></h4>
                    </div> 
                </span> 
            </div>
            <div class=\"modal-content\" >
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            {{\"Voulez vous vraiment activer le cours ?\"}}
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" onclick=\"validateCourse({{course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
     {# modal #}
    {# end modal #}
{%  endblock %}
{% block script %}
<script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
<script>
    \$(\"#addCommentBtn\").click(function(e){
        var comment=\$(\"#comment\").val()
        if(comment!=\"\")
        {
            var id=\$('#courseId').html();
            addComment(comment,id);
        }
        else
        {
            responseMessage.innerHTML=\"veuillez remplir le champ\";
            responseMessage.setAttribute('class','alert alert-danger');
            //affichage de message d'erreur
        }
    });
    function addComment(comment,id)
    {
        \$.ajax({
            type:'POST',
            dataType:'html',
            url:\"{{path('admin_course_edit')}}\",
            data:{'comment':comment,'id':id},
        }).then(function(data){
            var result=JSON.parse(data);
            if(result.data.statut==200)
            {
                \$('#commentModal').modal('hide');
                swal({
                    text: \"commentaire ajoutéé avec succes\",
                    icon: \"success\",
                    buttons: \"ok\",
                });
            }
            else
            {
                console.log('the message',result.data.message);
            }
        });
    }
    function deleteCourse(id){
        var path=\"{{path('admin_course_delete')}}\";
        AjaxFunction(id,path);
    }
    function validateCourse(id){
        var path=\"{{path('admin_course_validate_or_not')}}\";
        AjaxFunction(id,path);
    }
    function AjaxFunction(id,path){
        \$.ajax({
            type:'POST',
            dataType:'html',
            url:path,
            data:{'id':id},
        }).then(function(data){
            var result=JSON.parse(data);
            if(result.data.statut==200)
            {
                \$('#unValidateModal').modal('hide');
                \$('#validateModal').modal('hide');
                swal({
                    text: result.data.message,
                    icon: \"success\",
                    buttons: \"ok\",
                });
                console.log(result);
                \$('#unValidateModal').modal('show');
                \$('#validateModal').modal('show');
                if(result.data.validated==true)
                {
                    \$('#validateOrNot').html(`
                        <div class=\"heading mb-sm-5 mb-4 search-container\">
                            <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#unValidateModal\"  class=\"btn btn-danger\">Désactiver le cours</button>                
                        </div>
                    `);    
                }
                else
                {
                    \$('#validateOrNot').html(`
                        <div class=\"heading mb-sm-5 mb-4 search-container\">
                            <button style=\"align:center\" data-toggle=\"modal\" data-target=\"#validateModal\"  class=\"btn btn-success\">Activer le cours</button>                
                        </div>
                    `);  
                }
            }
            else
            {
                console.log(result.data.message);
            }
        });
    }
</script>
    <!--Style Switcher -->
{% endblock %}", "admin/courses/show.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/courses/show.html.twig");
    }
}
