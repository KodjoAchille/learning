<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* teacher/courses/byRoom.html.twig */
class __TwigTemplate_34d527028118b9f715041eecd832b8b6f1a08d2562a094cac2b127b12ab1225c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "teacher-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/courses/byRoom.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/courses/byRoom.html.twig"));

        $this->parent = $this->loadTemplate("teacher-base.html.twig", "teacher/courses/byRoom.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">les cours de la classe ";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["room"]) || array_key_exists("room", $context) ? $context["room"] : (function () { throw new RuntimeError('Variable "room" does not exist.', 10, $this->source); })()), "designation", [], "any", false, false, false, 10), "html", null, true);
        echo "</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\"> 
                            <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_room_user_list", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["room"]) || array_key_exists("room", $context) ? $context["room"] : (function () { throw new RuntimeError('Variable "room" does not exist.', 17, $this->source); })()), "id", [], "any", false, false, false, 17)]), "html", null, true);
        echo "\"><button class=\"btn btn-info\"> Les élèves</button></a>
                            <br><br><br>
                        </div>
                        <div class=\"row\" >
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Titre</th>
                                            <th style=\"color: white\" >Résumé</th>
                                            <th style=\"color: white\" >Commentaire</th>
                                            <th style=\"color: white\" >Contenu</th>
                                            <th style=\"color: white\" >Annexe</th>
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th> 
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        ";
        // line 35
        if (((isset($context["courseRooms"]) || array_key_exists("courseRooms", $context) ? $context["courseRooms"] : (function () { throw new RuntimeError('Variable "courseRooms" does not exist.', 35, $this->source); })()) != null)) {
            echo " 
                                            ";
            // line 36
            $context["compteur"] = 0;
            // line 37
            echo "                                            
                                            ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courseRooms"]) || array_key_exists("courseRooms", $context) ? $context["courseRooms"] : (function () { throw new RuntimeError('Variable "courseRooms" does not exist.', 38, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["courseRoom"]) {
                // line 39
                echo "                                                ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 39), "deleted", [], "any", false, false, false, 39) != "true")) {
                    // line 40
                    echo "                                                    ";
                    $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 40, $this->source); })()) + 1);
                    // line 41
                    echo "                                                    <tr>
                                                        <td style=\"color:black\">";
                    // line 42
                    echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 42, $this->source); })()), "html", null, true);
                    echo "</td>
                                                        <td style=\"color:black\">";
                    // line 43
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 43), "title", [], "any", false, false, false, 43), "html", null, true);
                    echo "</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal";
                    // line 44
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 44), "id", [], "any", false, false, false, 44), "html", null, true);
                    echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td class=\"text-center\"> 
                                                            ";
                    // line 46
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 46), "comment", [], "any", false, false, false, 46)) {
                        // line 47
                        echo "                                                                <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 47), "id", [], "any", false, false, false, 47), "html", null, true);
                        echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                                                            ";
                    } else {
                        // line 49
                        echo "                                                            ";
                    }
                    // line 50
                    echo "                                                        </td>
                                                        <td class=\"text-center\"> <a href=\"";
                    // line 51
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 51), "id", [], "any", false, false, false, 51)]), "html", null, true);
                    echo "\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
                                                        <td style=\"color:black\">
                                                            ";
                    // line 53
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 53), "annexes", [], "any", false, false, false, 53) != null)) {
                        // line 54
                        echo "                                                                ";
                        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 54), "annexes", [], "any", false, false, false, 54)), "html", null, true);
                        echo "
                                                                ";
                        // line 55
                        $context["break"] = false;
                        // line 56
                        echo "                                                                ";
                        $context["i"] = 0;
                        // line 57
                        echo "                                                                ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 57), "annexes", [], "any", false, false, false, 57));
                        foreach ($context['_seq'] as $context["_key"] => $context["annexe"]) {
                            if ( !(isset($context["break"]) || array_key_exists("break", $context) ? $context["break"] : (function () { throw new RuntimeError('Variable "break" does not exist.', 57, $this->source); })())) {
                                // line 58
                                echo "                                                                    ";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "title", [], "any", false, false, false, 58), "html", null, true);
                                echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 58), "html", null, true);
                                echo "\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                                                                     <div id=\"annexeModal";
                                // line 59
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 59), "html", null, true);
                                echo "\" class=\"modal fade\" role=\"dialog\">
                                                                        <div i class=\"modal-dialog\" >
                                                                            <div class=\"modal-content\">
                                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                                        <div class=\"row\">
                                                                                            <div class=\"col-md-2\">
                                                                                            </div>
                                                                                            <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                                                                        </div> 
                                                                                    </span> 
                                                                                </div>
                                                                                <div class=\"modal-body\">
                                                                                    ";
                                // line 73
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 73), "annexes", [], "any", false, false, false, 73));
                                foreach ($context['_seq'] as $context["_key"] => $context["currentAnnexe"]) {
                                    // line 74
                                    echo "                                                                                    <div class=\"row\">
                                                                                        <div class=\"col-6\">
                                                                                            ";
                                    // line 76
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "title", [], "any", false, false, false, 76), "html", null, true);
                                    echo "
                                                                                        </div>
                                                                                        <div class=\"col-6\">
                                                                                            ";
                                    // line 79
                                    if (twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 79)) {
                                        // line 80
                                        echo "                                                                                                <a href=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 80))), "html", null, true);
                                        echo "\"  download=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 80))), "html", null, true);
                                        echo "\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                                                            ";
                                    }
                                    // line 82
                                    echo "                                                                                        </div>
                                                                                    </div>
                                                                                   ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currentAnnexe'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 85
                                echo "                                                                                </div>
                                                                                <div class=\"modal-footer\">
                                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                        </div>
                                                                    </div>
                                                                    ";
                                // line 94
                                $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 94, $this->source); })()) + 1);
                                // line 95
                                echo "                                                                    ";
                                if (((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 95, $this->source); })()) == 1)) {
                                    // line 96
                                    echo "                                                                        ";
                                    $context["break"] = true;
                                    // line 97
                                    echo "                                                                    ";
                                }
                                // line 98
                                echo "                                                                ";
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annexe'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 99
                        echo "                                                            ";
                    } else {
                        // line 100
                        echo "                                                                0
                                                            ";
                    }
                    // line 102
                    echo "                                                        </td>
                                                        <td class=\"text-nowrap\">   
                                                            <a href=\"";
                    // line 104
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_edit_form", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 104), "id", [], "any", false, false, false, 104)]), "html", null, true);
                    echo "\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal";
                    // line 105
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 105), "id", [], "any", false, false, false, 105), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"deleteModal";
                    // line 109
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 109), "id", [], "any", false, false, false, 109), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 115
                    echo "Voulez vous vraiment supprimer le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteCourse(";
                    // line 120
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 120), "id", [], "any", false, false, false, 120), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"abstractModal";
                    // line 129
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 129), "id", [], "any", false, false, false, 129), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Résumé du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            ";
                    // line 145
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 145), "abstract", [], "any", false, false, false, 145), "html", null, true);
                    echo "
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"showCommentModal";
                    // line 158
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 158), "id", [], "any", false, false, false, 158), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            ";
                    // line 174
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 174), "comment", [], "any", false, false, false, 174), "html", null, true);
                    echo "
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                   
                                                ";
                }
                // line 188
                echo "                                                        
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['courseRoom'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 190
            echo "                                        ";
        } else {
            // line 191
            echo "                                            ";
            // line 194
            echo "                                        ";
        }
        // line 195
        echo "
                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 207
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 208
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function deleteCourse(id){
            var path=\"";
        // line 285
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_delete");
        echo "\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    console.log('good modal');
                    showModals();
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function showModals()
        {
            \$('#validateModal'+id).modal('show');
            \$('#unValidateModal'+id).modal('show');
            console.log(\"showModals called\")
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 325
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_list_update");
        echo "\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "teacher/courses/byRoom.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  583 => 325,  540 => 285,  461 => 209,  456 => 208,  446 => 207,  425 => 195,  422 => 194,  420 => 191,  417 => 190,  410 => 188,  392 => 174,  373 => 158,  357 => 145,  338 => 129,  326 => 120,  318 => 115,  309 => 109,  302 => 105,  298 => 104,  294 => 102,  290 => 100,  287 => 99,  280 => 98,  277 => 97,  274 => 96,  271 => 95,  269 => 94,  258 => 85,  250 => 82,  242 => 80,  240 => 79,  234 => 76,  230 => 74,  226 => 73,  209 => 59,  202 => 58,  196 => 57,  193 => 56,  191 => 55,  186 => 54,  184 => 53,  179 => 51,  176 => 50,  173 => 49,  167 => 47,  165 => 46,  160 => 44,  156 => 43,  152 => 42,  149 => 41,  146 => 40,  143 => 39,  139 => 38,  136 => 37,  134 => 36,  130 => 35,  109 => 17,  99 => 10,  93 => 6,  83 => 5,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'teacher-base.html.twig' %}
{%  block css %}
    <link href=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.css')}}\" rel=\"stylesheet\" type=\"text/css\" />
{% endblock %}
{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">les cours de la classe {{room.designation}}</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\"> 
                            <a href=\"{{path('teacher_room_user_list',{'id':room.id})}}\"><button class=\"btn btn-info\"> Les élèves</button></a>
                            <br><br><br>
                        </div>
                        <div class=\"row\" >
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Titre</th>
                                            <th style=\"color: white\" >Résumé</th>
                                            <th style=\"color: white\" >Commentaire</th>
                                            <th style=\"color: white\" >Contenu</th>
                                            <th style=\"color: white\" >Annexe</th>
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th> 
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        {% if courseRooms != null %} 
                                            {% set compteur = 0 %}
                                            
                                            {% for courseRoom in courseRooms  %}
                                                {% if courseRoom.course.deleted != \"true\" %}
                                                    {% set compteur=compteur+1%}
                                                    <tr>
                                                        <td style=\"color:black\">{{compteur}}</td>
                                                        <td style=\"color:black\">{{courseRoom.course.title}}</td>
                                                        <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal{{courseRoom.course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                                                        <td class=\"text-center\"> 
                                                            {% if courseRoom.course.comment %}
                                                                <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal{{courseRoom.course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                                                            {% else %}
                                                            {% endif %}
                                                        </td>
                                                        <td class=\"text-center\"> <a href=\"{{path('teacher_course_show',{'id':courseRoom.course.id})}}\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
                                                        <td style=\"color:black\">
                                                            {% if courseRoom.course.annexes != null %}
                                                                {{courseRoom.course.annexes|length }}
                                                                {% set break = false %}
                                                                {% set i = 0 %}
                                                                {% for annexe in courseRoom.course.annexes if not break %}
                                                                    {{annexe.title}}<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal{{annexe.id}}\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                                                                     <div id=\"annexeModal{{annexe.id}}\" class=\"modal fade\" role=\"dialog\">
                                                                        <div i class=\"modal-dialog\" >
                                                                            <div class=\"modal-content\">
                                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                                        <div class=\"row\">
                                                                                            <div class=\"col-md-2\">
                                                                                            </div>
                                                                                            <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                                                                        </div> 
                                                                                    </span> 
                                                                                </div>
                                                                                <div class=\"modal-body\">
                                                                                    {% for currentAnnexe in courseRoom.course.annexes %}
                                                                                    <div class=\"row\">
                                                                                        <div class=\"col-6\">
                                                                                            {{currentAnnexe.title}}
                                                                                        </div>
                                                                                        <div class=\"col-6\">
                                                                                            {% if currentAnnexe.file %}
                                                                                                <a href=\"{{asset('data/' ~ currentAnnexe.file)}}\"  download=\"{{asset('data/' ~ currentAnnexe.file)}}\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                                                            {% endif %}
                                                                                        </div>
                                                                                    </div>
                                                                                   {% endfor %}
                                                                                </div>
                                                                                <div class=\"modal-footer\">
                                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                        </div>
                                                                    </div>
                                                                    {% set i = i+1 %}
                                                                    {% if i==1 %}
                                                                        {% set break = true %}
                                                                    {% endif %}
                                                                {% endfor %}
                                                            {% else %}
                                                                0
                                                            {% endif %}
                                                        </td>
                                                        <td class=\"text-nowrap\">   
                                                            <a href=\"{{path('teacher_course_edit_form',{'id':courseRoom.course.id})}}\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal{{courseRoom.course.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"deleteModal{{courseRoom.course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment supprimer le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteCourse({{courseRoom.course.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                    <!-- Modal -->
                                                    <div id=\"abstractModal{{courseRoom.course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Résumé du cours</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            {{courseRoom.course.abstract}}
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"showCommentModal{{courseRoom.course.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-2\">
                                                                            </div>
                                                                            <h4 style=\"color:white\" ><strong >Commentaire</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-6\">
                                                                            {{courseRoom.course.comment}}
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                   
                                                {% endif %}                                                        
                                            {% endfor %}
                                        {% else %}
                                            {# <tr>
                                                <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
                                            </tr> #}
                                        {% endif %}

                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script src=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.js')}}\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function deleteCourse(id){
            var path=\"{{path('teacher_course_delete')}}\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    console.log('good modal');
                    showModals();
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function showModals()
        {
            \$('#validateModal'+id).modal('show');
            \$('#unValidateModal'+id).modal('show');
            console.log(\"showModals called\")
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('teacher_course_list_update')}}\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
{% endblock %}", "teacher/courses/byRoom.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/teacher/courses/byRoom.html.twig");
    }
}
