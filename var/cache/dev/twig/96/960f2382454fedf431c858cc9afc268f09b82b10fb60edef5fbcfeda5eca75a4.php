<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* teacher/rooms/displayName.html.twig */
class __TwigTemplate_65f4bf84d77ff448ac79f45037f6226955001d2ccf4fb225d10fd48ebf3a1880 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/rooms/displayName.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/rooms/displayName.html.twig"));

        // line 1
        if (((isset($context["roomUsers"]) || array_key_exists("roomUsers", $context) ? $context["roomUsers"] : (function () { throw new RuntimeError('Variable "roomUsers" does not exist.', 1, $this->source); })()) != null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["roomUsers"]) || array_key_exists("roomUsers", $context) ? $context["roomUsers"] : (function () { throw new RuntimeError('Variable "roomUsers" does not exist.', 2, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["roomUser"]) {
                // line 3
                echo "        <li><a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_by_room", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "room", [], "any", false, false, false, 3), "id", [], "any", false, false, false, 3)]), "html", null, true);
                echo "\"><i class=\"fa fa-download\"></i>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "room", [], "any", false, false, false, 3), "designation", [], "any", false, false, false, 3), "html", null, true);
                echo "</a></li>
        <li id=\"rooms\" role=\"separator\" class=\"divider\"></li>           
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['roomUser'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "teacher/rooms/displayName.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if roomUsers != null %}
    {% for roomUser in roomUsers %}
        <li><a href=\"{{path('teacher_course_by_room',{'id':roomUser.room.id})}}\"><i class=\"fa fa-download\"></i>{{roomUser.room.designation}}</a></li>
        <li id=\"rooms\" role=\"separator\" class=\"divider\"></li>           
    {% endfor %}
{% endif %}", "teacher/rooms/displayName.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/teacher/rooms/displayName.html.twig");
    }
}
