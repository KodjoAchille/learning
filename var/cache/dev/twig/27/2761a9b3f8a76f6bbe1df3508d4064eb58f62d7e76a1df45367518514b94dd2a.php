<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/show.html.twig */
class __TwigTemplate_a573fae99f78c12a209bceae4f27faeb9cb1da8cda4cebda90918511eb7131ae extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'header' => [$this, 'block_header'],
            'banner' => [$this, 'block_banner'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base-home.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $this->parent = $this->loadTemplate("base-home.html.twig", "user/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "\t<style>
\t.intro{
\t\tpadding:0 20px;
\t\tmargin-top:100px;
\t\ttext-align:center;background:#fff
\t}
\t.intro-section{
\t\tpadding-top:50px;
\t\tposition:relative;
\t\tbackground-size:cover;
\t\tz-index:1;
\t}
\t.intro-section:before{
\t\tcontent:'';
\t\tz-index:-1;
\t\tposition:absolute;top:0;
\t\tbottom:0;
\t\tleft:0;
\t\tright:0;
\t\tbackground:black
\t}
\t.intro-section:after{
\t\tcontent:'';
\t\tposition:absolute;
\t\tbottom:0;
\t\tleft:0;
\t\tright:0;
\t\theight:150px;
\t\tz-index:-1;
\t\tbackground:#fff
\t}
\t</style>\t\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 37
        echo "<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 65
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 66
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 67
        echo "<section class=\"intro-section\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t<div class=\"col-md-1 col-lg-2\"></div>
\t\t<div class=\"col-md-10 col-lg-8\">
\t\t\t<div class=\"intro\">
\t\t\t\t<div class=\"profile-img\"><img src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/user.png"), "html", null, true);
        echo "\" alt=\"\"/></div>
\t\t\t\t";
        // line 74
        if ((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 74, $this->source); })())) {
            // line 75
            echo "\t\t\t\t<form id=\"completeForm\" class=\"d-none\" action=\"#\" method=\"post\">
\t\t\t\t\t<div id=\"responseMessage\" class=\"d-none\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-style-w3ls\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"name\">Nom </label>
\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"";
            // line 81
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 81, $this->source); })()), "name", [], "any", false, false, false, 81), "html", null, true);
            echo "\" id=\"name\"  name=\"name\" type=\"text\" required=\"false\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"firstName\">Prenoms </label>
\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"";
            // line 85
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 85, $this->source); })()), "firstName", [], "any", false, false, false, 85), "html", null, true);
            echo "\"  id=\"firstName\" name=\"firstName\" type=\"text\" required=\"false\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"phone\">Telephone </label>
\t\t\t\t\t\t\t<input class=\"fomr-control\" placeholder=\"";
            // line 89
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 89, $this->source); })()), "phone", [], "any", false, false, false, 89), "html", null, true);
            echo "\" id=\"phone\" name=\"phone\" type=\"text\" required=\"false\">
\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label>Type</label>
\t\t\t\t\t\t\t<select class=\"form-control\" id=\"type\" name=\"type\" required=\"false\">
\t\t\t\t\t\t\t\t<option value=\"\">Type de compte</option>
\t\t\t\t\t\t\t\t<option value=\"ROLE_STUDENT\">Etudiant</option>
\t\t\t\t\t\t\t\t<option value=\"ROLE_TEACHER\">Enseignant</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Classes</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select id=\"rooms\"name=\"rooms[]\" class=\"js-example-basic-multiple\" placeholder=\"cliquer ici\" multiple=\"multiple\" required=\"true\">
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t<a  href=\"javascript:void(0)\" id=\"submitButton\" Class=\"btn btn-info\">Soumettre</a><br><br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t<a href=\"javascipt:void(0)\" id=\"closeEditForm\" Class=\"btn btn-danger\"> Fermer</a><br><br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t\t<div id=\"profilInfo\" class=\"\">
\t\t\t\t\t<h3><b>";
            // line 118
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 118, $this->source); })()), "firstName", [], "any", false, false, false, 118), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 118, $this->source); })()), "name", [], "any", false, false, false, 118), "html", null, true);
            echo "</b></h3>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t<table class=\"table table-borderless\" align=\"right\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Email</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 127
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 127, $this->source); })()), "account", [], "any", false, false, false, 127), "email", [], "any", false, false, false, 127), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Telephone</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 133
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 133, $this->source); })()), "phone", [], "any", false, false, false, 133), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Type de compte</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 138
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
                // line 139
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                echo "ENSEIGNANT";
                echo "
\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 141
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_STUDENT")) {
                    // line 142
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    echo "ETUDIANT";
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 144
                echo "\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 145
            echo "\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Classe</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 150
            if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 150, $this->source); })()), "roomUsers", [], "any", false, false, false, 150) != null)) {
                // line 151
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 151, $this->source); })()), "roomUsers", [], "any", false, false, false, 151));
                foreach ($context['_seq'] as $context["_key"] => $context["roomUser"]) {
                    // line 152
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "room", [], "any", false, false, false, 152), "designation", [], "any", false, false, false, 152), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['roomUser'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 154
                echo "\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 155
            echo "\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<button id=\"showEditForm\" class=\"btn btn-info\">Modifier<i class=\"fa fa-pencil\"></i></button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div><br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
        \t\t</div>
\t\t\t\t
\t\t\t\t";
        }
        // line 172
        echo "\t\t\t</div>
\t\t</div>
\t\t</div>
\t</div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 178
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 179
        echo "<script>
\t\$('#rooms').load(\"";
        // line 180
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("room_index");
        echo "\");
\tvar profilInfo=document.getElementById('profilInfo');
\tvar completeForm=document.getElementById('completeForm');
\t\$('#showEditForm').on('click',function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\tconsole.log('the text');
\t\tif(profilInfo.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tprofilInfo.setAttribute('class',\"\");
\t\t\tcompleteForm.setAttribute('class',\"d-none\");
\t\t}
\t\telse if(completeForm.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tcompleteForm.setAttribute('class',\"\");
\t\t\tprofilInfo.setAttribute('class',\"d-none\");
\t\t}
\t});
\t\$('#closeEditForm').on('click',function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\tif(profilInfo.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tprofilInfo.setAttribute('class',\"\");
\t\t\tcompleteForm.setAttribute('class',\"d-none\");
\t\t}
\t\telse if(completeForm.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tcompleteForm.setAttribute('class',\"\");
\t\t\tprofilInfo.setAttribute('class',\"d-none\");
\t\t}
\t});
\t\$('#submitButton').on('click',function(e){\t\t
\t\te.stopPropagation();
\t\te.preventDefault();
\t\t\$name=\$('#name').val();
\t\t\$firstName=\$('#firstName').val();
\t\t\$phone=\$('#phone').val();
\t\t\$type=\$('#type').val();
\t\t\$rooms=\$('#rooms').val();
\t\t\$.ajax({
\t\t\t'type':'POST',
\t\t\t'dataType':'html',
\t\t\t'url':'";
        // line 223
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_edit");
        echo "',
\t\t\t'data':{'name':\$name,'firstName':\$firstName,'phone':\$phone,'type':\$type,'rooms':\$rooms}
\t\t}).then(function(data){
\t\t\t\$('#profilInfo').html(data);
\t\t\tcompleteForm.setAttribute('class',\"d-none\");
\t\t\tprofilInfo.setAttribute('class',\"\");
\t\t});
\t});
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  433 => 223,  387 => 180,  384 => 179,  374 => 178,  359 => 172,  340 => 155,  337 => 154,  328 => 152,  323 => 151,  321 => 150,  314 => 145,  311 => 144,  305 => 142,  302 => 141,  296 => 139,  294 => 138,  286 => 133,  277 => 127,  263 => 118,  231 => 89,  224 => 85,  217 => 81,  209 => 75,  207 => 74,  203 => 73,  195 => 67,  185 => 66,  167 => 65,  150 => 58,  142 => 53,  124 => 37,  114 => 36,  72 => 3,  62 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base-home.html.twig\" %}
{% block css %}
\t<style>
\t.intro{
\t\tpadding:0 20px;
\t\tmargin-top:100px;
\t\ttext-align:center;background:#fff
\t}
\t.intro-section{
\t\tpadding-top:50px;
\t\tposition:relative;
\t\tbackground-size:cover;
\t\tz-index:1;
\t}
\t.intro-section:before{
\t\tcontent:'';
\t\tz-index:-1;
\t\tposition:absolute;top:0;
\t\tbottom:0;
\t\tleft:0;
\t\tright:0;
\t\tbackground:black
\t}
\t.intro-section:after{
\t\tcontent:'';
\t\tposition:absolute;
\t\tbottom:0;
\t\tleft:0;
\t\tright:0;
\t\theight:150px;
\t\tz-index:-1;
\t\tbackground:#fff
\t}
\t</style>\t\t
{% endblock %}
{% block header %}
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"{{asset('web/images/s2.png')}}\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"{{path('index')}}\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
{% endblock %}
{% block banner %}{% endblock %}
{% block content %}
<section class=\"intro-section\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t<div class=\"col-md-1 col-lg-2\"></div>
\t\t<div class=\"col-md-10 col-lg-8\">
\t\t\t<div class=\"intro\">
\t\t\t\t<div class=\"profile-img\"><img src=\"{{asset('web/images/user.png')}}\" alt=\"\"/></div>
\t\t\t\t{% if user %}
\t\t\t\t<form id=\"completeForm\" class=\"d-none\" action=\"#\" method=\"post\">
\t\t\t\t\t<div id=\"responseMessage\" class=\"d-none\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-style-w3ls\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"name\">Nom </label>
\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"{{user.name}}\" id=\"name\"  name=\"name\" type=\"text\" required=\"false\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"firstName\">Prenoms </label>
\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"{{user.firstName}}\"  id=\"firstName\" name=\"firstName\" type=\"text\" required=\"false\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"phone\">Telephone </label>
\t\t\t\t\t\t\t<input class=\"fomr-control\" placeholder=\"{{user.phone}}\" id=\"phone\" name=\"phone\" type=\"text\" required=\"false\">
\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label>Type</label>
\t\t\t\t\t\t\t<select class=\"form-control\" id=\"type\" name=\"type\" required=\"false\">
\t\t\t\t\t\t\t\t<option value=\"\">Type de compte</option>
\t\t\t\t\t\t\t\t<option value=\"ROLE_STUDENT\">Etudiant</option>
\t\t\t\t\t\t\t\t<option value=\"ROLE_TEACHER\">Enseignant</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group row\">
\t\t\t\t\t\t\t<label for=\"abstract\" class=\"col-sm-2 col-form-label\">Classes</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select id=\"rooms\"name=\"rooms[]\" class=\"js-example-basic-multiple\" placeholder=\"cliquer ici\" multiple=\"multiple\" required=\"true\">
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t<a  href=\"javascript:void(0)\" id=\"submitButton\" Class=\"btn btn-info\">Soumettre</a><br><br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t<a href=\"javascipt:void(0)\" id=\"closeEditForm\" Class=\"btn btn-danger\"> Fermer</a><br><br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t\t<div id=\"profilInfo\" class=\"\">
\t\t\t\t\t<h3><b>{{user.firstName}} {{user.name}}</b></h3>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t<table class=\"table table-borderless\" align=\"right\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Email</td>
\t\t\t\t\t\t\t\t\t\t<td>{{user.account.email}}</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Telephone</td>
\t\t\t\t\t\t\t\t\t\t<td>{{user.phone}}</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Type de compte</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{% if is_granted('ROLE_TEACHER') %}
\t\t\t\t\t\t\t\t\t\t\t\t{{'ENSEIGNANT'}}
\t\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t\t{% if is_granted('ROLE_STUDENT')%}
\t\t\t\t\t\t\t\t\t\t\t\t\t{{'ETUDIANT'}}
\t\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Classe</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{% if user.roomUsers != null %}
\t\t\t\t\t\t\t\t\t\t\t\t{% for roomUser in user.roomUsers %}
\t\t\t\t\t\t\t\t\t\t\t\t\t{{roomUser.room.designation}}
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<button id=\"showEditForm\" class=\"btn btn-info\">Modifier<i class=\"fa fa-pencil\"></i></button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div><br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
        \t\t</div>
\t\t\t\t
\t\t\t\t{% endif %}
\t\t\t</div>
\t\t</div>
\t\t</div>
\t</div>
</section>
{% endblock %}
{% block script %}
<script>
\t\$('#rooms').load(\"{{path('room_index')}}\");
\tvar profilInfo=document.getElementById('profilInfo');
\tvar completeForm=document.getElementById('completeForm');
\t\$('#showEditForm').on('click',function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\tconsole.log('the text');
\t\tif(profilInfo.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tprofilInfo.setAttribute('class',\"\");
\t\t\tcompleteForm.setAttribute('class',\"d-none\");
\t\t}
\t\telse if(completeForm.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tcompleteForm.setAttribute('class',\"\");
\t\t\tprofilInfo.setAttribute('class',\"d-none\");
\t\t}
\t});
\t\$('#closeEditForm').on('click',function(e){
\t\te.stopPropagation();
\t\te.preventDefault();
\t\tif(profilInfo.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tprofilInfo.setAttribute('class',\"\");
\t\t\tcompleteForm.setAttribute('class',\"d-none\");
\t\t}
\t\telse if(completeForm.getAttribute('class')==\"d-none\")
\t\t{
\t\t\tcompleteForm.setAttribute('class',\"\");
\t\t\tprofilInfo.setAttribute('class',\"d-none\");
\t\t}
\t});
\t\$('#submitButton').on('click',function(e){\t\t
\t\te.stopPropagation();
\t\te.preventDefault();
\t\t\$name=\$('#name').val();
\t\t\$firstName=\$('#firstName').val();
\t\t\$phone=\$('#phone').val();
\t\t\$type=\$('#type').val();
\t\t\$rooms=\$('#rooms').val();
\t\t\$.ajax({
\t\t\t'type':'POST',
\t\t\t'dataType':'html',
\t\t\t'url':'{{path('user_edit')}}',
\t\t\t'data':{'name':\$name,'firstName':\$firstName,'phone':\$phone,'type':\$type,'rooms':\$rooms}
\t\t}).then(function(data){
\t\t\t\$('#profilInfo').html(data);
\t\t\tcompleteForm.setAttribute('class',\"d-none\");
\t\t\tprofilInfo.setAttribute('class',\"\");
\t\t});
\t});
</script>
{% endblock %}", "user/show.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/user/show.html.twig");
    }
}
