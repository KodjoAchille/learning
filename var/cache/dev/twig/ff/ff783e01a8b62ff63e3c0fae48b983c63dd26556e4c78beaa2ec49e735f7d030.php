<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/users/teachers.html.twig */
class __TwigTemplate_ab42187e5e508e14d7085a500a41ef32fdd0df2a625737c1886eea1514b20f9f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/teachers.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/teachers.html.twig"));

        $this->parent = $this->loadTemplate("admin-base.html.twig", "admin/users/teachers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des utilisateurs</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            ";
        // line 17
        $context["compteur"] = 0;
        // line 18
        echo "                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Nom d'utilisateur</th>
                                            <th style=\"color: white\" >Email</th>
                                            <th style=\"color: white\" >Nom</th>
                                            <th style=\"color: white\" >Prenoms</th>
                                            <th style=\"color: white\" >telephone</th>
                                            <th style=\"color: white\" >Classes</th>
                                            ";
        // line 30
        echo "                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        ";
        // line 34
        if (((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 34, $this->source); })()) != null)) {
            echo "                 
                                            ";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 35, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 36
                echo "                                            ";
                $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 36, $this->source); })()) + 1);
                // line 37
                echo "                                                <tr>
                                                    <td style=\"color:black\">";
                // line 38
                echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 38, $this->source); })()), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 39
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 39), "username", [], "any", false, false, false, 39), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 40), "email", [], "any", false, false, false, 40), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 41
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "name", [], "any", false, false, false, 41), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 42
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstName", [], "any", false, false, false, 42), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "phone", [], "any", false, false, false, 43), "html", null, true);
                echo "</td>                                                    
                                                    <td  style=\"color:black\">
                                                        ";
                // line 45
                if ((twig_get_attribute($this->env, $this->source, $context["user"], "roomUsers", [], "any", false, false, false, 45) != null)) {
                    // line 46
                    echo "                                                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["user"], "roomUsers", [], "any", false, false, false, 46));
                    foreach ($context['_seq'] as $context["_key"] => $context["roomUser"]) {
                        // line 47
                        echo "                                                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "room", [], "any", false, false, false, 47), "designation", [], "any", false, false, false, 47), "html", null, true);
                        echo "
                                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['roomUser'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 49
                    echo "                                                        ";
                }
                // line 50
                echo "                                                    </td>
                                                    ";
                // line 52
                echo "                                                    <td class=\"text-nowrap\">   
                                                        ";
                // line 53
                if ((twig_get_attribute($this->env, $this->source, $context["user"], "validated", [], "any", false, false, false, 53) != "true")) {
                    // line 54
                    echo "                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 54), "html", null, true);
                    echo "\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                                                        ";
                } else {
                    // line 56
                    echo "                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 56), "html", null, true);
                    echo "\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                                                        ";
                }
                // line 58
                echo "                                                    </td>
                                                </tr>
                                                <div id=\"unValidateModal";
                // line 60
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 60), "html", null, true);
                echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                // line 66
                echo "Voulez vous vraiment désactiver l'utilisateur ?";
                echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser(";
                // line 71
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 71), "html", null, true);
                echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal";
                // line 78
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 78), "html", null, true);
                echo "\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                // line 84
                echo "Voulez vous vraiment activer l'utilisateur ?";
                echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser(";
                // line 89
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 89), "html", null, true);
                echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                <!-- Modal --> 
                                            <!-- /Modal -->
                                            
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 100
            echo "                                        ";
        } else {
            // line 101
            echo "                                            ";
            // line 104
            echo "                                        ";
        }
        // line 105
        echo "
                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 117
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 118
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function getRooms(id){
            console.log('good');
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 199
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_room_list");
        echo "\",
                data:{'id':id}
            }).then(function(data){
                \$('#id').html(data);
            });
        }
        function validateUser(id){
            var path=\"";
        // line 206
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_validate_or_not");
        echo "\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#validateModal'+id).modal('show');
                    \$('#unValidateModal'+id).modal('show');
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 243
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list_update");
        echo "\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
    <script src=\"";
        // line 252
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/users/teachers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  459 => 252,  447 => 243,  407 => 206,  397 => 199,  314 => 119,  309 => 118,  299 => 117,  278 => 105,  275 => 104,  273 => 101,  270 => 100,  253 => 89,  245 => 84,  236 => 78,  226 => 71,  218 => 66,  209 => 60,  205 => 58,  199 => 56,  193 => 54,  191 => 53,  188 => 52,  185 => 50,  182 => 49,  173 => 47,  168 => 46,  166 => 45,  161 => 43,  157 => 42,  153 => 41,  149 => 40,  145 => 39,  141 => 38,  138 => 37,  135 => 36,  131 => 35,  127 => 34,  121 => 30,  108 => 18,  106 => 17,  93 => 6,  83 => 5,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin-base.html.twig' %}
{%  block css %}
    <link href=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.css')}}\" rel=\"stylesheet\" type=\"text/css\" />
{% endblock %}
{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des utilisateurs</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            {% set compteur = 0 %}
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Nom d'utilisateur</th>
                                            <th style=\"color: white\" >Email</th>
                                            <th style=\"color: white\" >Nom</th>
                                            <th style=\"color: white\" >Prenoms</th>
                                            <th style=\"color: white\" >telephone</th>
                                            <th style=\"color: white\" >Classes</th>
                                            {# <th style=\"color: white\" >Addresse</th> #}
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        {% if users != null %}                 
                                            {% for user in users %}
                                            {% set compteur=compteur+1%}
                                                <tr>
                                                    <td style=\"color:black\">{{compteur}}</td>
                                                    <td style=\"color:black\">{{user.account.username}}</td>
                                                    <td style=\"color:black\">{{user.account.email}}</td>
                                                    <td style=\"color:black\">{{user.name}}</td>
                                                    <td style=\"color:black\">{{user.firstName}}</td>
                                                    <td style=\"color:black\">{{user.phone}}</td>                                                    
                                                    <td  style=\"color:black\">
                                                        {% if user.roomUsers != null %}
                                                            {% for roomUser in user.roomUsers %}
                                                                {{roomUser.room.designation}}
                                                            {% endfor %}
                                                        {% endif %}
                                                    </td>
                                                    {# <td style=\"color:black\">{{user.address}}</td> #}
                                                    <td class=\"text-nowrap\">   
                                                        {% if user.validated != \"true\" %}
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal{{user.id}}\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                                                        {% else %}
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal{{user.id}}\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                                                        {% endif %}
                                                    </td>
                                                </tr>
                                                <div id=\"unValidateModal{{user.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment désactiver l'utilisateur ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser({{user.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal{{user.id}}\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment activer l'utilisateur ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser({{user.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                <!-- Modal --> 
                                            <!-- /Modal -->
                                            
                                            {% endfor %}
                                        {% else %}
                                            {# <tr>
                                                <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
                                            </tr> #}
                                        {% endif %}

                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script src=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.js')}}\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function getRooms(id){
            console.log('good');
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_user_room_list')}}\",
                data:{'id':id}
            }).then(function(data){
                \$('#id').html(data);
            });
        }
        function validateUser(id){
            var path=\"{{path('admin_user_validate_or_not')}}\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#validateModal'+id).modal('show');
                    \$('#unValidateModal'+id).modal('show');
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_user_list_update')}}\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
    <script src=\"{{asset('admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}\"></script>
{% endblock %}", "admin/users/teachers.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/users/teachers.html.twig");
    }
}
