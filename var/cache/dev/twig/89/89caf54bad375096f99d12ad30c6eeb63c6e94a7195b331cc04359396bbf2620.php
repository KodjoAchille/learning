<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/users/index.html.twig */
class __TwigTemplate_f30dd03c53b31b8e0a762cd32fc19846f7cc8ec9a23bb04c0460f06524eca1c8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/index.html.twig"));

        $this->parent = $this->loadTemplate("admin-base.html.twig", "admin/users/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des utilisateurs</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            ";
        // line 17
        $context["compteur"] = 0;
        // line 18
        echo "                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Nom d'utilisateur</th>
                                            <th style=\"color: white\" >Email</th>
                                            <th style=\"color: white\" >Nom</th>
                                            <th style=\"color: white\" >Prenoms</th>
                                            <th style=\"color: white\" >telephone</th>
                                            <th style=\"color: white\" >Type de compte</th>
                                            <th style=\"color: white\" >Classes</th>
                                            ";
        // line 31
        echo "                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        ";
        // line 35
        if (((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 35, $this->source); })()) != null)) {
            echo "                 
                                            ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 36, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 37
                echo "                                            ";
                $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 37, $this->source); })()) + 1);
                // line 38
                echo "                                                <tr>
                                                    <td style=\"color:black\">";
                // line 39
                echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 39, $this->source); })()), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 40), "username", [], "any", false, false, false, 40), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 41
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 41), "email", [], "any", false, false, false, 41), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 42
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "name", [], "any", false, false, false, 42), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstName", [], "any", false, false, false, 43), "html", null, true);
                echo "</td>
                                                    <td style=\"color:black\">";
                // line 44
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "phone", [], "any", false, false, false, 44), "html", null, true);
                echo "</td>                                                    
                                                    <td style=\"color:black\">
                                                        ";
                // line 46
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 46), "roles", [], "any", false, false, false, 46)) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                    // line 47
                    echo "                                                            ";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 47), "roles", [], "any", false, false, false, 47), $context["a"], [], "array", false, false, false, 47) == "ROLE_TEACHER")) {
                        // line 48
                        echo "                                                                ";
                        echo "Enseignant";
                        echo "
                                                            ";
                    } else {
                        // line 50
                        echo "                                                                ";
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 50), "roles", [], "any", false, false, false, 50), $context["a"], [], "array", false, false, false, 50) == "ROLE_STUDENT")) {
                            // line 51
                            echo "                                                                    ";
                            echo "Etudiant";
                            echo "
                                                                ";
                        }
                        // line 53
                        echo "                                                            ";
                    }
                    // line 54
                    echo "                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 55
                echo "                                                    </td>
                                                    <td  style=\"color:black\">
                                                        ";
                // line 57
                if ((twig_get_attribute($this->env, $this->source, $context["user"], "roomUsers", [], "any", false, false, false, 57) != null)) {
                    // line 58
                    echo "                                                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["user"], "roomUsers", [], "any", false, false, false, 58));
                    foreach ($context['_seq'] as $context["_key"] => $context["roomUser"]) {
                        // line 59
                        echo "                                                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "room", [], "any", false, false, false, 59), "designation", [], "any", false, false, false, 59), "html", null, true);
                        echo "
                                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['roomUser'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 61
                    echo "                                                        ";
                }
                // line 62
                echo "                                                    </td>
                                                    ";
                // line 64
                echo "                                                    <td class=\"text-nowrap\">   
                                                        ";
                // line 65
                if ((twig_get_attribute($this->env, $this->source, $context["user"], "validated", [], "any", false, false, false, 65) != "true")) {
                    // line 66
                    echo "                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 66), "html", null, true);
                    echo "\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                                                        ";
                } else {
                    // line 68
                    echo "                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 68), "html", null, true);
                    echo "\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                                                        ";
                }
                // line 70
                echo "                                                    </td>
                                                </tr>
                                                <div id=\"unValidateModal";
                // line 72
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 72), "html", null, true);
                echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                // line 78
                echo "Voulez vous vraiment désactiver l'utilisateur ?";
                echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser(";
                // line 83
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 83), "html", null, true);
                echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal";
                // line 90
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 90), "html", null, true);
                echo "\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                // line 96
                echo "Voulez vous vraiment activer l'utilisateur ?";
                echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser(";
                // line 101
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 101), "html", null, true);
                echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                <!-- Modal --> 
                                            <!-- /Modal -->
                                            
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 112
            echo "                                        ";
        } else {
            // line 113
            echo "                                            ";
            // line 116
            echo "                                        ";
        }
        // line 117
        echo "
                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 129
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 130
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function getRooms(id){
            console.log('good');
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 211
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_room_list");
        echo "\",
                data:{'id':id}
            }).then(function(data){
                \$('#id').html(data);
            });
        }
        function validateUser(id){
            var path=\"";
        // line 218
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_validate_or_not");
        echo "\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#validateModal'+id).modal('show');
                    \$('#unValidateModal'+id).modal('show');
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 255
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list_update");
        echo "\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
    <script src=\"";
        // line 264
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/users/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  495 => 264,  483 => 255,  443 => 218,  433 => 211,  350 => 131,  345 => 130,  335 => 129,  314 => 117,  311 => 116,  309 => 113,  306 => 112,  289 => 101,  281 => 96,  272 => 90,  262 => 83,  254 => 78,  245 => 72,  241 => 70,  235 => 68,  229 => 66,  227 => 65,  224 => 64,  221 => 62,  218 => 61,  209 => 59,  204 => 58,  202 => 57,  198 => 55,  192 => 54,  189 => 53,  183 => 51,  180 => 50,  174 => 48,  171 => 47,  167 => 46,  162 => 44,  158 => 43,  154 => 42,  150 => 41,  146 => 40,  142 => 39,  139 => 38,  136 => 37,  132 => 36,  128 => 35,  122 => 31,  108 => 18,  106 => 17,  93 => 6,  83 => 5,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin-base.html.twig' %}
{%  block css %}
    <link href=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.css')}}\" rel=\"stylesheet\" type=\"text/css\" />
{% endblock %}
{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des utilisateurs</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            {% set compteur = 0 %}
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >Nom d'utilisateur</th>
                                            <th style=\"color: white\" >Email</th>
                                            <th style=\"color: white\" >Nom</th>
                                            <th style=\"color: white\" >Prenoms</th>
                                            <th style=\"color: white\" >telephone</th>
                                            <th style=\"color: white\" >Type de compte</th>
                                            <th style=\"color: white\" >Classes</th>
                                            {# <th style=\"color: white\" >Addresse</th> #}
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        {% if users != null %}                 
                                            {% for user in users %}
                                            {% set compteur=compteur+1%}
                                                <tr>
                                                    <td style=\"color:black\">{{compteur}}</td>
                                                    <td style=\"color:black\">{{user.account.username}}</td>
                                                    <td style=\"color:black\">{{user.account.email}}</td>
                                                    <td style=\"color:black\">{{user.name}}</td>
                                                    <td style=\"color:black\">{{user.firstName}}</td>
                                                    <td style=\"color:black\">{{user.phone}}</td>                                                    
                                                    <td style=\"color:black\">
                                                        {% for a in 0 .. user.account.roles|length-1 %}
                                                            {% if user.account.roles[a]==\"ROLE_TEACHER\" %}
                                                                {{\"Enseignant\"}}
                                                            {% else %}
                                                                {% if user.account.roles[a]==\"ROLE_STUDENT\" %}
                                                                    {{\"Etudiant\"}}
                                                                {% endif %}
                                                            {% endif %}
                                                        {% endfor %}
                                                    </td>
                                                    <td  style=\"color:black\">
                                                        {% if user.roomUsers != null %}
                                                            {% for roomUser in user.roomUsers %}
                                                                {{roomUser.room.designation}}
                                                            {% endfor %}
                                                        {% endif %}
                                                    </td>
                                                    {# <td style=\"color:black\">{{user.address}}</td> #}
                                                    <td class=\"text-nowrap\">   
                                                        {% if user.validated != \"true\" %}
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal{{user.id}}\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                                                        {% else %}
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal{{user.id}}\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                                                        {% endif %}
                                                    </td>
                                                </tr>
                                                <div id=\"unValidateModal{{user.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment désactiver l'utilisateur ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser({{user.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"validateModal{{user.id}}\"  class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\" >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment activer l'utilisateur ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"validateUser({{user.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                <!-- Modal --> 
                                            <!-- /Modal -->
                                            
                                            {% endfor %}
                                        {% else %}
                                            {# <tr>
                                                <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
                                            </tr> #}
                                        {% endif %}

                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script src=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.js')}}\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function getRooms(id){
            console.log('good');
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_user_room_list')}}\",
                data:{'id':id}
            }).then(function(data){
                \$('#id').html(data);
            });
        }
        function validateUser(id){
            var path=\"{{path('admin_user_validate_or_not')}}\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    \$('#validateModal'+id).modal('hide');
                    \$('#unValidateModal'+id).modal('hide');
                    //\$('#deleteModal'+id).modal('show');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#validateModal'+id).modal('show');
                    \$('#unValidateModal'+id).modal('show');
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_user_list_update')}}\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
                showModals();
            });
        }
    </script>
    <!--Style Switcher -->
    <script src=\"{{asset('admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}\"></script>
{% endblock %}", "admin/users/index.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/users/index.html.twig");
    }
}
