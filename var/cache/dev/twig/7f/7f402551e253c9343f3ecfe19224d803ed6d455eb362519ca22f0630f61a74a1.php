<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/rooms/index.html.twig */
class __TwigTemplate_758f1aa501b4e11e580026f0c8e9d546c8e871a198197a0c021df23b726c49e2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/rooms/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/rooms/index.html.twig"));

        $this->parent = $this->loadTemplate("admin-base.html.twig", "admin/rooms/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des classes de cours</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#addModal\"><button class=\"btn btn-info\"> Ajouter une classe</button></a>
                            <br><br><br>
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >designation</th>
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th> 
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        ";
        // line 30
        if (((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new RuntimeError('Variable "rooms" does not exist.', 30, $this->source); })()) != null)) {
            echo " 
                                            ";
            // line 31
            $context["compteur"] = 0;
            // line 32
            echo "                                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new RuntimeError('Variable "rooms" does not exist.', 32, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 33
                echo "                                                ";
                if ((twig_get_attribute($this->env, $this->source, $context["room"], "deleted", [], "any", false, false, false, 33) != "true")) {
                    // line 34
                    echo "                                                    ";
                    $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 34, $this->source); })()) + 1);
                    // line 35
                    echo "                                                    <tr>
                                                        <td style=\"color:black\">";
                    // line 36
                    echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 36, $this->source); })()), "html", null, true);
                    echo "</td>
                                                        <td class=\"text-center\">";
                    // line 37
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "designation", [], "any", false, false, false, 37), "html", null, true);
                    echo "</td>
                                                        <td class=\"text-nowrap\">   
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#editModal";
                    // line 39
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 39), "html", null, true);
                    echo "\" title=\"Modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal";
                    // line 40
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 40), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"editModal";
                    // line 44
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 44), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <h4 style=\"color:white\" ><strong >Modifier une classe</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div id=\"editMessage";
                    // line 58
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 58), "html", null, true);
                    echo "\" class=\"d-none\"></div>
                                                                            <form id=\"roomForm\" method=\"POST\">    
                                                                                <div class=\"form-group row\">
                                                                                    <label for=\"abstract\" class=\"col-sm-3 col-form-label\">désignation</label>
                                                                                    <div class=\"col-sm-9\">
                                                                                        <input type=\"text\" required=\"true\"  class=\"form-control\" id=\"designation";
                    // line 63
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 63), "html", null, true);
                    echo "\" placeholder=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "designation", [], "any", false, false, false, 63), "html", null, true);
                    echo "\">
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"beforeEditRoom(";
                    // line 71
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 71), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"deleteModal";
                    // line 78
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 78), "html", null, true);
                    echo "\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            ";
                    // line 84
                    echo "Voulez vous vraiment supprimer le cours ?";
                    echo "
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteRoom(";
                    // line 89
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "id", [], "any", false, false, false, 89), "html", null, true);
                    echo ")\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                ";
                }
                // line 97
                echo "                                                        
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 99
            echo "                                        ";
        } else {
            // line 100
            echo "                                            ";
            // line 103
            echo "                                        ";
        }
        // line 104
        echo "
                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <div id=\"addModal\" class=\"modal fade\" role=\"dialog\">
        <div i class=\"modal-dialog\" >
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                    <span class=\"modal-title\" id=\"myModalLabel\">
                        <div class=\"row\">
                            <h4 style=\"color:white\" ><strong >Ajouter une classe</strong></h4>
                        </div> 
                    </span> 
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <div id=\"addMessage\" class=\"d-none\"></div>
                            <form id=\"roomForm\" method=\"POST\">    
                                <div class=\"form-group row\">
                                    <label for=\"abstract\" class=\"col-sm-3 col-form-label\">désignation</label>
                                    <div class=\"col-sm-9\">
                                        <input type=\"text\" required=\"true\" class=\"form-control\" id=\"designation\" placeholder=\"\">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" onclick=\"beforeAddRoom()\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 150
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 151
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function beforeAddRoom()
        {
            var designation=\$(\"#designation\").val()
            if(designation!=\"\")
            {
                addRoom(designation);
            }
            else
            {
                document.getElementById('addMessage').innerHTML=\"veuillez remplir le champ\";
                document.getElementById('addMessage').setAttribute('class','alert alert-danger');
            }
        }
        function addRoom(designation)
        {
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:\"";
        // line 245
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_room_add");
        echo "\",
                data:{'designation':designation},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#addModal').modal('hide');
                    swal({
                        text: \"Modification effectuée avec succes\",
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#addModal').modal('show');
                }
                else
                {
                    console.log('the message',result.data.message);
                }
            });
        }
        function beforeEditRoom(id)
        {
            var designation=\$(\"#designation\"+id).val()
            if(designation!=\"\")
            {
                editRoom(designation,id);
            }
            else
            {
                document.getElementById('addMessage'+id).innerHTML=\"veuillez remplir le champ\";
                document.getElementById('addMessage'+id).setAttribute('class','alert alert-danger');
                //affichage de message d'erreur
            }
        }
        function editRoom(designation,id)
        {
            var currentId=id;
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:\"";
        // line 286
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_room_edit");
        echo "\",
                data:{'designation':designation,'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#editModal'+currentId).modal('hide');
                    swal({
                        text: \"Modification effectuée avec succes\",
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#editModal'+currentId).modal('show');
                }
                else
                {
                    console.log('the message',result.data.message);
                }
            });
        }
        function deleteRoom(id){
            var path=\"";
        // line 308
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_room_delete");
        echo "\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"";
        // line 340
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_room_list_update");
        echo "\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
            });
        }
    </script>
    <!--Style Switcher -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/rooms/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  520 => 340,  485 => 308,  460 => 286,  416 => 245,  320 => 152,  315 => 151,  305 => 150,  250 => 104,  247 => 103,  245 => 100,  242 => 99,  235 => 97,  223 => 89,  215 => 84,  206 => 78,  196 => 71,  183 => 63,  175 => 58,  158 => 44,  151 => 40,  147 => 39,  142 => 37,  138 => 36,  135 => 35,  132 => 34,  129 => 33,  124 => 32,  122 => 31,  118 => 30,  93 => 7,  83 => 6,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin-base.html.twig' %}
{%  block css %}
    <link href=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.css')}}\" rel=\"stylesheet\" type=\"text/css\" />
{% endblock %}

{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Liste des classes de cours</h4> 
                </div>
            </div>
            <div class=\"row \">
                <div class=\"col-md-12 \">
                    <div class=\"white-box\">
                        <div class=\"row\" >
                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#addModal\"><button class=\"btn btn-info\"> Ajouter une classe</button></a>
                            <br><br><br>
                            <div class=\"table-responsive\">
                                <table id=\"myTable\" class=\"table table-striped\">
                                    <thead style=\"background-color:#03a9f3\">
                                        <tr >
                                            <th style=\"color: white\">N°</th>
                                            <th style=\"color: white\" >designation</th>
                                            <th style=\"color: white\" class=\"text-nowrap\" >Action</th> 
                                        </tr>
                                    </thead>
                                    <tbody id=\"dataContent\" class=\"table table-bordered\">
                                        {% if rooms != null %} 
                                            {% set compteur = 0 %}
                                            {% for room in rooms  %}
                                                {% if room.deleted != \"true\" %}
                                                    {% set compteur=compteur+1%}
                                                    <tr>
                                                        <td style=\"color:black\">{{compteur}}</td>
                                                        <td class=\"text-center\">{{room.designation}}</td>
                                                        <td class=\"text-nowrap\">   
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#editModal{{room.id}}\" title=\"Modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                                                            <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal{{room.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div id=\"editModal{{room.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div i class=\"modal-dialog\" >
                                                            <div class=\"modal-content\">
                                                                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                                                    <span class=\"modal-title\" id=\"myModalLabel\">
                                                                        <div class=\"row\">
                                                                            <h4 style=\"color:white\" ><strong >Modifier une classe</strong></h4>
                                                                        </div> 
                                                                    </span> 
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            <div id=\"editMessage{{room.id}}\" class=\"d-none\"></div>
                                                                            <form id=\"roomForm\" method=\"POST\">    
                                                                                <div class=\"form-group row\">
                                                                                    <label for=\"abstract\" class=\"col-sm-3 col-form-label\">désignation</label>
                                                                                    <div class=\"col-sm-9\">
                                                                                        <input type=\"text\" required=\"true\"  class=\"form-control\" id=\"designation{{room.id}}\" placeholder=\"{{room.designation}}\">
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"beforeEditRoom({{room.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <div id=\"deleteModal{{room.id}}\" class=\"modal fade\" role=\"dialog\">
                                                        <div id=\"currentModal\" class=\"modal-dialog modal-dialog-centered\" >
                                                            <div class=\"modal-content\"  >
                                                                <div class=\"modal-body\">
                                                                    <div class=\"row\">
                                                                        <div class=\"col-md-12\">
                                                                            {{\"Voulez vous vraiment supprimer le cours ?\"}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" onclick=\"deleteRoom({{room.id}})\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                                                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                    </div>
                                                    <!-- /Modal -->
                                                {% endif %}                                                        
                                            {% endfor %}
                                        {% else %}
                                            {# <tr>
                                                <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
                                            </tr> #}
                                        {% endif %}

                                    </tbody>     
                                </table>
                                <!-- Modal -->
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <div id=\"addModal\" class=\"modal fade\" role=\"dialog\">
        <div i class=\"modal-dialog\" >
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                    <span class=\"modal-title\" id=\"myModalLabel\">
                        <div class=\"row\">
                            <h4 style=\"color:white\" ><strong >Ajouter une classe</strong></h4>
                        </div> 
                    </span> 
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <div id=\"addMessage\" class=\"d-none\"></div>
                            <form id=\"roomForm\" method=\"POST\">    
                                <div class=\"form-group row\">
                                    <label for=\"abstract\" class=\"col-sm-3 col-form-label\">désignation</label>
                                    <div class=\"col-sm-9\">
                                        <input type=\"text\" required=\"true\" class=\"form-control\" id=\"designation\" placeholder=\"\">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" onclick=\"beforeAddRoom()\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Ajouter</button>                                                    
                    <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script src=\"{{asset('admin/plugins/bower_components/datatables/jquery.dataTables.min.js')}}\"></script>
 <script>
    \$(document).ready(function() {
    \$('#myTable').DataTable( {
        \"language\": {
            \"sProcessing\":     \"Traitement en cours...\",
            \"sSearch\":         \"Rechercher&nbsp;:\",
            \"sLengthMenu\":     \"Afficher _MENU_ &eacute;l&eacute;ments\",
            \"sInfo\":           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",
            \"sInfoEmpty\":      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",
            \"sInfoFiltered\":   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",
            \"sInfoPostFix\":    \"\",
            \"sLoadingRecords\": \"Chargement en cours...\",
            \"sZeroRecords\":    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",
            \"sEmptyTable\":     \"Aucune donn&eacute;e disponible dans le tableau\",
            \"oPaginate\": {
                \"sFirst\":      \"Premier\",
                \"sPrevious\":   \"Pr&eacute;c&eacute;dent\",
                \"sNext\":       \"Suivant\",
                \"sLast\":       \"Dernier\"
            },
            \"oAria\": {
                \"sSortAscending\":  \": activer pour trier la colonne par ordre croissant\",
                \"sSortDescending\": \": activer pour trier la colonne par ordre d&eacute;croissant\"
            }
        }
    } );
    } );
    \$(document).ready(function() {
        \$('#myTable').DataTable();
        \$(document).ready(function() {
            var table = \$('#example').DataTable({
                \"columnDefs\": [{
                    \"visible\": false,
                    \"targets\": 2
                }],
                \"order\": [
                    [2, 'asc']
                ],
                \"displayLength\": 25,
                \"drawCallback\": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            \$(rows).eq(i).before('<tr class=\"group\"><td colspan=\"5\">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            \$('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    \$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>
        function beforeAddRoom()
        {
            var designation=\$(\"#designation\").val()
            if(designation!=\"\")
            {
                addRoom(designation);
            }
            else
            {
                document.getElementById('addMessage').innerHTML=\"veuillez remplir le champ\";
                document.getElementById('addMessage').setAttribute('class','alert alert-danger');
            }
        }
        function addRoom(designation)
        {
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:\"{{path('admin_room_add')}}\",
                data:{'designation':designation},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#addModal').modal('hide');
                    swal({
                        text: \"Modification effectuée avec succes\",
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#addModal').modal('show');
                }
                else
                {
                    console.log('the message',result.data.message);
                }
            });
        }
        function beforeEditRoom(id)
        {
            var designation=\$(\"#designation\"+id).val()
            if(designation!=\"\")
            {
                editRoom(designation,id);
            }
            else
            {
                document.getElementById('addMessage'+id).innerHTML=\"veuillez remplir le champ\";
                document.getElementById('addMessage'+id).setAttribute('class','alert alert-danger');
                //affichage de message d'erreur
            }
        }
        function editRoom(designation,id)
        {
            var currentId=id;
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:\"{{path('admin_room_edit')}}\",
                data:{'designation':designation,'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#editModal'+currentId).modal('hide');
                    swal({
                        text: \"Modification effectuée avec succes\",
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                    \$('#editModal'+currentId).modal('show');
                }
                else
                {
                    console.log('the message',result.data.message);
                }
            });
        }
        function deleteRoom(id){
            var path=\"{{path('admin_room_delete')}}\";
            AjaxFunction(id,path);
        }
        function AjaxFunction(id,path){
            \$.ajax({
                type:'POST',
                dataType:'html',
                url:path,
                data:{'id':id},
            }).then(function(data){
                var result=JSON.parse(data);
                if(result.data.statut==200)
                {
                    \$('#deleteModal'+id).modal('hide');
                    swal({
                        text: result.data.message,
                        icon: \"success\",
                        buttons: \"ok\",
                    });
                    updateTable();
                }
                else
                {
                    console.log(result.data.message);
                }
            });
        }
        function updateTable()
        {
            \$.ajax({
                type:'GET',
                dataType:'html',
                url:\"{{path('admin_room_list_update')}}\",
            }).then(function(data){
                \$('#dataContent').html('');
                \$('#dataContent').html(data);
            });
        }
    </script>
    <!--Style Switcher -->
{% endblock %}", "admin/rooms/index.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/rooms/index.html.twig");
    }
}
