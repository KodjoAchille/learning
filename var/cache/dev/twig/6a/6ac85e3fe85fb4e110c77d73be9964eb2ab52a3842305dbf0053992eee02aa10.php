<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* courses/index.html.twig */
class __TwigTemplate_706091b69406e524badb545dd34f842aef32ffef924ff657238038519c12a6b2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'banner' => [$this, 'block_banner'],
            'header' => [$this, 'block_header'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base-home.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/index.html.twig"));

        $this->parent = $this->loadTemplate("base-home.html.twig", "courses/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<style>
input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}
.search-container button:hover {
  background: #ccc;
}
.search-container {
  
}
</style>
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s2.png"), "html", null, true);
        echo "\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 46
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 47
        echo "<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
        \t\t<h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3>
            </div>
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"heading mb-sm-5 mb-4 search-container\">
                    <form action=\"\">
                        <input type=\"text\" placeholder=\"taper quelquechose..\" name=\"search\">
                        <button style=\"align:center\"  class=\"btn btn-dark\">Rechercher</button>                
                    </form>
                </div>
            </div>
        </div>
        ";
        // line 64
        echo "\t\t<div class=\"row\">
\t\t";
        // line 65
        if (((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 65, $this->source); })()) != null)) {
            echo " 
   \t\t\t";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 66, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
                // line 67
                echo "    \t\t\t";
                if (((twig_get_attribute($this->env, $this->source, $context["course"], "deleted", [], "any", false, false, false, 67) != "true") && (twig_get_attribute($this->env, $this->source, $context["course"], "validated", [], "any", false, false, false, 67) == "true"))) {
                    // line 68
                    echo "\t\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"grid\">
\t\t\t\t\t\t<img src=\"";
                    // line 70
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/choose1.jpg"), "html", null, true);
                    echo "\" alt=\"\" class=\"img-fluid\" />
\t\t\t\t\t\t<div class=\"info p-4\">
\t\t\t\t\t\t\t<h4 class=\"\"><img src=\"";
                    // line 72
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s1.png"), "html", null, true);
                    echo "\" class=\"img-fluid\" alt=\"\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "title", [], "any", false, false, false, 72), "html", null, true);
                    echo "</h4>
\t\t\t\t\t\t\t";
                    // line 73
                    if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
                        // line 74
                        echo "\t\t\t\t\t\t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_show", ["id" => twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 74)]), "html", null, true);
                        echo "\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t";
                    } else {
                        // line 76
                        echo "\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" title=\"veuilez vous connecter ou créer un compte\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t";
                    }
                    // line 78
                    echo "\t\t\t\t\t\t\t<p class=\"mt-3\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "abstract", [], "any", false, false, false, 78), "html", null, true);
                    echo "</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
                }
                // line 83
                echo "\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 84
            echo "\t\t";
        }
        // line 85
        echo "\t\t</div>
    </div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "courses/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 85,  229 => 84,  223 => 83,  214 => 78,  210 => 76,  204 => 74,  202 => 73,  196 => 72,  191 => 70,  187 => 68,  184 => 67,  180 => 66,  176 => 65,  173 => 64,  155 => 47,  145 => 46,  128 => 39,  120 => 34,  88 => 4,  78 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base-home.html.twig\" %}
{% block banner %}{% endblock %}
{% block header %}
<style>
input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}
.search-container button:hover {
  background: #ccc;
}
.search-container {
  
}
</style>
<header  style=\"background-color:black\">  
\t<div class=\"top-head container\">
\t\t<div class=\"ml-auto text-right right-p\">
\t\t\t<ul>
\t\t\t\t<li class=\"mr-3\">
\t\t\t\t\t<span class=\"fa fa-clock-o\"></span> Mon-Sat : 9:00 to 17:00</li>
\t\t\t\t<li>
\t\t\t\t\t<span class=\"fa fa-envelope-open\"></span> <a href=\"mailto:info@example.com\">info@example.com</a> 
                </li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<!-- nav -->
\t\t<nav class=\"py-3 d-lg-flex\">
\t\t\t<div id=\"logo\">
\t\t\t\t<h1> <a href=\"index.html\"><img src=\"{{asset('web/images/s2.png')}}\" alt=\"\"> Child Learn </a></h1>
\t\t\t</div>
\t\t\t<label for=\"drop\" class=\"toggle\"><span class=\"fa fa-bars\"></span></label>
\t\t\t<input type=\"checkbox\" id=\"drop\" />
\t\t\t<ul class=\"menu ml-auto mt-1\">
\t\t\t\t<li class=\"active\"><a href=\"{{path('index')}}\">Acceuil</a></li>
\t\t\t</ul>
\t\t</nav>
\t\t<!-- //nav -->
\t</div>
</header>
{% endblock %}
{% block content %}
<br/><br/><br/><br/>
<section class=\"other_services py-5\" id=\"courses\">
\t<div class=\"container py-lg-5 py-3\">
\t\t<div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
        \t\t<h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3>
            </div>
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"heading mb-sm-5 mb-4 search-container\">
                    <form action=\"\">
                        <input type=\"text\" placeholder=\"taper quelquechose..\" name=\"search\">
                        <button style=\"align:center\"  class=\"btn btn-dark\">Rechercher</button>                
                    </form>
                </div>
            </div>
        </div>
        {# <h3 class=\"heading mb-sm-5 mb-4\">Nos cours</h3> #}
\t\t<div class=\"row\">
\t\t{% if courses != null %} 
   \t\t\t{% for course in courses  %}
    \t\t\t{% if (course.deleted != \"true\") and (course.validated==\"true\") %}
\t\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"grid\">
\t\t\t\t\t\t<img src=\"{{asset('web/images/choose1.jpg')}}\" alt=\"\" class=\"img-fluid\" />
\t\t\t\t\t\t<div class=\"info p-4\">
\t\t\t\t\t\t\t<h4 class=\"\"><img src=\"{{asset('web/images/s1.png')}}\" class=\"img-fluid\" alt=\"\">{{course.title}}</h4>
\t\t\t\t\t\t\t{% if is_granted('ROLE_USER')%}
\t\t\t\t\t\t\t\t<a href=\"{{path('course_show',{'id':course.id})}}\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" title=\"veuilez vous connecter ou créer un compte\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t<p class=\"mt-3\">{{course.abstract}}</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t{% endif %}
\t\t\t{% endfor %}
\t\t{% endif %}
\t\t</div>
    </div>
</section>
{% endblock %}", "courses/index.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/courses/index.html.twig");
    }
}
