<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* courses/front.html.twig */
class __TwigTemplate_298e437bb16a5e724373fd79d5cf7a5f37790ba10e086213d04d7dd616aab72a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/front.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/front.html.twig"));

        // line 1
        echo "<form  id=\"addForm\" method=\"POST\" enctype=\"multipart/form-data\">
    <div class=\"form-group row\">
        <label for=\"abstract\" class=\"col-sm-2 col-form-label\">Contenu</label>
        <div class=\"col-sm-10\">
            <div id=\"videoMessage\" class=\"d-none\"></div>
            <input type=\"file\" required=\"true\" name=\"content\" class=\"dropify-fr\" id=\"content\" />
        </div>
    </div>
</form>

<script>
\tvar responseMessage=document.getElementById(\"responseMessage\");
\tvar videoMessage=document.getElementById(\"videoMessage\");
\t\$(\"#content\").change(function() {
\t\tvar file = this.files[0];
\t\tif(isVideo(file['name']))
\t\t{
\t\t\tvideoMessage.innerHTML=\"\";
\t\t\tvideoMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tvideoMessage.innerHTML=\"Le contenu doit être une video\";
\t\t\tvideoMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#content\").val('');
\t\t}
\t\tvar fileType = file.type;
\t\tconsole.log('the files', this.files)
\t});
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isVideo(filename) {
\t\tconsole.log(\" is video called\")
\t\tvar ext = getExtension(filename);
\t\tconsole.log(ext);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'm4v':
\t\t\tcase 'avi':
\t\t\tcase 'mpg':
\t\t\tcase 'mp4':
\t\t\t// etc
\t\t\tconsole.log(\"extension is good\");
\t\t\treturn true;
\t\t}
\t\tconsole.log(\"extension is not good\");
\t\treturn false;
\t}
\tfunction _(el){
\t\treturn document.getElementById(el);
\t}
\t\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\t//alert(file.name+\" | \"+file.size+\" | \"+file.type);
\t\t
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\tif(\$content)
\t\t{
\t\t\tvar file=document.getElementById(\"content\").files[0];
\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\t//fd.append('abstract',\$abstract);
\t\t\t//fd.append('title',\$title);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});\t
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
        echo "\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tconsole.log('good job');
\t\t\tvar result=JSON.parse(data);
\t\t\tconsole.log('the result',result);
\t\t});
    }

    //ajkljlkjl
    \$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar file=document.getElementById('content').files[0];
\t\tconsole.log('the file',file);
\t\talert(file.name+\" | \"+file.size+\" | \"+file.type);\t\t
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\tif(\$content)
\t\t{
\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\t//fd.append('abstract',\$abstract);
\t\t\t//fd.append('title',\$title);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});
</script> ";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "courses/front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 83,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form  id=\"addForm\" method=\"POST\" enctype=\"multipart/form-data\">
    <div class=\"form-group row\">
        <label for=\"abstract\" class=\"col-sm-2 col-form-label\">Contenu</label>
        <div class=\"col-sm-10\">
            <div id=\"videoMessage\" class=\"d-none\"></div>
            <input type=\"file\" required=\"true\" name=\"content\" class=\"dropify-fr\" id=\"content\" />
        </div>
    </div>
</form>

<script>
\tvar responseMessage=document.getElementById(\"responseMessage\");
\tvar videoMessage=document.getElementById(\"videoMessage\");
\t\$(\"#content\").change(function() {
\t\tvar file = this.files[0];
\t\tif(isVideo(file['name']))
\t\t{
\t\t\tvideoMessage.innerHTML=\"\";
\t\t\tvideoMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tvideoMessage.innerHTML=\"Le contenu doit être une video\";
\t\t\tvideoMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#content\").val('');
\t\t}
\t\tvar fileType = file.type;
\t\tconsole.log('the files', this.files)
\t});
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isVideo(filename) {
\t\tconsole.log(\" is video called\")
\t\tvar ext = getExtension(filename);
\t\tconsole.log(ext);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'm4v':
\t\t\tcase 'avi':
\t\t\tcase 'mpg':
\t\t\tcase 'mp4':
\t\t\t// etc
\t\t\tconsole.log(\"extension is good\");
\t\t\treturn true;
\t\t}
\t\tconsole.log(\"extension is not good\");
\t\treturn false;
\t}
\tfunction _(el){
\t\treturn document.getElementById(el);
\t}
\t\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\t//alert(file.name+\" | \"+file.size+\" | \"+file.type);
\t\t
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\tif(\$content)
\t\t{
\t\t\tvar file=document.getElementById(\"content\").files[0];
\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\t//fd.append('abstract',\$abstract);
\t\t\t//fd.append('title',\$title);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});\t
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"{{path('course_new')}}\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tconsole.log('good job');
\t\t\tvar result=JSON.parse(data);
\t\t\tconsole.log('the result',result);
\t\t});
    }

    //ajkljlkjl
    \$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar file=document.getElementById('content').files[0];
\t\tconsole.log('the file',file);
\t\talert(file.name+\" | \"+file.size+\" | \"+file.type);\t\t
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\tif(\$content)
\t\t{
\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\t//fd.append('abstract',\$abstract);
\t\t\t//fd.append('title',\$title);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});
</script> ", "courses/front.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/courses/front.html.twig");
    }
}
