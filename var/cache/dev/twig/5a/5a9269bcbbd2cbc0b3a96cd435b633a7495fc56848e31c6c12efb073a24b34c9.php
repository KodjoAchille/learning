<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* courses/indexLite.html.twig */
class __TwigTemplate_1b2ca4d340e4d31e27cdf2069e5e1c71537c971b6c8b214fa280bd9548d88e28 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/indexLite.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/indexLite.html.twig"));

        // line 1
        if (((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 1, $this->source); })()) != null)) {
            // line 2
            echo "\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new RuntimeError('Variable "courses" does not exist.', 2, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
                // line 3
                echo "\t<div class=\"col-lg-4 col-md-6\">
\t\t<div class=\"grid\">
\t\t\t<img src=\"";
                // line 5
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/choose1.jpg"), "html", null, true);
                echo "\" alt=\"\" class=\"img-fluid\" />
\t\t\t<div class=\"info p-4\">
\t\t\t\t<h4 class=\"\"><img src=\"";
                // line 7
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/s1.png"), "html", null, true);
                echo "\" class=\"img-fluid\" alt=\"\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "title", [], "any", false, false, false, 7), "html", null, true);
                echo "</h4>
\t\t\t\t";
                // line 8
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
                    // line 9
                    echo "\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_show", ["id" => twig_get_attribute($this->env, $this->source, $context["course"], "id", [], "any", false, false, false, 9)]), "html", null, true);
                    echo "\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t";
                } else {
                    // line 11
                    echo "\t\t\t\t\t<a href=\"javascript:void(0)\" title=\"veuilez vous connecter ou créer un compte\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t";
                }
                // line 13
                echo "\t\t\t\t<p class=\"mt-3\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["course"], "abstract", [], "any", false, false, false, 13), "html", null, true);
                echo "</p>
\t\t\t</div>
\t\t</div>
\t</div>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "courses/indexLite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 13,  73 => 11,  67 => 9,  65 => 8,  59 => 7,  54 => 5,  50 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if courses != null %}
\t{% for course in courses %}
\t<div class=\"col-lg-4 col-md-6\">
\t\t<div class=\"grid\">
\t\t\t<img src=\"{{asset('web/images/choose1.jpg')}}\" alt=\"\" class=\"img-fluid\" />
\t\t\t<div class=\"info p-4\">
\t\t\t\t<h4 class=\"\"><img src=\"{{asset('web/images/s1.png')}}\" class=\"img-fluid\" alt=\"\">{{course.title}}</h4>
\t\t\t\t{% if is_granted('ROLE_USER')%}
\t\t\t\t\t<a href=\"{{path('course_show',{'id':course.id})}}\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t{% else %}
\t\t\t\t\t<a href=\"javascript:void(0)\" title=\"veuilez vous connecter ou créer un compte\" style=\"align:center\" type=\"button\" class=\"btn btn-dark\">Suivre le cours</a>\t\t\t\t
\t\t\t\t{% endif %}
\t\t\t\t<p class=\"mt-3\">{{course.abstract}}</p>
\t\t\t</div>
\t\t</div>
\t</div>
\t{% endfor %}
{% endif %}
", "courses/indexLite.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/courses/indexLite.html.twig");
    }
}
