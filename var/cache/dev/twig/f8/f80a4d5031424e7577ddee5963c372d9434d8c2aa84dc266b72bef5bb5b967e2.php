<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/showLite.html.twig */
class __TwigTemplate_9c95c2ab8750b8683ea6473714517785b66cfdd4c01c753411f62a5bc7151eea extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/showLite.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/showLite.html.twig"));

        // line 1
        echo "<h3><b>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 1, $this->source); })()), "firstName", [], "any", false, false, false, 1), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 1, $this->source); })()), "name", [], "any", false, false, false, 1), "html", null, true);
        echo "</b></h3>
<div class=\"row\">
    <div class=\"col-md-2\">
    </div>
    <div class=\"col-md-8\">
        <table class=\"table table-borderless\" align=\"right\">
            <thead>
                <tr>
                    <td>Email</td>
                    <td>";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 10, $this->source); })()), "account", [], "any", false, false, false, 10), "email", [], "any", false, false, false, 10), "html", null, true);
        echo "</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Telephone</td>
                    <td>";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 16, $this->source); })()), "phone", [], "any", false, false, false, 16), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>Type de compte</td>
                    <td>";
        // line 20
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEACHER")) {
            echo "ENSEIGNANT";
        } else {
            echo "ETUDIANT";
        }
        echo "</td>
                </tr>
                <tr>
                    <td>Classe</td>
                    <td>
                        ";
        // line 25
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 25, $this->source); })()), "roomUsers", [], "any", false, false, false, 25) != null)) {
            // line 26
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 26, $this->source); })()), "roomUsers", [], "any", false, false, false, 26));
            foreach ($context['_seq'] as $context["_key"] => $context["roomUser"]) {
                // line 27
                echo "                                ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "room", [], "any", false, false, false, 27), "designation", [], "any", false, false, false, 27), "html", null, true);
                echo "
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['roomUser'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "                        ";
        }
        // line 30
        echo "                    </td>
                </tr>
            </tbody>
        </table>
        <div class=\"row\">
            <div class=\"col-md-4\">
            </div>
            <div class=\"col-md-4\">
                <button id=\"showEditForm\" class=\"btn btn-info\">Modifier<i class=\"fa fa-pencil\"></i></button>
            </div>
        </div><br>
        </div>
    </div>
</div>
<script>
    \$('#showEditForm').on('click',function(e){
        e.stopPropagation();
        e.preventDefault();
        console.log('the text');
        if(profilInfo.getAttribute('class')==\"d-none\")
        {
            profilInfo.setAttribute('class',\"\");
            completeForm.setAttribute('class',\"d-none\");
        }
        else if(completeForm.getAttribute('class')==\"d-none\")
        {
            completeForm.setAttribute('class',\"\");
            profilInfo.setAttribute('class',\"d-none\");
        }
    });
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "user/showLite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 30,  102 => 29,  93 => 27,  88 => 26,  86 => 25,  74 => 20,  67 => 16,  58 => 10,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<h3><b>{{user.firstName}} {{user.name}}</b></h3>
<div class=\"row\">
    <div class=\"col-md-2\">
    </div>
    <div class=\"col-md-8\">
        <table class=\"table table-borderless\" align=\"right\">
            <thead>
                <tr>
                    <td>Email</td>
                    <td>{{user.account.email}}</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Telephone</td>
                    <td>{{user.phone}}</td>
                </tr>
                <tr>
                    <td>Type de compte</td>
                    <td>{% if is_granted('ROLE_TEACHER') %}{{\"ENSEIGNANT\"}}{% else %}{{'ETUDIANT'}}{% endif %}</td>
                </tr>
                <tr>
                    <td>Classe</td>
                    <td>
                        {% if user.roomUsers != null %}
                            {% for roomUser in user.roomUsers %}
                                {{roomUser.room.designation}}
                            {% endfor %}
                        {% endif %}
                    </td>
                </tr>
            </tbody>
        </table>
        <div class=\"row\">
            <div class=\"col-md-4\">
            </div>
            <div class=\"col-md-4\">
                <button id=\"showEditForm\" class=\"btn btn-info\">Modifier<i class=\"fa fa-pencil\"></i></button>
            </div>
        </div><br>
        </div>
    </div>
</div>
<script>
    \$('#showEditForm').on('click',function(e){
        e.stopPropagation();
        e.preventDefault();
        console.log('the text');
        if(profilInfo.getAttribute('class')==\"d-none\")
        {
            profilInfo.setAttribute('class',\"\");
            completeForm.setAttribute('class',\"d-none\");
        }
        else if(completeForm.getAttribute('class')==\"d-none\")
        {
            completeForm.setAttribute('class',\"\");
            profilInfo.setAttribute('class',\"d-none\");
        }
    });
</script>", "user/showLite.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/user/showLite.html.twig");
    }
}
