<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/users/indexLite.html.twig */
class __TwigTemplate_4ecfe7c7b390784f4f38f5d032f7d423a2c1e39bd865e8d298ee9ce4396a07f4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/indexLite.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/indexLite.html.twig"));

        // line 1
        if (((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 1, $this->source); })()) != null)) {
            echo " 
    ";
            // line 2
            $context["compteur"] = 0;
            // line 3
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 3, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 4
                echo "        ";
                $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 4, $this->source); })()) + 1);
                // line 5
                echo "         <tr>
            <td style=\"color:black\">";
                // line 6
                echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 6, $this->source); })()), "html", null, true);
                echo "</td>
            <td style=\"color:black\">";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 7), "username", [], "any", false, false, false, 7), "html", null, true);
                echo "</td>
            <td style=\"color:black\">";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 8), "email", [], "any", false, false, false, 8), "html", null, true);
                echo "</td>
            <td style=\"color:black\">";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "name", [], "any", false, false, false, 9), "html", null, true);
                echo "</td>
            <td style=\"color:black\">";
                // line 10
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstName", [], "any", false, false, false, 10), "html", null, true);
                echo "</td>
            <td style=\"color:black\">";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "phone", [], "any", false, false, false, 11), "html", null, true);
                echo "</td>                                                    
            <td style=\"color:black\">
                ";
                // line 13
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 13), "roles", [], "any", false, false, false, 13)) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                    // line 14
                    echo "                    ";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 14), "roles", [], "any", false, false, false, 14), $context["a"], [], "array", false, false, false, 14) == "ROLE_TEACHER")) {
                        // line 15
                        echo "                        ";
                        echo "Enseignant";
                        echo "
                    ";
                    } else {
                        // line 17
                        echo "                        ";
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "account", [], "any", false, false, false, 17), "roles", [], "any", false, false, false, 17), $context["a"], [], "array", false, false, false, 17) == "ROLE_STUDENT")) {
                            // line 18
                            echo "                            ";
                            echo "Etudiant";
                            echo "
                        ";
                        }
                        // line 20
                        echo "                    ";
                    }
                    // line 21
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 22
                echo "            </td>
            <td  style=\"color:black\">
                ";
                // line 24
                if ((twig_get_attribute($this->env, $this->source, $context["user"], "roomUsers", [], "any", false, false, false, 24) != null)) {
                    // line 25
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["user"], "roomUsers", [], "any", false, false, false, 25));
                    foreach ($context['_seq'] as $context["_key"] => $context["roomUser"]) {
                        // line 26
                        echo "                        ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["roomUser"], "room", [], "any", false, false, false, 26), "designation", [], "any", false, false, false, 26), "html", null, true);
                        echo "
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['roomUser'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 28
                    echo "                ";
                }
                // line 29
                echo "            </td>
            <td style=\"color:black\">";
                // line 30
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "address", [], "any", false, false, false, 30), "html", null, true);
                echo "</td>
            <td class=\"text-nowrap\">   
                ";
                // line 32
                if ((twig_get_attribute($this->env, $this->source, $context["user"], "validated", [], "any", false, false, false, 32) != "true")) {
                    // line 33
                    echo "                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 33), "html", null, true);
                    echo "\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                ";
                } else {
                    // line 35
                    echo "                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 35), "html", null, true);
                    echo "\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                ";
                }
                // line 37
                echo "            </td>
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 41
            echo "    ";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/users/indexLite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 41,  161 => 37,  155 => 35,  149 => 33,  147 => 32,  142 => 30,  139 => 29,  136 => 28,  127 => 26,  122 => 25,  120 => 24,  116 => 22,  110 => 21,  107 => 20,  101 => 18,  98 => 17,  92 => 15,  89 => 14,  85 => 13,  80 => 11,  76 => 10,  72 => 9,  68 => 8,  64 => 7,  60 => 6,  57 => 5,  54 => 4,  49 => 3,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if users != null %} 
    {% set compteur = 0 %}
    {% for user in users  %}
        {% set compteur=compteur+1%}
         <tr>
            <td style=\"color:black\">{{compteur}}</td>
            <td style=\"color:black\">{{user.account.username}}</td>
            <td style=\"color:black\">{{user.account.email}}</td>
            <td style=\"color:black\">{{user.name}}</td>
            <td style=\"color:black\">{{user.firstName}}</td>
            <td style=\"color:black\">{{user.phone}}</td>                                                    
            <td style=\"color:black\">
                {% for a in 0 .. user.account.roles|length-1 %}
                    {% if user.account.roles[a]==\"ROLE_TEACHER\" %}
                        {{\"Enseignant\"}}
                    {% else %}
                        {% if user.account.roles[a]==\"ROLE_STUDENT\" %}
                            {{\"Etudiant\"}}
                        {% endif %}
                    {% endif %}
                {% endfor %}
            </td>
            <td  style=\"color:black\">
                {% if user.roomUsers != null %}
                    {% for roomUser in user.roomUsers %}
                        {{roomUser.room.designation}}
                    {% endfor %}
                {% endif %}
            </td>
            <td style=\"color:black\">{{user.address}}</td>
            <td class=\"text-nowrap\">   
                {% if user.validated != \"true\" %}
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#validateModal{{user.id}}\"  title=\"activer\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-up text-succes\"></i></span> </a>                                                             
                {% else %}
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#unValidateModal{{user.id}}\"  title=\"désactiver\"><span class=\"btn btn-default\"><i class=\"fa fa-hand-o-down text-danger\"></i></span> </a>                                                             
                {% endif %}
            </td>
        </tr>
    {% endfor %}
{% else %}
    {# <tr>
        <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
    </tr> #}
{% endif %}
", "admin/users/indexLite.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/users/indexLite.html.twig");
    }
}
