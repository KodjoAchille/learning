<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/courses/edit.html.twig */
class __TwigTemplate_df49b1c4e8113b050262155d8029f5c11fbd019a98bd7fb06e96a1e74db1872d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'content' => [$this, 'block_content'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin-base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/courses/edit.html.twig"));

        $this->parent = $this->loadTemplate("admin-base.html.twig", "admin/courses/edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 3
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/css/dropify.min.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Modification de cours/<a href=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_list");
        echo "\" class=\"btn btn-default\">Retour</a> </h4> 
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-2\">
                </div>
                <div class=\"col-8\">
                    <div class=\"white-box\">
                        <br/>
                        <div id=\"responseMessage\" class=\"d-none\"></div>
                        <div id=\"courseId\" class=\"d-none\">";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 20, $this->source); })()), "id", [], "any", false, false, false, 20), "html", null, true);
        echo "</div>
                        <form  id=\"addForm\" action=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
        echo "\" method=\"POST\" enctype=\"multipart/form-data\">
                            <div class=\"form-group row\">
                                <label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
                                <div class=\"col-sm-10\">
                                    <input type=\"text\" class=\"form-control\" id=\"title\"  placeholder=\"";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 25, $this->source); })()), "title", [], "any", false, false, false, 25), "html", null, true);
        echo "\" >
                                </div>
                            </div>
                            <div class=\"form-group row\">
                                <label for=\"abstract\" class=\"col-sm-2 col-form-label\">Resumé</label>
                                <div class=\"col-sm-10\">
                                    <textarea   rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"abstract\" placeholder=\"";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["course"]) || array_key_exists("course", $context) ? $context["course"] : (function () { throw new RuntimeError('Variable "course" does not exist.', 31, $this->source); })()), "abstract", [], "any", false, false, false, 31), "html", null, true);
        echo "\"></textarea>
                                </div>
                            </div>
                            <div class=\"form-group row\">
                                <label for=\"abstract\" class=\"col-sm-2 col-form-label\">Contenu</label>
                                <div class=\"col-sm-10\">
                                    <div id=\"videoMessage\" class=\"d-none\"></div>
                                    <input type=\"file\"  name=\"content\" class=\"dropify-fr\" id=\"content\" />
                                </div>
                            </div>
                            <div class=\"form-group row\">
                                <div class=\"col-sm-10\">
                                <button type=\"submit\" id=\"submit\"  class=\"btn btn-primary\">Modifier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 55
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "script"));

        // line 56
        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/js/custom.min.js"), "html", null, true);
        echo "\"></script>
 <script type=\"text/javascript\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/js/image-uploader.js"), "html", null, true);
        echo "\"></script>
<!-- jQuery file upload -->
<script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("admin/plugins/bower_components/dropify/dist/js/dropify.min.js"), "html", null, true);
        echo "\"></script>
";
        // line 61
        echo "<script>
    \$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar responseMessage=document.getElementById(\"responseMessage\");
\tvar videoMessage=document.getElementById(\"videoMessage\");
\t\$(\"#content\").change(function() {
\t\tvar file = this.files[0];
\t\tif(isVideo(file['name']))
\t\t{
\t\t\tvideoMessage.innerHTML=\"\";
\t\t\tvideoMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tvideoMessage.innerHTML=\"Le contenu doit être une video\";
\t\t\tvideoMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#content\").val('');
\t\t}
\t\t//console.log('the files', this.files)
\t});
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isVideo(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'm4v':
\t\t\tcase 'avi':
\t\t\tcase 'mpg':
\t\t\tcase 'mp4':
\t\t\t// etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\t
\t\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar file=document.getElementById('content').files[0];
\t\tvar id=\$('#courseId').html();
        console.log('the id',id);
        var fd=new FormData();
        fd.append('content',file);
        fd.append('id',id);
        fd.append('title',\$('#title').val());
        fd.append('abstract',\$('#abstract').val());
        AjaxFileFunction(fd);
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"";
        // line 151
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_edit");
        echo "\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
            if(result.data.statut==200)
            {
                responseMessage.innerHTML=result.data.message;
\t\t\t    responseMessage.setAttribute('class','alert alert-success');
            }
\t\t\telse
            {
                console.log('the message',result.data.message);
            }
\t\t\t
\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
 ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/courses/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  282 => 151,  190 => 61,  186 => 59,  181 => 57,  176 => 56,  166 => 55,  132 => 31,  123 => 25,  116 => 21,  112 => 20,  99 => 10,  93 => 6,  83 => 5,  70 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin-base.html.twig' %}
{%  block css %}
<link rel=\"stylesheet\" href=\"{{asset('admin/plugins/bower_components/dropify/dist/css/dropify.min.css')}}\">
{% endblock %}
{%  block content %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title\">Modification de cours/<a href=\"{{path('admin_course_list')}}\" class=\"btn btn-default\">Retour</a> </h4> 
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-2\">
                </div>
                <div class=\"col-8\">
                    <div class=\"white-box\">
                        <br/>
                        <div id=\"responseMessage\" class=\"d-none\"></div>
                        <div id=\"courseId\" class=\"d-none\">{{course.id}}</div>
                        <form  id=\"addForm\" action=\"{{path('course_new')}}\" method=\"POST\" enctype=\"multipart/form-data\">
                            <div class=\"form-group row\">
                                <label for=\"title\" class=\"col-sm-2 col-form-label\">Titre</label>
                                <div class=\"col-sm-10\">
                                    <input type=\"text\" class=\"form-control\" id=\"title\"  placeholder=\"{{course.title}}\" >
                                </div>
                            </div>
                            <div class=\"form-group row\">
                                <label for=\"abstract\" class=\"col-sm-2 col-form-label\">Resumé</label>
                                <div class=\"col-sm-10\">
                                    <textarea   rows=\"4\" style=\"overflow: hidden; word-wrap: break-word; resize: none; height: 160px; \" class=\"form-control\" id=\"abstract\" placeholder=\"{{course.abstract}}\"></textarea>
                                </div>
                            </div>
                            <div class=\"form-group row\">
                                <label for=\"abstract\" class=\"col-sm-2 col-form-label\">Contenu</label>
                                <div class=\"col-sm-10\">
                                    <div id=\"videoMessage\" class=\"d-none\"></div>
                                    <input type=\"file\"  name=\"content\" class=\"dropify-fr\" id=\"content\" />
                                </div>
                            </div>
                            <div class=\"form-group row\">
                                <div class=\"col-sm-10\">
                                <button type=\"submit\" id=\"submit\"  class=\"btn btn-primary\">Modifier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
{%  endblock %}
{% block script %}
 <script src=\"{{asset('admin/js/custom.min.js')}}\"></script>
 <script type=\"text/javascript\" src=\"{{asset('web/js/image-uploader.js')}}\"></script>
<!-- jQuery file upload -->
<script src=\"{{asset('admin/plugins/bower_components/dropify/dist/js/dropify.min.js')}}\"></script>
{# <script src=\"{{asset('web/js/dropzone.js')}}\"></script> #}
<script>
    \$(document).ready(function() {
        // Basic
        \$('.dropify').dropify();
        // Translated
        \$('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = \$('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm(\"Do you really want to delete \\\"\" + element.file.name + \"\\\" ?\");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = \$('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        \$('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
<script>
\tvar responseMessage=document.getElementById(\"responseMessage\");
\tvar videoMessage=document.getElementById(\"videoMessage\");
\t\$(\"#content\").change(function() {
\t\tvar file = this.files[0];
\t\tif(isVideo(file['name']))
\t\t{
\t\t\tvideoMessage.innerHTML=\"\";
\t\t\tvideoMessage.setAttribute('class','');
\t\t}
\t\telse
\t\t{
\t\t\tvideoMessage.innerHTML=\"Le contenu doit être une video\";
\t\t\tvideoMessage.setAttribute('class','alert alert-danger');
\t\t\t\$(\"#content\").val('');
\t\t}
\t\t//console.log('the files', this.files)
\t});
\tfunction getExtension(filename) {
\t\tvar parts = filename.split('.');
\t\treturn parts[parts.length - 1];
\t}
\tfunction isVideo(filename) {
\t\tvar ext = getExtension(filename);
\t\tswitch (ext.toLowerCase()) {
\t\t\tcase 'm4v':
\t\t\tcase 'avi':
\t\t\tcase 'mpg':
\t\t\tcase 'mp4':
\t\t\t// etc
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}
\t
\t\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\tvar file=document.getElementById('content').files[0];
\t\tvar id=\$('#courseId').html();
        console.log('the id',id);
        var fd=new FormData();
        fd.append('content',file);
        fd.append('id',id);
        fd.append('title',\$('#title').val());
        fd.append('abstract',\$('#abstract').val());
        AjaxFileFunction(fd);
\t});
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"{{path('admin_course_edit')}}\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tvar result=JSON.parse(data);
            if(result.data.statut==200)
            {
                responseMessage.innerHTML=result.data.message;
\t\t\t    responseMessage.setAttribute('class','alert alert-success');
            }
\t\t\telse
            {
                console.log('the message',result.data.message);
            }
\t\t\t
\t\t});
    }
\t\$(document).ajaxSend(function(event, request, settings) {
\t\t\$('#loading-indicator').show();
\t});

\t\$(document).ajaxComplete(function(event, request, settings) {
\t\t\$('#loading-indicator').hide();
\t});
</script> 
 {% endblock %}", "admin/courses/edit.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/admin/courses/edit.html.twig");
    }
}
