<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* teacher/courses/indexLite.html.twig */
class __TwigTemplate_8806df925b8e4b739dfbcc510fc522b567701df0a15b2fd55caeb971e813f848 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/courses/indexLite.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "teacher/courses/indexLite.html.twig"));

        // line 1
        if (((isset($context["courseRooms"]) || array_key_exists("courseRooms", $context) ? $context["courseRooms"] : (function () { throw new RuntimeError('Variable "courseRooms" does not exist.', 1, $this->source); })()) != null)) {
            echo " 
    ";
            // line 2
            $context["compteur"] = 0;
            // line 3
            echo "    
    ";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courseRooms"]) || array_key_exists("courseRooms", $context) ? $context["courseRooms"] : (function () { throw new RuntimeError('Variable "courseRooms" does not exist.', 4, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["courseRoom"]) {
                // line 5
                echo "        ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 5), "deleted", [], "any", false, false, false, 5) != "true")) {
                    // line 6
                    echo "            ";
                    $context["compteur"] = ((isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 6, $this->source); })()) + 1);
                    // line 7
                    echo "            <tr>
                <td style=\"color:black\">";
                    // line 8
                    echo twig_escape_filter($this->env, (isset($context["compteur"]) || array_key_exists("compteur", $context) ? $context["compteur"] : (function () { throw new RuntimeError('Variable "compteur" does not exist.', 8, $this->source); })()), "html", null, true);
                    echo "</td>
                <td style=\"color:black\">";
                    // line 9
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 9), "title", [], "any", false, false, false, 9), "html", null, true);
                    echo "</td>
                <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal";
                    // line 10
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10), "html", null, true);
                    echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                <td class=\"text-center\"> 
                    ";
                    // line 12
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 12), "comment", [], "any", false, false, false, 12)) {
                        // line 13
                        echo "                        <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 13), "id", [], "any", false, false, false, 13), "html", null, true);
                        echo "\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                    ";
                    } else {
                        // line 15
                        echo "                    ";
                    }
                    // line 16
                    echo "                </td>
                <td class=\"text-center\"> <a href=\"";
                    // line 17
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 17), "id", [], "any", false, false, false, 17)]), "html", null, true);
                    echo "\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
                <td style=\"color:black\">
                    ";
                    // line 19
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 19), "annexes", [], "any", false, false, false, 19) != null)) {
                        // line 20
                        echo "                        ";
                        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 20), "annexes", [], "any", false, false, false, 20)), "html", null, true);
                        echo "
                        ";
                        // line 21
                        $context["break"] = false;
                        // line 22
                        echo "                        ";
                        $context["i"] = 0;
                        // line 23
                        echo "                        ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 23), "annexes", [], "any", false, false, false, 23));
                        foreach ($context['_seq'] as $context["_key"] => $context["annexe"]) {
                            if ( !(isset($context["break"]) || array_key_exists("break", $context) ? $context["break"] : (function () { throw new RuntimeError('Variable "break" does not exist.', 23, $this->source); })())) {
                                // line 24
                                echo "                            ";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "title", [], "any", false, false, false, 24), "html", null, true);
                                echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 24), "html", null, true);
                                echo "\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                                <div id=\"annexeModal";
                                // line 25
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annexe"], "id", [], "any", false, false, false, 25), "html", null, true);
                                echo "\" class=\"modal fade\" role=\"dialog\">
                                <div i class=\"modal-dialog\" >
                                    <div class=\"modal-content\">
                                        <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                            <span class=\"modal-title\" id=\"myModalLabel\">
                                                <div class=\"row\">
                                                    <div class=\"col-md-2\">
                                                    </div>
                                                    <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                                </div> 
                                            </span> 
                                        </div>
                                        <div class=\"modal-body\">
                                            ";
                                // line 39
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 39), "annexes", [], "any", false, false, false, 39));
                                foreach ($context['_seq'] as $context["_key"] => $context["currentAnnexe"]) {
                                    // line 40
                                    echo "                                            <div class=\"row\">
                                                <div class=\"col-6\">
                                                    ";
                                    // line 42
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "title", [], "any", false, false, false, 42), "html", null, true);
                                    echo "
                                                </div>
                                                <div class=\"col-6\">
                                                    ";
                                    // line 45
                                    if (twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 45)) {
                                        // line 46
                                        echo "                                                        <a href=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 46))), "html", null, true);
                                        echo "\"  download=\"";
                                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("data/" . twig_get_attribute($this->env, $this->source, $context["currentAnnexe"], "file", [], "any", false, false, false, 46))), "html", null, true);
                                        echo "\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                    ";
                                    }
                                    // line 48
                                    echo "                                                </div>
                                            </div>
                                            ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currentAnnexe'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 51
                                echo "                                        </div>
                                        <div class=\"modal-footer\">
                                            <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                            <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </div>
                            ";
                                // line 60
                                $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 60, $this->source); })()) + 1);
                                // line 61
                                echo "                            ";
                                if (((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 61, $this->source); })()) == 1)) {
                                    // line 62
                                    echo "                                ";
                                    $context["break"] = true;
                                    // line 63
                                    echo "                            ";
                                }
                                // line 64
                                echo "                        ";
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annexe'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 65
                        echo "                    ";
                    } else {
                        // line 66
                        echo "                        0
                    ";
                    }
                    // line 68
                    echo "                </td>
                <td class=\"text-nowrap\">   
                    <a href=\"";
                    // line 70
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("teacher_course_edit_form", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 70), "id", [], "any", false, false, false, 70)]), "html", null, true);
                    echo "\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal";
                    // line 71
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["courseRoom"], "course", [], "any", false, false, false, 71), "id", [], "any", false, false, false, 71), "html", null, true);
                    echo "\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                </td>
            </tr>
            <!-- Modal -->
        ";
                }
                // line 75
                echo "                                                        
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['courseRoom'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 78
            echo "    ";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "teacher/courses/indexLite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 78,  223 => 75,  215 => 71,  211 => 70,  207 => 68,  203 => 66,  200 => 65,  193 => 64,  190 => 63,  187 => 62,  184 => 61,  182 => 60,  171 => 51,  163 => 48,  155 => 46,  153 => 45,  147 => 42,  143 => 40,  139 => 39,  122 => 25,  115 => 24,  109 => 23,  106 => 22,  104 => 21,  99 => 20,  97 => 19,  92 => 17,  89 => 16,  86 => 15,  80 => 13,  78 => 12,  73 => 10,  69 => 9,  65 => 8,  62 => 7,  59 => 6,  56 => 5,  52 => 4,  49 => 3,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if courseRooms != null %} 
    {% set compteur = 0 %}
    
    {% for courseRoom in courseRooms  %}
        {% if courseRoom.course.deleted != \"true\" %}
            {% set compteur=compteur+1%}
            <tr>
                <td style=\"color:black\">{{compteur}}</td>
                <td style=\"color:black\">{{courseRoom.course.title}}</td>
                <td class=\"text-center\">  <a href=\"#\" data-toggle=\"modal\" data-target=\"#abstractModal{{courseRoom.course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a></td>
                <td class=\"text-center\"> 
                    {% if courseRoom.course.comment %}
                        <a href=\"#\" data-toggle=\"modal\" data-target=\"#showCommentModal{{courseRoom.course.id}}\" title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Afficher</i></span></a>
                    {% else %}
                    {% endif %}
                </td>
                <td class=\"text-center\"> <a href=\"{{path('teacher_course_show',{'id':courseRoom.course.id})}}\"  title=\"Consulter\"><span class=\"btn btn-default\"><i class=\"fa fa-plus-square\">Consulter</i></span></a></td>
                <td style=\"color:black\">
                    {% if courseRoom.course.annexes != null %}
                        {{courseRoom.course.annexes|length }}
                        {% set break = false %}
                        {% set i = 0 %}
                        {% for annexe in courseRoom.course.annexes if not break %}
                            {{annexe.title}}<a href=\"#\" data-toggle=\"modal\" data-target=\"#annexeModal{{annexe.id}}\" title=\"Consulter\"><i class=\"fa fa-plus-square\"></i></a>
                                <div id=\"annexeModal{{annexe.id}}\" class=\"modal fade\" role=\"dialog\">
                                <div i class=\"modal-dialog\" >
                                    <div class=\"modal-content\">
                                        <div class=\"modal-header\" style=\"background-color:#03a9f3\">
                                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" style=\"color:white\"><strong>×</strong></button>
                                            <span class=\"modal-title\" id=\"myModalLabel\">
                                                <div class=\"row\">
                                                    <div class=\"col-md-2\">
                                                    </div>
                                                    <h4 style=\"color:white\" ><strong >Les annexes du cours</strong></h4>
                                                </div> 
                                            </span> 
                                        </div>
                                        <div class=\"modal-body\">
                                            {% for currentAnnexe in courseRoom.course.annexes %}
                                            <div class=\"row\">
                                                <div class=\"col-6\">
                                                    {{currentAnnexe.title}}
                                                </div>
                                                <div class=\"col-6\">
                                                    {% if currentAnnexe.file %}
                                                        <a href=\"{{asset('data/' ~ currentAnnexe.file)}}\"  download=\"{{asset('data/' ~ currentAnnexe.file)}}\" ><i class=\"fa fa-download\">Télécharger</i></a>
                                                    {% endif %}
                                                </div>
                                            </div>
                                            {% endfor %}
                                        </div>
                                        <div class=\"modal-footer\">
                                            <button type=\"button\" class=\"btn btn-outline-info waves-effect btn-rounded waves-light\">Valider</button>                                                    
                                            <button type=\"button\" class=\"btn btn-outline-danger waves-effect btn-rounded waves-light\" data-dismiss=\"modal\">Fermer</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </div>
                            {% set i = i+1 %}
                            {% if i==1 %}
                                {% set break = true %}
                            {% endif %}
                        {% endfor %}
                    {% else %}
                        0
                    {% endif %}
                </td>
                <td class=\"text-nowrap\">   
                    <a href=\"{{path('teacher_course_edit_form',{'id':courseRoom.course.id})}}\" data-toggle=\"tooltip\" data-original-title=\"modifier\"><span class=\"btn btn-default\"><i class=\"fa fa-pencil text-inverse\"></i></span> </a>
                    <a href=\"javascript:void(0)\" data-toggle=\"modal\" data-target=\"#deleteModal{{courseRoom.course.id}}\"  title=\"supprimer\"><span class=\"btn btn-default\"><i class=\"fa fa-trash text-danger\"></i></span> </a>
                </td>
            </tr>
            <!-- Modal -->
        {% endif %}                                                        
    {% endfor %}
{% else %}
    {# <tr>
        <td colspan=\"4\" align=\"center\"> Il n'ya aucun utilisateur </td>
    </tr> #}
{% endif %}", "teacher/courses/indexLite.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/teacher/courses/indexLite.html.twig");
    }
}
