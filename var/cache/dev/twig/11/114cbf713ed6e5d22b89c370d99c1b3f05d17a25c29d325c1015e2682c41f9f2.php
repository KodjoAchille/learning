<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* courses/temp.html.twig */
class __TwigTemplate_67f6e3eaaea54778f938188f3c170350aa2bedf4ac85c9e4df4b9bd8e97d2782 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/temp.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "courses/temp.html.twig"));

        // line 1
        echo "\$.ajax({
        type:'POST',
        dataType:'html',
        enctype:'multipart/form-data',
        url:\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_set_files");
        echo "\",
        data:fd,                
        contentType:false,
        cache:false,
        processData:false,
    });


        var fileType = file.type;
\t\tvar match = ['video/mp4', 'video/mkv', 'video/avi'];
\t\tif(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) || (fileType == match[3]) || (fileType == match[4]) || (fileType == match[5]))){
\t\t\talert('Sorry, only PDF, DOC, JPG, JPEG, & PNG files are allowed to upload.');
\t\t\t\$(\"#file\").val('');
\t\t\treturn false;
\t\t}

         \$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_set_files");
        echo "\",
\t\t\tdata:fd,                
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        });

function getExtension(filename) {
  var parts = filename.split('.');
  return parts[parts.length - 1];
}

function isImage(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
      //etc
      return true;
  }
  return false;
}

function isVideo(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'm4v':
    case 'avi':
    case 'mpg':
    case 'mp4':
      // etc
      return true;
  }
  return false;
}



//old
\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\t//alert(file.name+\" | \"+file.size+\" | \"+file.type);
\t\t
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\tif(\$content)
\t\t{
\t\t\tvar file=document.getElementById(\"content\").files[0];

\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\tfd.append('abstract',\$abstract);
\t\t\tfd.append('title',\$title);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});\t
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"";
        // line 97
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("course_new");
        echo "\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tconsole.log('good job');
\t\t\tvar result=JSON.parse(data);
\t\t\tconsole.log('the result',result);
\t\t});
    }

\t<script>
    function deleteCourse(id){
        AjaxFunction(id);
    }
    function AjaxFunction(id){
        \$.ajax({
            type:'POST',
            dataType:'html',
            url:'";
        // line 117
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_delete");
        echo "',
            data:{'id':id},
        }).then(function(data){
            var result=JSON.parse(data);
            if(result.data.statut==200)
            {
                \$('#currentModal').parent().modal('hide');
                console.log('the parent',\$('#currentModal').parent())
                
                swal({
                    text: result.data.message,
                    icon: \"success\",
                    buttons: \"ok\",
                });
                //var lite=\"";
        // line 131
        $this->loadTemplate("/admin/courses/index.html.twig", "courses/temp.html.twig", 131)->display(twig_array_merge($context, ["courses" => "theCourses"]));
        echo "\";
                //lite.replace('theCourses',result.data.courses);
                //\$('#dataContent').html(lite);
            }
            else
            {
                console.log(result.data.message);
            }
        });
    }
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "courses/temp.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 131,  170 => 117,  147 => 97,  72 => 25,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("\$.ajax({
        type:'POST',
        dataType:'html',
        enctype:'multipart/form-data',
        url:\"{{path('user_set_files')}}\",
        data:fd,                
        contentType:false,
        cache:false,
        processData:false,
    });


        var fileType = file.type;
\t\tvar match = ['video/mp4', 'video/mkv', 'video/avi'];
\t\tif(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) || (fileType == match[3]) || (fileType == match[4]) || (fileType == match[5]))){
\t\t\talert('Sorry, only PDF, DOC, JPG, JPEG, & PNG files are allowed to upload.');
\t\t\t\$(\"#file\").val('');
\t\t\treturn false;
\t\t}

         \$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"{{path('user_set_files')}}\",
\t\t\tdata:fd,                
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        });

function getExtension(filename) {
  var parts = filename.split('.');
  return parts[parts.length - 1];
}

function isImage(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
      //etc
      return true;
  }
  return false;
}

function isVideo(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'm4v':
    case 'avi':
    case 'mpg':
    case 'mp4':
      // etc
      return true;
  }
  return false;
}



//old
\$('#addForm').submit(function(e){
\t\te.preventDefault();
\t\te.stopPropagation();
\t\t//alert(file.name+\" | \"+file.size+\" | \"+file.type);
\t\t
\t\t\$title=\$('#title').val();
\t\t\$abstract=\$('#abstract').val();
\t\t\$content=\$('#content').val();
\t\t\$annexes=\$('#annexes').val();
\t\tif(\$content)
\t\t{
\t\t\tvar file=document.getElementById(\"content\").files[0];

\t\t\tvar fd=new FormData();
\t\t\tfd.append('content',file);
\t\t\tfd.append('abstract',\$abstract);
\t\t\tfd.append('title',\$title);
\t\t\tAjaxFileFunction(fd);
\t\t}
\t\telse
\t\t{
\t\t\tresponseMessage.innerHTML=\"veuillez ajouter le contenu du cours\";
\t\t\tresponseMessage.setAttribute('class','alert alert-danger');
\t\t\t//envoie de message d'erreur
\t\t}
\t});\t
\tfunction AjaxFileFunction(fd){
\t\t\$.ajax({
\t\t\ttype:'POST',
\t\t\tdataType:'html',
\t\t\tenctype:'multipart/form-data',
\t\t\turl:\"{{path('course_new')}}\",
\t\t\tdata:fd,               
\t\t\tcontentType:false,
\t\t\tcache:false,
\t\t\tprocessData:false,
        }).then(function(data){
\t\t\tconsole.log('good job');
\t\t\tvar result=JSON.parse(data);
\t\t\tconsole.log('the result',result);
\t\t});
    }

\t<script>
    function deleteCourse(id){
        AjaxFunction(id);
    }
    function AjaxFunction(id){
        \$.ajax({
            type:'POST',
            dataType:'html',
            url:'{{path('admin_course_delete')}}',
            data:{'id':id},
        }).then(function(data){
            var result=JSON.parse(data);
            if(result.data.statut==200)
            {
                \$('#currentModal').parent().modal('hide');
                console.log('the parent',\$('#currentModal').parent())
                
                swal({
                    text: result.data.message,
                    icon: \"success\",
                    buttons: \"ok\",
                });
                //var lite=\"{% include '/admin/courses/index.html.twig' with {'courses':'theCourses'} %}\";
                //lite.replace('theCourses',result.data.courses);
                //\$('#dataContent').html(lite);
            }
            else
            {
                console.log(result.data.message);
            }
        });
    }
</script>", "courses/temp.html.twig", "/media/achille/Nouveau nom/daabtech/learning/templates/courses/temp.html.twig");
    }
}
