<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
        'register' => [[], ['_controller' => 'App\\Controller\\AccountController::register'], [], [['text', '/register']], [], []],
        'login' => [[], ['_controller' => 'App\\Controller\\AccountController::login'], [], [['text', '/login']], [], []],
        'logout' => [[], ['_controller' => 'App\\Controller\\AccountController::logout'], [], [['text', '/logout']], [], []],
        'admin_index' => [[], ['_controller' => 'App\\Controller\\AdminController::index'], [], [['text', '/admin/']], [], []],
        'admin_course_validate_or_not' => [[], ['_controller' => 'App\\Controller\\AdminController::courseValidateOrNot'], [], [['text', '/admin/course/validate']], [], []],
        'admin_course_edit_form' => [['id'], ['_controller' => 'App\\Controller\\AdminController::courseEditForm'], [], [['text', '/editForm'], ['variable', '/', '[^/]++', 'id', true], ['text', '/admin/course']], [], []],
        'admin_course_edit' => [[], ['_controller' => 'App\\Controller\\AdminController::courseEdit'], [], [['text', '/admin/course/edit']], [], []],
        'admin_course_delete' => [[], ['_controller' => 'App\\Controller\\AdminController::courseDelete'], [], [['text', '/admin/course/delete']], [], []],
        'admin_course_list' => [[], ['_controller' => 'App\\Controller\\AdminController::courseList'], [], [['text', '/admin/course/list']], [], []],
        'admin_course_show' => [['id'], ['_controller' => 'App\\Controller\\AdminController::courseShow'], [], [['text', '/show'], ['variable', '/', '[^/]++', 'id', true], ['text', '/admin/course']], [], []],
        'admin_course_list_update' => [[], ['_controller' => 'App\\Controller\\AdminController::courseListUpdate'], [], [['text', '/admin/course/list/update']], [], []],
        'admin_course_valide' => [[], ['_controller' => 'App\\Controller\\AdminController::courseValide'], [], [['text', '/admin/course/valide']], [], []],
        'admin_course_notvalide' => [[], ['_controller' => 'App\\Controller\\AdminController::courseNotvalide'], [], [['text', '/admin/course/notvalide']], [], []],
        'admin_course_students' => [['id'], ['_controller' => 'App\\Controller\\AdminController::courseStudents'], [], [['text', '/students'], ['variable', '/', '[^/]++', 'id', true], ['text', '/admin/course']], [], []],
        'admin_user_list' => [[], ['_controller' => 'App\\Controller\\AdminController::userList'], [], [['text', '/admin/user/list']], [], []],
        'admin_user_teacher_list' => [[], ['_controller' => 'App\\Controller\\AdminController::userTeacherList'], [], [['text', '/admin/user/list']], [], []],
        'admin_user_student_list' => [[], ['_controller' => 'App\\Controller\\AdminController::userStudentList'], [], [['text', '/admin/user/list']], [], []],
        'admin_room_list' => [[], ['_controller' => 'App\\Controller\\AdminController::roomList'], [], [['text', '/admin/room/list']], [], []],
        'admin_room_list_update' => [[], ['_controller' => 'App\\Controller\\AdminController::roomListUpdate'], [], [['text', '/admin/room/list/update']], [], []],
        'admin_room_edit' => [[], ['_controller' => 'App\\Controller\\AdminController::roomEdit'], [], [['text', '/admin/room/edit']], [], []],
        'admin_room_delete' => [[], ['_controller' => 'App\\Controller\\AdminController::roomDelete'], [], [['text', '/admin/room/delete']], [], []],
        'admin_user_room_list' => [[], ['_controller' => 'App\\Controller\\AdminController::roomUserList'], [], [['text', '/admin/user/room/list']], [], []],
        'admin_room_add' => [[], ['_controller' => 'App\\Controller\\AdminController::roomAdd'], [], [['text', '/admin/room/add']], [], []],
        'admin_user_validate_or_not' => [[], ['_controller' => 'App\\Controller\\AdminController::userValidateOrNot'], [], [['text', '/admin/user/validate']], [], []],
        'admin_user_list_update' => [[], ['_controller' => 'App\\Controller\\AdminController::userListUpdate'], [], [['text', '/admin/user/list/update']], [], []],
        'annexe_new' => [[], ['_controller' => 'App\\Controller\\AnnexeController::new'], [], [['text', '/annexes/new']], [], []],
        'annexe_new_form' => [['id'], ['_controller' => 'App\\Controller\\AnnexeController::newForm'], [], [['text', '/newForm'], ['variable', '/', '[^/]++', 'id', true], ['text', '/annexes']], [], []],
        'course_index' => [[], ['_controller' => 'App\\Controller\\CourseController::index'], [], [['text', '/courses/']], [], []],
        'course_list_by_user' => [[], ['_controller' => 'App\\Controller\\CourseController::listByUser'], [], [['text', '/courses/byUser']], [], []],
        'course_index_lite' => [[], ['_controller' => 'App\\Controller\\CourseController::indexLite'], [], [['text', '/courses/lite']], [], []],
        'course_new' => [[], ['_controller' => 'App\\Controller\\CourseController::new'], [], [['text', '/courses/new']], [], []],
        'course_take' => [[], ['_controller' => 'App\\Controller\\CourseController::take'], [], [['text', '/courses/take']], [], []],
        'course_show' => [['id'], ['_controller' => 'App\\Controller\\CourseController::show'], [], [['text', '/show'], ['variable', '/', '[^/]++', 'id', true], ['text', '/courses']], [], []],
        'index' => [[], ['_controller' => 'App\\Controller\\DefaultController::index'], [], [['text', '/']], [], []],
        'room_index' => [[], ['_controller' => 'App\\Controller\\RoomController::index'], [], [['text', '/room/']], [], []],
        'room_user_index' => [[], ['_controller' => 'App\\Controller\\RoomController::roomUserIndex'], [], [['text', '/room/user']], [], []],
        'teacher_index' => [[], ['_controller' => 'App\\Controller\\TeacherController::index'], [], [['text', '/teachers/']], [], []],
        'teacher_room_display_name' => [[], ['_controller' => 'App\\Controller\\TeacherController::roomDisplayName'], [], [['text', '/teachersroom/display/name']], [], []],
        'teacher_course_by_room' => [['id'], ['_controller' => 'App\\Controller\\TeacherController::courseByRoom'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/teachers/course/byRoom']], [], []],
        'teacher_course_delete' => [[], ['_controller' => 'App\\Controller\\TeacherController::courseDelete'], [], [['text', '/teachers/course/delete']], [], []],
        'teacher_course_show' => [['id'], ['_controller' => 'App\\Controller\\TeacherController::courseShow'], [], [['text', '/show'], ['variable', '/', '[^/]++', 'id', true], ['text', '/teachers/course']], [], []],
        'teacher_course_edit_form' => [['id'], ['_controller' => 'App\\Controller\\TeacherController::courseEditForm'], [], [['text', '/editForm'], ['variable', '/', '[^/]++', 'id', true], ['text', '/teachers/course']], [], []],
        'teacher_course_list_update' => [[], ['_controller' => 'App\\Controller\\TeacherController::courseListUpdate'], [], [['text', '/teachers/course/list/update']], [], []],
        'teacher_room_user_list' => [['id'], ['_controller' => 'App\\Controller\\TeacherController::roomUserlist'], [], [['text', '/user/list'], ['variable', '/', '[^/]++', 'id', true], ['text', '/teachers/room']], [], []],
        'user_new' => [[], ['_controller' => 'App\\Controller\\UserController::new'], [], [['text', '/user/new']], [], []],
        'user_show' => [[], ['_controller' => 'App\\Controller\\UserController::show'], [], [['text', '/user/show']], [], []],
        'user_edit' => [[], ['_controller' => 'App\\Controller\\UserController::edit'], [], [['text', '/user/edit']], [], []],
        'fos_js_routing_js' => [['_format'], ['_controller' => 'fos_js_routing.controller::indexAction', '_format' => 'js'], ['_format' => 'js|json'], [['variable', '.', 'js|json', '_format', true], ['text', '/js/routing']], [], []],
        '_twig_error_test' => [['code', '_format'], ['_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code' => '\\d+'], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '\\d+', 'code', true], ['text', '/_error']], [], []],
        '_wdt' => [['token'], ['_controller' => 'web_profiler.controller.profiler::toolbarAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_wdt']], [], []],
        '_profiler_home' => [[], ['_controller' => 'web_profiler.controller.profiler::homeAction'], [], [['text', '/_profiler/']], [], []],
        '_profiler_search' => [[], ['_controller' => 'web_profiler.controller.profiler::searchAction'], [], [['text', '/_profiler/search']], [], []],
        '_profiler_search_bar' => [[], ['_controller' => 'web_profiler.controller.profiler::searchBarAction'], [], [['text', '/_profiler/search_bar']], [], []],
        '_profiler_phpinfo' => [[], ['_controller' => 'web_profiler.controller.profiler::phpinfoAction'], [], [['text', '/_profiler/phpinfo']], [], []],
        '_profiler_search_results' => [['token'], ['_controller' => 'web_profiler.controller.profiler::searchResultsAction'], [], [['text', '/search/results'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_open_file' => [[], ['_controller' => 'web_profiler.controller.profiler::openAction'], [], [['text', '/_profiler/open']], [], []],
        '_profiler' => [['token'], ['_controller' => 'web_profiler.controller.profiler::panelAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_router' => [['token'], ['_controller' => 'web_profiler.controller.router::panelAction'], [], [['text', '/router'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception' => [['token'], ['_controller' => 'web_profiler.controller.exception::showAction'], [], [['text', '/exception'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception_css' => [['token'], ['_controller' => 'web_profiler.controller.exception::cssAction'], [], [['text', '/exception.css'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
    ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && null !== $name) {
            do {
                if ((self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
                    unset($parameters['_locale']);
                    $name .= '.'.$locale;
                    break;
                }
            } while (false !== $locale = strstr($locale, '_', true));
        }

        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
