var BootstrapModaliOS = ( fonction () {

	var disableBodyScroll = ( function () { // Thijs Huijssoon https://gist.github.com/thuijssoon
	
	    / **
	     * Variables privées
	     * /
	    var _selector =  false ,
	        _element =  false ,
	        _clientY;
	
	    / **
	     * Polyfills pour Element.matches et Element.closest
	     * /
	    si ( ! Element . prototype . correspond )
	        Element . prototype . allumettes  =  élément . prototype . msMatchesSelector  ||
	        Element . prototype . webkitMatchesSelector ;
	
	    si ( ! Element . prototype . le plus proche )
	        Element . prototype . le plus proche  =  fonction ( s ) {
	            ancêtre var =  this ;
	            if ( ! document . documentElement . contient (el)) return  null ;
	            faire {
	                if ( ancêtre . correspond (s)) retourne ancêtre;
	                ancêtre =  ancêtre . parentElement ;
	            } while (ancêtre ! ==  null );
	            return el;
	        };
	
	    / **
	     * Empêcher la valeur par défaut sauf dans _selector
	     * 
	     * @param   event object event
	     * @return void
	     * /
	    var  preventBodyScroll  =  function ( event ) {
	        if ( false  === _element ||  ! événement . cible . plus proche (_selector)) {
	            événement . preventDefault ();
	        }
	    };
	
	    / **
	     * Cachez les coordonnées du client pour
	     * Comparaison
	     * 
	     * @param   event object event
	     * @return void
	     * /
	    var  captureClientY  =  function ( event ) {
	        // répond seulement à une seule touche
	        if ( event . targetTouches . length  ===  1 ) {
	            _clientY =  événement . targetTouches [ 0 ]. clientY ;
	        }
	    };
	
	    / **
	     * Détecter si l'élément est au sommet
	     * ou le bas de leur parchemin et empêcher
	     * l'utilisateur de défiler au-delà
	     * 
	     * @param   event object event
	     * @return void
	     * /
	    var  preventOverscroll  =  function ( event ) {
	
	        // répond seulement à une seule touche
		    if ( event . targetTouches . length  ! ==  1 ) {
		    	retour ;
		    }
	
		    var clientY =  événement . targetTouches [ 0 ]. clientY  - _clientY;
	
		    // L'élément en haut de son parchemin,
		    // et l'utilisateur fait défiler
		    if ( _element . scrollTop  ===  0  && clientY >  0 ) {
		        événement . preventDefault ();
		    }
	
		    // L'élément au bas de son parchemin,
		    // et l'utilisateur fait défiler
			// https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
			if (( _element . scrollHeight  -  _element . scrollTop  <=  _element . clientHeight ) && clientY <  0 ) {
		        événement . preventDefault ();
		    }
	
	    };
	
	    / **
	     * Désactiver le défilement du corps. Faire défiler avec le sélecteur est
	     * autorisé si un sélecteur est proposé.
	     * 
	     * @param   boolean permettre
	     * @param   string selector Sélecteur en élément pour changer l'autorisation de défilement
	     * @return void
	     * /
	     fonction de retour ( autoriser , sélecteur ) {
	    	if ( typeof selector ! ==  " undefined " ) {
		        _sélecteur = sélecteur;
		        _element =  document . querySelector (sélecteur);
	    	}
	
	        si ( vrai  === autoriser) {
	        	si ( faux  ! == _element) {
		            _element . addEventListener ( ' touchstart ' , captureClientY, {passif :  false });
		            _element . addEventListener ( ' touchmove ' , preventOverscroll, {passif :  false });
	        	}
	            document . corps . addEventListener ( " touchmove " , preventBodyScroll, {passif :  false });
	        } else {
	        	si ( faux  ! == _element) {
		            _element . removeEventListener ( ' touchstart ' , captureClientY, {passif :  false });
		            _element . removeEventListener ( ' touchmove ' , preventOverscroll, {passif :  false });
		        }
	          document . corps . removeEventListener ( " touchmove " , preventBodyScroll, {passif :  false });
	        }
	    };
	} ());
	
	fonction  adjustModal ( e ) {
			
		var modal =  $ ( ' .modal.show ' ) [ 0 ];
		var actual_viewport =  fenêtre . innerHeight ;
		var offset_y =  modal . getBoundingClientRect (). y ;
	
		modal . style . top  =  0 ;
		modal . style . en bas  =  0 ;
		var screen_height =  modal . scrollHeight ;
	
		if ( ! navigator . userAgent . match ( / (iPod | iPhone | iPad) / i ) ||  Math . abs ( fenêtre . orientation ) ! ==  90  || actual_viewport === screen_height) { // Uniquement pour Safari mobile dans mode paysage, lorsque la hauteur modale ne correspond pas à la fenêtre d'affichage
			
			retour ;
	
		}
		
		if ( typeof e ! ==  ' undefined ' ) { // Evénement de redimensionnement (des barres d'outils sont apparues en tapotant dans la zone supérieure ou inférieure
	
			modal . style . top  = ( 0  - offset_y) +  ' px ' ;
			modal . style . bottom  = (screen_height - actual_viewport + offset_y) +  ' px ' ;
			
		} else {
		
			if (actual_viewport <= screen_height) { // le modal est tronqué, ajuste son haut / bas
				
				if (( document . body . scrollHeight  +  document . body . getBoundingClientRect (). y ) === actual_viewport) { // page défilant en bas
	
					modal . style . en bas  =  0 ;
					modal . style . top  = (screen_height - actual_viewport) +  ' px ' ;
	
				} else {
	
					modal . style . top  =  0 ;
					modal . style . bottom  = (screen_height - actual_viewport) +  ' px ' ;
				}
			
			}
		
			if ( modal . getBoundingClientRect (). y  ! ==  0 ) { // Un peu en retrait
	
				modal . style . top  = ( parseInt ( modal . style . top ) -  modal . getBoundingClientRect (). y ) +  ' px ' ;
				modal . style . bottom  = ( parseInt ( modal . style . bottom ) +  modal . getBoundingClientRect (). y ) +  ' px ' ;
				
			}
			
			if ((actual_viewport +  parseInt ( modal . style . top )) +  parseInt ( modal . style . bottom )) > screen_height) { // Bug supplémentaire lors du défilement près du bas
				
				modal . style . bottom  = (screen_height - actual_viewport -  parseInt ( modal . style . top )) +  ' px ' ;
				
			}
		
		}
		
	}
	
	$ ( ' .modal ' ). chaque ( fonction () {
		
		var previousScrollY =  fenêtre . scrollY ;
	
		$ ( ceci ). on ( ' shown.bs.modal ' , fonction ( e ) {
			
			var active_modal =  ' .modal.show .modal-dialogue ' ;
			$ (active_modal) [ 0 ]. scrollTop  =  0 ;
			document . documentElement . classList . add ( ' no-scroll ' );
			disableBodyScroll ( true , active_modal);
			previousScrollY =  fenêtre . scrollY ;
			ajusterModal ();
			fenêtre . addEventListener ( ' resize ' , adjustModal, {passif :  false });
			$ (active_modal). sur ( ' clic ' , fonction ( e ) {
	
				if ( $ ( e . cible ). hasClass ( ' dialogue modal ' )) {
					
					$ ( e . target . parentNode ). modal ( ' cacher ' );
					
				}
				
			});
		
		});
		
		$ ( ceci ). on ( ' hidden.bs.modal ' , fonction ( e ) {
		
			document . documentElement . classList . remove ( ' no-scroll ' );
			$ ( ' corps ' ). animate ({scrollTop : previousScrollY}, 200 );
			disableBodyScroll ( false , ' .modal-dialog ' );
			fenêtre . removeEventListener ( ' resize ' , adjustModal, {passif :  false });
		
		});
		
	});

}) ();