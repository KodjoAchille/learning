$(document).on('change', '#epreuve_exam, #epreuve_serie', function () {
    let $field = $(this)
    let $examField = $('#epreuve_exam')
    let $form = $field.closest('form')
    let target = '#' + $field.attr('id').replace('serie', 'lesson').replace('exam', 'serie')
    // Les données à envoyer en Ajax
    let data = {}
    data[$examField.attr('name')] = $examField.val()
    data[$field.attr('name')] = $field.val()
    // On soumet les données
    $.post($form.attr('action'), data).then(function (data) {
      // On récupère le nouveau <select>
      let $input = $(data).find(target)
      // On remplace notre <select> actuel
      $(target).replaceWith($input)
    })
  })