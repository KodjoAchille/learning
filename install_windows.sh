#!/bin/sh

############################################
#DAAB TECH
#Installation du site de cours en Ligne
############################################

echo " "
echo " "
echo " "
echo " "
echo "|----------------------------------------------------------|"
echo "|              ENTREPRISE  DAAB TECH                       |"
echo "|----------------------------------------------------------|"

echo "NB:Décompressez le .tar vers le serveur avant de commencer toute opération..........................NB"
read -p  "Entrez le nom de serveur(Si c'est en local, mettez Localhost):   " host
read -p   "Entrez le nom de la base de données  " base
read -p   "Entrez le nom de l'utilisateur de la base de données  " user
read -p   "Entrez le mot de passe de l'utilisateur  " password
read -p   "Donnez le driver ex(mysql ou sqlite.....)   " driver
echo "Installation de composer"
#sudo sh ./composer/composer.sh
echo "Exécution des migrations"

  if [ "$base" != "" ] ; then
 		 echo "Creation de la base $base"

  		 echo -n "Confirmez-vous cette action ? [o/N] "
  		 read CONFIRM
     if [ "$CONFIRM" = "o" -o "$CONFIRM" = "O" ] ; then

    	echo " "
    	echo "Connexion MySQL ..."
    	echo " Donnez le mot de passe de root "

      #set PATH=%PATH%;

    	C:/xampp/mysql/bin/mysql.exe -u root -e "create database $base"
       if [ $? != "0" ] ; then
         echo "Probleme de connexion MySQL"
       else
        echo "Création avec succès"
       fi 
   fi
 fi

echo " # In all environments, the following files are loaded if they exist,
# the later taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.
#
# DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
#
# Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

### symfony/framework-bundle ###
APP_ENV=dev
#APP_ENV=prod
#APP_DEBUG=1
APP_SECRET=ffaac313168f718bc017fdf846252e9d
#TRUSTED_PROXIES=127.0.0.1,127.0.0.2
#TRUSTED_HOSTS='^localhost|example\.com$'
### symfony/framework-bundle ###

### doctrine/doctrine-bundle ###
# Format described at http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=$driver://$user:$password@$host:3306/$base
### doctrine/doctrine-bundle ###

### symfony/swiftmailer-bundle ###

# Delivery is disabled by default via "null://localhost"
#MAILER_URL=null://localhost
#MAILER_URL=gmail://228rme@gmail.com:999rme999@localhost
###symfony/swiftmailer-bundle ###" > ".env"




if [ -d "./src/Migrations" ];then
del ./src/Migrations/*;
fi
if [ -d "./var/cache/dev" ];then
rmdir ./var/cache/dev;
fi


 C:/xampp/php/php ./bin/console doctrine:migrations:diff





C:/xampp/php/php ./bin/console doctrine:migrations:migrate
echo "Accéder a votre site sur le lien http://localhost/learning/public/"





