-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 29 Mars 2019 à 07:18
-- Version du serveur :  5.7.25-0ubuntu0.18.04.2
-- Version de PHP :  7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `nikio`
--

-- --------------------------------------------------------

--
-- Structure de la table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `account`
--

INSERT INTO `account` (`id`, `username`, `email`, `password`, `priority`, `is_active`) VALUES
(1, '1achille', '1achille@gmail.com', '$2y$13$ltaxulXJx0DaKGHRY88uA.LfVx5pfoektLXFgccTL99W6pPOxwgDe', 3, 1),
(2, '2achille', '2achille@gmail.com', '$2y$13$IuGized6MKAeatdvgLrxG.eqZXKambNm5tHSJxKaec476lLwvkbz2', 0, 1),
(3, '3achille', '3achille@gmail.com', '$2y$13$3wTxt7TPSC76fa5tN7AAsurk/jqtwwIbGNTQT0Kve5jBFKRNCtjT6', 0, 1),
(4, 'Anisoft', 'lanicet17@gmail.com', '$2y$13$eiuNuAkPhVZNlViocvCxk.Qi8N8BzZSk9jouXOclokesmBPj2YnWq', 3, 1),
(5, 'test', 'test@gmail.com', '$2y$13$pMvrdQjO/ClJ64Q5tRy6WO5zzIQkSYXJpW0x0x3X.HUUUl7F6N5Ca', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `comment`
--

INSERT INTO `comment` (`id`, `email`, `content`, `etat`) VALUES
(2, 'lanicet17@gmail.com', 'Juste un commentaire pour tester voir si cela marche.', 1),
(3, 'lanicet17@gmail.com', 'Un second test pour voir comment réagirait la page de gestion des commenataires.', 1),
(4, 'fqjzebfq@gmail.com', 'test 3', 1),
(5, 'lanicet17@gmail.com', 'nhb,kik k n gyb uhb i b 5oih i', 0),
(6, 'lanicet17@gmail.com', ',kuhuilyukgjlbyub hibllb', 0),
(7, 'lanicet17@gmail.com', 'fjkjklguljkjljklhklhklkhlkjhljkhlkhlkjhl', 0);

-- --------------------------------------------------------

--
-- Structure de la table `exam`
--

CREATE TABLE `exam` (
  `id` int(11) NOT NULL,
  `degree` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `exam`
--

INSERT INTO `exam` (`id`, `degree`) VALUES
(1, 'BEPC'),
(2, 'BAC I'),
(3, 'BAC II');

-- --------------------------------------------------------

--
-- Structure de la table `lesson`
--

CREATE TABLE `lesson` (
  `id` int(11) NOT NULL,
  `serie_id` int(11) NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `lesson`
--

INSERT INTO `lesson` (`id`, `serie_id`, `designation`) VALUES
(1, 1, 'HG'),
(2, 2, 'MATH'),
(3, 2, 'PHYSIQUE CHIMIE'),
(4, 2, 'ANGLAIS'),
(5, 2, 'FRANCAIS'),
(6, 2, 'SVT'),
(7, 2, 'ALLEMAND'),
(8, 2, 'ESPAGNOL'),
(9, 2, 'RUSSE'),
(10, 2, 'ARABE'),
(11, 2, 'DESSIN'),
(12, 2, 'EWE'),
(13, 2, 'COUTURE'),
(14, 3, 'MATH'),
(15, 3, 'PHYSIQUE CHIMIE'),
(16, 3, 'ANGLAIS'),
(17, 3, 'FRANCAIS'),
(18, 3, 'SVT'),
(19, 3, 'ALLEMAND'),
(20, 3, 'ESPAGNOL'),
(21, 3, 'RUSSE'),
(22, 3, 'ARABE'),
(23, 3, 'DESSIN'),
(24, 3, 'EWE'),
(25, 3, 'COUTURE'),
(26, 14, 'MATH'),
(27, 14, 'PHYSIQUE CHIMIE'),
(28, 14, 'ANGLAIS'),
(29, 14, 'FRANCAIS'),
(30, 14, 'SVT'),
(31, 14, 'ALLEMAND'),
(32, 14, 'ESPAGNOL'),
(33, 14, 'RUSSE'),
(34, 14, 'ARABE'),
(35, 14, 'DESSIN'),
(36, 14, 'EWE'),
(37, 14, 'COUTURE'),
(38, 1, 'SVT'),
(39, 1, 'MATH'),
(40, 1, 'PHYSIQUE CHIMIE'),
(41, 1, 'DESSIN'),
(42, 1, 'EWE'),
(43, 1, 'COUTURE'),
(44, 1, 'MUSIQUE'),
(45, 1, 'ECM'),
(46, 1, 'DICTEE QUESTION'),
(47, 1, 'REDACTION'),
(48, 1, 'ANGLAIS'),
(49, 1, 'KABYE'),
(50, 1, 'ARABE'),
(51, 1, 'AGRICULTURE'),
(52, 1, 'MUSIQUE');

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190209135400', '2019-02-20 15:26:23'),
('20190212223014', '2019-02-20 15:26:25'),
('20190218154231', '2019-02-20 15:26:26'),
('20190220152946', '2019-02-20 15:30:05'),
('20190302140235', '2019-03-02 14:03:12');

-- --------------------------------------------------------

--
-- Structure de la table `serie`
--

CREATE TABLE `serie` (
  `id` int(11) NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `serie`
--

INSERT INTO `serie` (`id`, `designation`, `exam_id`) VALUES
(1, 'Sans Serie', 1),
(2, 'D', 2),
(3, 'A4', 2),
(4, 'G1', 2),
(5, 'G2', 2),
(6, 'G3', 2),
(7, 'A4', 3),
(8, 'D', 3),
(9, 'C', 3),
(10, 'G1', 3),
(11, 'G2', 3),
(12, 'G3', 3),
(13, 'F', 3),
(14, 'C', 2),
(15, 'F', 2);

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` int(11) NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `period` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ndownload` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `test`
--

INSERT INTO `test` (`id`, `lesson_id`, `user_id`, `file_name`, `etat`, `year`, `period`, `ndownload`) VALUES
(7, 45, 3, 'NIKIO_0_BEPC_Sans Serie_2017_ECM.pdf', 0, '2017', 'Juillet 2017', NULL),
(8, 39, 3, 'NIKIO_0_BEPC_Sans Serie_2013_MATH.pdf', 0, '2013', 'Juin 2013', NULL),
(9, 39, 3, 'NIKIO_0_BEPC_Sans Serie_2014_MATH.pdf', 0, '2014', 'Juin 2014', NULL),
(10, 39, 3, 'NIKIO_0_BEPC_Sans Serie_2015_MATH.pdf', 0, '2015', 'Juin 2015', NULL),
(11, 39, 3, 'NIKIO_0_BEPC_Sans Serie_2016_MATH.pdf', 0, '2016', 'Juin 2016', NULL),
(12, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2001_PHYSIQUE CHIMIE.pdf', 0, '2001', 'Juin 2001', NULL),
(13, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2002_PHYSIQUE CHIMIE.pdf', 0, '2002', 'Juin 2002', NULL),
(14, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2005_PHYSIQUE CHIMIE.pdf', 0, '2005', 'Juin 2005', NULL),
(15, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2008_PHYSIQUE CHIMIE.pdf', 0, '2008', 'Juin 2008', NULL),
(16, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2010_PHYSIQUE CHIMIE.pdf', 0, '2010', 'Juin 2010', NULL),
(17, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2012_PHYSIQUE CHIMIE.pdf', 0, '2012', 'Juin 2012', NULL),
(18, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2013_PHYSIQUE CHIMIE.pdf', 0, '2013', 'Juin 2013', NULL),
(19, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2014_PHYSIQUE CHIMIE.pdf', 0, '2014', 'Juin 2014', NULL),
(20, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2015_PHYSIQUE CHIMIE.pdf', 0, '2015', 'Juin 2015', NULL),
(21, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2016_PHYSIQUE CHIMIE.pdf', 0, '2016', 'Juin 2016', NULL),
(22, 40, 3, 'NIKIO_0_BEPC_Sans Serie_2017_PHYSIQUE CHIMIE.pdf', 0, '2017', 'Juin 2017', NULL),
(23, 38, 3, 'NIKIO_0_BEPC_Sans Serie_2015_SVT.pdf', 0, '2015', 'Juin 2015', NULL),
(24, 38, 3, 'NIKIO_0_BEPC_Sans Serie_2016_SVT.pdf', 0, '2016', 'Juin 2016', NULL),
(25, 38, 3, 'NIKIO_0_BEPC_Sans Serie_2017_SVT.pdf', 0, '2017', 'Juin 2017', NULL),
(26, 1, 3, 'NIKIO_0_BEPC_Sans Serie_2011_HG.pdf', 0, '2011', 'Mai 2011', NULL),
(27, 1, 3, 'NIKIO_0_BEPC_Sans Serie_2014_HG.pdf', 0, '2014', 'Août 2014', NULL),
(28, 1, 3, 'NIKIO_0_BEPC_Sans Serie_2015_HG.pdf', 0, '2015', 'Juin 2015', NULL),
(29, 1, 3, 'NIKIO_0_BEPC_Sans Serie_2016_HG.pdf', 0, '2016', 'Juin 2016', NULL),
(30, 1, 3, 'NIKIO_0_BEPC_Sans Serie_2017_HG.pdf', 0, '2017', 'Juillet 2017', NULL),
(31, 19, 3, 'BAC I-A4-2009-ALLEMAND.pdf', 1, '2009', 'Juin 2009', NULL),
(32, 19, 3, 'BAC I-A4-2011-ALLEMAND.pdf', 1, '2011', 'Mai 2011', NULL),
(33, 19, 3, 'BAC I-A4-2012-ALLEMAND.pdf', 1, '2012', 'Mai 2012', NULL),
(34, 19, 3, 'BAC I-A4-2013-ALLEMAND.pdf', 1, '2013', 'Mai 2013', NULL),
(35, 19, 3, 'BAC I-A4-2014-ALLEMAND.pdf', 1, '2014', 'Mai 2014', NULL),
(36, 19, 3, 'BAC I-A4-2015-ALLEMAND.pdf', 1, '2015', 'Juillet 2015', NULL),
(37, 19, 3, 'BAC I-A4-2016-ALLEMAND.pdf', 1, '2016', 'Mai 2016', NULL),
(39, 15, 3, 'BAC I-A4-2008-PHYSIQUE CHIMIE.pdf', 1, '2008', 'Juin 2008', NULL),
(40, 15, 3, 'BAC I-A4-2009-PHYSIQUE CHIMIE.pdf', 1, '2009', 'Mai 2009', NULL),
(41, 15, 3, 'BAC I-A4-2010-PHYSIQUE CHIMIE.pdf', 1, '2010', 'Mai 2010', NULL),
(42, 15, 3, 'BAC I-A4-2011-PHYSIQUE CHIMIE.pdf', 1, '2011', 'Mai 2011', NULL),
(43, 15, 3, 'BAC I-A4-2013-PHYSIQUE CHIMIE.pdf', 1, '2013', 'Mai 2013', NULL),
(44, 18, 3, 'BAC I-A4-2013-SVT.pdf', 1, '2013', 'Mai 2013', NULL),
(45, 18, 3, 'BAC I-A4-2014-SVT.pdf', 1, '2014', 'Mai 2014', NULL),
(46, 18, 3, 'BAC I-A4-2016-SVT.pdf', 1, '2016', 'Mai 2016', NULL),
(47, 14, 3, 'BAC I-A4-2015-MATH.pdf', 1, '2015', 'Juillet 2015', NULL),
(48, 4, 3, 'BAC I-D-2015-ANGLAIS.pdf', 1, '2015', 'Juillet 2015', NULL),
(49, 16, 3, 'BAC I-A4-2015-ANGLAIS.pdf', 1, '2015', 'Juillet 2015', NULL),
(50, 28, 3, 'BAC I-C-2015-ANGLAIS.pdf', 1, '2015', 'Juillet 2015', NULL),
(51, 4, 3, 'BAC I-D-2017-ANGLAIS.pdf', 1, '2017', 'Juin 2017', NULL),
(52, 16, 3, 'BAC I-A4-2017-ANGLAIS.pdf', 1, '2017', 'Juin 2017', NULL),
(53, 28, 3, 'BAC I-C-2017-ANGLAIS.pdf', 1, '2017', 'Juin 2017', NULL),
(54, 5, 3, 'BAC I-D-2013-FRANCAIS.pdf', 1, '2013', 'Mai 2013', NULL),
(55, 17, 3, 'BAC I-A4-2013-FRANCAIS.pdf', 1, '2013', 'Mai 2013', NULL),
(56, 29, 3, 'BAC I-C-2013-FRANCAIS.pdf', 1, '2013', 'Mai 2013', NULL),
(57, 5, 3, 'BAC I-D-2017-FRANCAIS.pdf', 1, '2017', 'Juillet 2017', NULL),
(58, 17, 3, 'BAC I-A4-2017-FRANCAIS.pdf', 1, '2017', 'Juillet 2017', NULL),
(59, 29, 3, 'BAC I-C-2017-FRANCAIS.pdf', 1, '2017', 'Juillet 2017', NULL),
(60, 26, 3, 'BAC I-C-2011-MATH.pdf', 1, '2011', 'Juin 2011', NULL),
(61, 26, 3, 'NIKIO_1_BAC I_C_2006_MATH.pdf', 1, '2006', 'Juin 2006', NULL),
(68, 49, 3, 'NIKIO_1_BEPC_Sans Serie_2018_KABYE.pdf', 1, '2018', 'Juillet 2018', NULL),
(69, 50, 3, 'NIKIO_1_BEPC_Sans Serie_2018_ARABE.pdf', 1, '2018', 'Juillet 2018', NULL),
(70, 1, 3, 'NIKIO_1_BEPC_Sans Serie_2018_HG.pdf', 1, '2018', 'Juillet 2018', NULL),
(71, 40, 3, 'NIKIO_1_BEPC_Sans Serie_2018_PHYSIQUE CHIMIE.pdf', 1, '2018', 'Juillet 2018', NULL),
(72, 51, 3, 'NIKIO_1_BEPC_Sans Serie_2018_AGRICULTURE.pdf', 1, '2018', 'Juillet 2018', NULL),
(73, 52, 3, 'NIKIO_1_BEPC_Sans Serie_2018_MUSIQUE.pdf', 1, '2018', 'Juillet 2018', NULL),
(74, 41, 3, 'NIKIO_1_BEPC_Sans Serie_2018_DESSIN.pdf', 1, '2018', 'Juillet 2018', NULL),
(75, 43, 3, 'NIKIO_1_BEPC_Sans Serie_2018_COUTURE.pdf', 1, '2018', 'Juillet 2018', NULL),
(76, 47, 3, 'NIKIO_1_BEPC_Sans Serie_2018_REDACTION.pdf', 1, '2018', 'Juillet 2018', NULL),
(77, 38, 3, 'NIKIO_1_BEPC_Sans Serie_2018_SVT.pdf', 1, '2018', 'Juillet 2018', NULL),
(78, 46, 3, 'NIKIO_1_BEPC_Sans Serie_2018_DICTEE QUESTION.pdf', 1, '2018', 'Juillet 2018', NULL),
(79, 45, 3, 'NIKIO_1_BEPC_Sans Serie_2018_ECM.pdf', 1, '2018', 'Juillet 2018', NULL),
(80, 39, 3, 'NIKIO_1_BEPC_Sans Serie_2018_MATH.pdf', 1, '2018', 'Juillet 2018', NULL),
(81, 48, 3, 'NIKIO_1_BEPC_Sans Serie_2018_ANGLAIS.pdf', 1, '2018', 'Juillet 2018', NULL),
(82, 42, 3, 'NIKIO_1_BEPC_Sans Serie_2018_EWE.pdf', 1, '2018', 'Juillet 2018', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `account_id`, `name`, `first_name`, `phone`, `address`) VALUES
(1, 1, 'GLEY', 'Kodjo Achille', '+228 99518522', 'ADIDOGOME'),
(3, 4, 'AGBONON EDAGBEDJI', 'Yao Anicet', '97519850', 'Kpota'),
(4, 5, 'TESTO', 'Test', '97-51-98-50', 'Testland');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7D3656A4F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_7D3656A4E7927C74` (`email`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lesson`
--
ALTER TABLE `lesson`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F87474F3D94388BD` (`serie_id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `serie`
--
ALTER TABLE `serie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_AA3A9334578D5E91` (`exam_id`);

--
-- Index pour la table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D87F7E0CA76ED395` (`user_id`),
  ADD KEY `IDX_D87F7E0CCDF80196` (`lesson_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D6499B6B5FBA` (`account_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `exam`
--
ALTER TABLE `exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `lesson`
--
ALTER TABLE `lesson`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT pour la table `serie`
--
ALTER TABLE `serie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `lesson`
--
ALTER TABLE `lesson`
  ADD CONSTRAINT `FK_F87474F3D94388BD` FOREIGN KEY (`serie_id`) REFERENCES `serie` (`id`);

--
-- Contraintes pour la table `serie`
--
ALTER TABLE `serie`
  ADD CONSTRAINT `FK_AA3A9334578D5E91` FOREIGN KEY (`exam_id`) REFERENCES `exam` (`id`);

--
-- Contraintes pour la table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `FK_D87F7E0CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_D87F7E0CCDF80196` FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D6499B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
