<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $edited;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $validated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teacher;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Annexe", mappedBy="course")
     */
    private $annexes;

    /**
     * @ORM\Column(type="text")
     */
    private $abstract;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseRoom", mappedBy="course")
     */
    private $courseRooms;

    public function __construct()
    {
        $this->annexes = new ArrayCollection();
        $this->courseRooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getEdited(): ?bool
    {
        return $this->edited;
    }

    public function setEdited(?bool $edited): self
    {
        $this->edited = $edited;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getValidated(): ?bool
    {
        return $this->validated;
    }

    public function setValidated(?bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * @return Collection|Annexe[]
     */
    public function getAnnexes(): Collection
    {
        return $this->annexes;
    }

    public function addAnnex(Annexe $annex): self
    {
        if (!$this->annexes->contains($annex)) {
            $this->annexes[] = $annex;
            $annex->setCourse($this);
        }

        return $this;
    }

    public function removeAnnex(Annexe $annex): self
    {
        if ($this->annexes->contains($annex)) {
            $this->annexes->removeElement($annex);
            // set the owning side to null (unless already changed)
            if ($annex->getCourse() === $this) {
                $annex->setCourse(null);
            }
        }

        return $this;
    }

    public function getAbstract(): ?string
    {
        return $this->abstract;
    }

    public function setAbstract(string $abstract): self
    {
        $this->abstract = $abstract;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|CourseRoom[]
     */
    public function getCourseRooms(): Collection
    {
        return $this->courseRooms;
    }

    public function addCourseRoom(CourseRoom $courseRoom): self
    {
        if (!$this->courseRooms->contains($courseRoom)) {
            $this->courseRooms[] = $courseRoom;
            $courseRoom->setCourse($this);
        }

        return $this;
    }

    public function removeCourseRoom(CourseRoom $courseRoom): self
    {
        if ($this->courseRooms->contains($courseRoom)) {
            $this->courseRooms->removeElement($courseRoom);
            // set the owning side to null (unless already changed)
            if ($courseRoom->getCourse() === $this) {
                $courseRoom->setCourse(null);
            }
        }

        return $this;
    }
}
