<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 */
class Room
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseRoom", mappedBy="room")
     */
    private $courseRooms;

    public function __construct()
    {
        $this->courseRooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|CourseRoom[]
     */
    public function getCourseRooms(): Collection
    {
        return $this->courseRooms;
    }

    public function addCourseRoom(CourseRoom $courseRoom): self
    {
        if (!$this->courseRooms->contains($courseRoom)) {
            $this->courseRooms[] = $courseRoom;
            $courseRoom->setRoom($this);
        }

        return $this;
    }

    public function removeCourseRoom(CourseRoom $courseRoom): self
    {
        if ($this->courseRooms->contains($courseRoom)) {
            $this->courseRooms->removeElement($courseRoom);
            // set the owning side to null (unless already changed)
            if ($courseRoom->getRoom() === $this) {
                $courseRoom->setRoom(null);
            }
        }

        return $this;
    }
}
