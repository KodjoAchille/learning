<?php
// src/Service/RoleManager.php
namespace App\Service;

use App\Entity\Role;
use App\Repository\RoleRepository;


class RoleManager
{
    public function __construct()
    {
    }

    public function hasRole($label, RoleRepository $roleRepository)
    {
        $number=$roleRepository->FindOneByLabel($label)->getNumber();
        return $this->getUser()->getPriority()==$number;
    }
   
}
