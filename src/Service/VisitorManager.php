<?php
// src/Service/RoleManager.php
namespace App\Service;

use App\Entity\Visitor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



class VisitorManager extends AbstractController
{
    public function __construct()
    {
    }

    public function manage(Request $request)
    {
        //récuppération de l'addresse ip du visiteur
        $em=$this->getDoctrine()->getManager();
        $ip=$request->getClientIp();
        $repo=$em->getRepository(Visitor::class);
        
         

        $ipexist=$repo->findOneByIp($ip);
        //vérifions si l'addresse ip est dans la table de visiteur
        if($ipexist)
        {   
            $ipexist->setTimestamp(time());
            $em->flush();
        }
        else
        {
            $visitor = new Visitor();
            $visitor->setIP($ip);
            $visitor->setTimestamp(time());
            $em->persist($visitor);
            $em->flush();
        }

        // //ETAPE 2 : On supprime toutes les e,trees dont le timestamp est plus vieux que 5 min
        // //On stocke dans une variable le timestamp qu'il était il y a 5 min
        // $visitors=$repo->findAll();
        // $timestamp_5min = time() - (60 * 5);//nombre de seconde ecoulé en 5min
        // foreach($visitors as $currentVisitor)
        // {
        //     if($currentVisitor->getTimestamp()>$timestamp_5min)
        //     {
        //         $em->remove($currentVisitor);
        //         $em->flush(); 
        //     }
        // }

    }

    public function countVisitor()
    {
        $em=$this->getDoctrine()->getManager();
        $repo=$em->getRepository(Visitor::class);
        //comptons maintenant le nombre de visiteur
        return sizeof($repo->findAll());
    }
}
