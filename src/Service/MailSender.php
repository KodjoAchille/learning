<?php

// src/Service/MailSender.php

namespace App\Service;

use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\IPRepository;
use Doctrine\ORM\EntityManagerInterface;


class MailSender
{
    protected $mailer;
    private $templating;    
    private $em;

    public function __construct(\Swift_Mailer $mailer,\Twig_Environment $templating,EntityManagerInterface $em)
    {
        $this->mailer = $mailer;
        $this->templating=$templating;
        $this->em = $em;
    }

    // Méthode pour notifier par e-mail un administrateur

    public function sendMail($email,$subject)
    {
        $message = (new \Swift_Message())
          ->setSubject($subject)
          ->setFrom(['info@rme-nikio.com'=>'NIKIO'])
          ->setTo($email)
          ->setBody('Felicitation nous sommes heureux de vous compter parmi nos abonnées..');
        
        $this->mailer->send($message);
    }
}