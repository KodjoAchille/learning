<?php

namespace App\Repository;

use App\Entity\CourseRoom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method CourseRoom|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseRoom|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseRoom[]    findAll()
 * @method CourseRoom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRoomRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CourseRoom::class);
    }

    // /**
    //  * @return CourseRoom[] Returns an array of CourseRoom objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseRoom
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
