<?php

namespace App\Controller;

use App\Entity\UserCourse;
use App\Entity\User;
use App\Entity\Room;
use App\Entity\RoomUser;
use App\Entity\Course;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\FileUploader;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/courses")    
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/", name="course_index")    
     */
    public function index(Request $request)
    {   
        $courses=$this->getDoctrine()->getManager()->getRepository(Course::class)
        ->findByValidated(true);
        return $this->render('courses/index.html.twig',[
            'courses'=>$courses,
        ]);
    }

    /**
     * @Route("/byUser", name="course_list_by_user")
     * @Security("is_granted('ROLE_USER')")       
     */
    public function listByUser(Request $request)
    {   
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository(User::class);
        $account=$this->getUser();
        $user=$repository->findOneByAccount($account);
        if($account->hasRole('ROLE_TEACHER'))
        {
            $userCourseRepository=$em->getRepository(UserCourse::Class);
            $userCourses=$userCourseRepository->findByUser($user);
            return $this->render('user/teacherCourses.html.twig',[
                'userCourses'=>$userCourses,
            ]);
        }
        else
        {
            $RAW_QUERY = 'SELECT * FROM course_room,course
                            WHERE course_room.course_id=course.id  AND course_room.room_id  
                            IN (SELECT room_id FROM room_user WHERE room_user.user_id = :theUser )';
            $statement = $em->getConnection()->prepare($RAW_QUERY);
            // Set parameters 
            $statement->bindValue('theUser', $user->getId());
            $statement->execute();
            $courseRooms = $statement->fetchAll();
            return $this->render('user/courses.html.twig',[
                'courseRooms'=>$courseRooms,
            ]);
        }
        
    }

    /**
     * @Route("/lite", name="course_index_lite")    
     */
    public function indexLite(Request $request)
    {   
        $courses=$this->getDoctrine()->getManager()->getRepository(Course::class)
        ->findBy(
            ['validated'=>'true'],
            null,
            3
        );
        return $this->render('courses/indexLite.html.twig',[
            'courses'=>$courses,
        ]);
    }
    
    /**
     * @Route("/new", name="course_new")
     * @security("is_granted('ROLE_USER')")
     */
    public function new(Request $request,FileUploader $fileUploader)
    {
        $course=new Course();
        $em=$this->getDoctrine()->getManager();
        $userRepo=$em->getRepository(User::class);
        $roomRepository=$em->getRepository(Room::class);
        $account=$this->getUser();        
        $user=$userRepo->findOneByAccount($account);
        $file=$request->files->get('content');
        if($file)
        {   
            $course->setTitle(htmlspecialchars($request->get('title')));
            $course->setAbstract(htmlspecialchars($request->get('abstract')));
            //uploading file content
            $fileName = $fileUploader->upload($file);
            $course->setContent($fileName);
            $course->setTeacher($user);
            $rooms=$request->get('rooms');
    		$rooms_ar = explode(',', $rooms);
            //dump($rooms_ar);
            foreach($rooms_ar as $roomId)
            {
                $room=$roomRepository->findOneById($roomId);
                $roomUser=new RoomUser();
                $roomUser->setUser($user);
                $roomUser->setRoom($room);
                $em->persist($roomUser);
            }
            $em->persist($course);
            $em->flush();
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'courseId'=>$course->getId(),
            ]]);
        }
        return $this->render('courses/new.html.twig');
    }

    /**
     * @Route("/take", name="course_take")
     */
    public function take(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $userRepo=$em->getRepository(User::class);
        $account=$this->getUser();        
        $user=$userRepo->findOneByAccount($account);
        if($user)
        {
            $courseRepo=$em->getRepository(Course::class);
            $course=$courseRepo->findOneBy($request->get('id'));
            if($course)
            {
                $userCourseRepo=$em->getRepository(Course::class);
                $exist=$userCourseRepo->findOneBy([
                    'user'=>$user,
                    'course'=>$course
                ]);
                if(!$exist)
                {
                    $userCourse= new UserCourse();
                    $userCourse->setUser($user);
                    $userCourse->setCourse($course);
                    $em->persist($userCourse);
                    $em->flush();
                    return new JsonResponse(['data'=>[
                        'statut'=>200,
                        'message'=>"cours ajoutée avec sucess veuillez consulter le cours",
                    ]]);
                }
                else
                {
                    return new JsonResponse(['data'=>[
                        'statut'=>500,
                        'message'=>"vous avez déjà ajouter ce cours au besoin vous pouvez le consulter",
                    ]]);
                }
                
            }
            else
            {
                return new JsonResponse(['data'=>[
                    'statut'=>500,
                    'message'=>"il y a pas d'entité cours correspondant à l'id",
                ]]);
            }
        }
        else
        {
            return $this->redirectToRoute('index');
        }
    }
    /**
     * @Route("/{id}/show", name="course_show")    
     */
    public function show(Request $request,Course $course)
    {   
        return $this->render('courses/show.html.twig',[
            'course'=>$course,
        ]);
    }

}
