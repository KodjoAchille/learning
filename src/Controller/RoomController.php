<?php

namespace App\Controller;

use App\Entity\RoomUser;
use App\Entity\User;
use App\Entity\Room;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\FileUploader;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/room")    
 */
class RoomController extends AbstractController
{
    /**
     * @Route("/", name="room_index")    
     */
    public function index(Request $request)
    {   
        $rooms=$this->getDoctrine()->getManager()->getRepository(Room::class)
        ->findAll();
        return $this->render('rooms/indexLite.html.twig',[
            'rooms'=>$rooms,
        ]);
    }

    /**
     * @Route("/user", name="room_user_index")
     *  @Security("is_granted('ROLE_TEACHER')")    
     */
    public function roomUserIndex(Request $request)
    {   
        $account=$this->getUser();
        $userRepository=$this->getDoctrine()->getManager()->getRepository(User::class);
        $user=$userRepository->findOneByAccount($account);
        $roomUsers=$this->getDoctrine()->getManager()->getRepository(RoomUser::class)
        ->findByUser($user);
        return $this->render('roomUsers/indexLite.html.twig',[
            'roomUsers'=>$roomUsers,
        ]);
    }
}
