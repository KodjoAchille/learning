<?php

namespace App\Controller;

use App\Entity\Annexe;
use App\Entity\User;
use App\Entity\Course;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\FileUploader;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/annexes")    
 */
class AnnexeController extends AbstractController
{
    /**
     * @Route("/new", name="annexe_new")    
     */
    public function new(Request $request,FileUploader $fileUploader)
    {           
        if($request->get('id'))
        {    
            $em=$this->getDoctrine()->getManager();
            $course=$em->getRepository(Course::class)
            ->findOneById(htmlspecialchars($request->get('id')));
            $file=$request->files->get('file');
            if($file)
            {
                $annexe= new Annexe();
                $annexe->setTitle(htmlspecialchars($request->get('title')));
                $annexe->setDescription(htmlspecialchars($request->get('description')));
                $fileName = $fileUploader->upload($file);
                $annexe->setFile($fileName);
                $annexe->setCourse($course);
                $em->persist($annexe);
                $em->flush();
            }
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'message'=>'Annexe ajoutée avec success',
            ]]);
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'veuillez préciser l\'id du cours concerné',
            ]]);
        }
    }

    /**
     * @Route("/{id}/newForm", name="annexe_new_form")    
     */
    public function newForm(Request $request,$id)
    {   
        return $this->render('annexes/new.html.twig',[
            'courseId'=>$id,
        ]);
    }

}
