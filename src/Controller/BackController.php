<?php

namespace App\Controller;

use App\Entity\UserCourse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\MailSender;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/courses")    
 */
class BackController extends AbstractController
{    
    /**
     * @Route("/new", name="course_new")
     */
    public function new(Request $request)
    {
        $file=$request->files->get('content');
        if($file)
        {    
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'post'=>$_POST,
                'files'=>$_FILES,
                'the file'=> $request->files->get('content'),
            ]]);                
        }
        return $this->render('courses/new.html.twig');
    }
}
