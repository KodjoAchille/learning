<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Course;
use App\Entity\Room;
use App\Entity\RoomTeacher;
use App\Entity\Account;
use App\Entity\Annexe;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Filesystem\Filesystem;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\MailSender;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin_index")
     */
    public function index(Request $request)
    {
        return $this->render('admin-base.html.twig');
    }
    /**
     * @Route("/course/validate", name="admin_course_validate_or_not")
     */
    public function courseValidateOrNot(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $message="";
        $repository= $em->getRepository(Course::class);
        if($request->get('id'))
        {
            $course=$repository->findOneById($request->get('id'));
            if($course)
            {
                if($course->getValidated()==true)
                {
                    $course->setValidated(false);
                    $message="cours est invalidé avec success";
                }
                else
                {
                    $course->setValidated(true);
                    $message="cours validé avec success";
                }
                $em->flush();
                return new JsonResponse(['data'=>[
                    'statut'=>200,
                    'message'=>$message,
                    'validated'=>$course->getValidated(),
                ]]);
            }
            else
            {
                return new JsonResponse(['data'=>[
                    'statut'=>500,
                    'message'=>'pas d\'entité correspondant au id reçu',
                ]]);    
            }

        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'veuillez précisez l\'id du cours',
            ]]);
        }
    }

    /**
     * @Route("/course/{id}/editForm", name="admin_course_edit_form")
     */
    public function courseEditForm(Request $request, Course $course)
    {
        return $this->render('admin/courses/edit.html.twig',[
            'course'=>$course,
        ]);
    }

    /**
     * @Route("/course/edit", name="admin_course_edit")
     */
    public function courseEdit(Request $request, FileUploader $fileUploader)
    {
        $fs = new Filesystem(); 
        $em=$this->getDoctrine()->getManager();
        $courseRepo=$em->getRepository(Course::class);
        $course=$courseRepo->findOneById($request->get('id'));
        if($course)
        {
            if($request->get('title'))
            {
                $course->setTitle($request->get('title'));
            }
            if($request->get('abstract'))
            {
                $course->setAbstract($request->get('abstract'));
            }
            if($request->get('comment'))
            {
                $course->setComment($request->get('comment'));
            }
            $file=$request->files->get('content');
            if($file!=null)
            {   
                //uploading file content
                $oldContent=$course->getContent();
                $fileName = $fileUploader->upload($file);
                $course->setContent($fileName);
                //let delete the old content
                $fs->remove($this->getParameter('test_directory').'/'.$oldContent);
            }
            $em->persist($course);
            $em->flush();
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'message'=>'le cours  est modifié avec success',
            ]]);
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'Aucun cours ne correpond à l\'id envoyé',
            ]]);
        }
        return $this->render('admin/courses/edit.html.twig',[
            'course'=>$course,
        ]);
    }

    /**
     * @Route("/course/delete", name="admin_course_delete")
     */
    public function courseDelete(Request $request)
    {
        $fs=new Filesystem();
        $em=$this->getDoctrine()->getManager();
        $courseRepo=$em->getRepository(Course::class);
        $annexeRepo=$em->getRepository(Annexe::class);
        $course=$courseRepo->findOneById(htmlspecialchars($request->get('id')));
        if($course)
        {
            $course->setDeleted(true);
            $em->flush();
            $courses=$courseRepo->findAll();
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'message'=>'le cours est supprimé avec sucess',
                'courses'=>$courses,
            ]]);
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'impossible de trouver le cours correspondant à l\'id',
            ]]);
        } 
    }
    /**
     * @Route("/course/list", name="admin_course_list")
     */
    public function courseList(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $repository= $em->getRepository(Course::class);
        $courses=$repository->findAll();
        return $this->render('admin/courses/index.html.twig',[
            'courses'=>$courses,
        ]);            
    }

    /**
     * @Route("/course/{id}/show", name="admin_course_show")    
     */
    public function courseShow(Request $request,Course $course)
    {   
        return $this->render('admin/courses/show.html.twig',[
            'course'=>$course,
        ]);
    }

    /**
     * @Route("/course/list/update", name="admin_course_list_update")
     */
    public function courseListUpdate(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $repository= $em->getRepository(Course::class);
        $courses=$repository->findAll();
        return $this->render('admin/courses/indexLite.html.twig',[
            'courses'=>$courses,
        ]);
    }

    /**
     * @Route("/course/valide", name="admin_course_valide")
     */
    public function courseValide(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(Course::class);
        $courses=$repository->findByValidated(true);        
        return $this->render('admin/courses/valide.html.twig',[
            'courses'=>$courses,
        ]);            
    }

    /**
     * @Route("/course/notvalide", name="admin_course_notvalide")
     */
    public function courseNotvalide(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(Course::class);
        $courses=$repository->findByValidated(false);        
        return $this->render('admin/courses/notvalide.html.twig',[
            'courses'=>$courses,
        ]);            
    }

    /**
     * @Route("/course/{id}/students", name="admin_course_students")
     */
    public function courseStudents(Request $request,Course $course)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(UserCourse::class);
        $userCourses=$repository->findByCourse($course);            
        return $this->render('admin/courses/students.html.twig');
    }

    /**
     * @Route("/user/list", name="admin_user_list")
     */
    public function userList(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(User::class);
        $users=$repository->findAll();
        return $this->render('admin/users/index.html.twig',[
            'users'=>$users,
        ]);

    }

    /**
     * @Route("/user/list", name="admin_user_teacher_list")
     */
    public function userTeacherList(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(User::class);
        $users=$repository->findAll();
        return $this->render('admin/users/teachers.html.twig',[
            'users'=>$users,
        ]);

    }

    /**
     * @Route("/user/list", name="admin_user_student_list")
     */
    public function userStudentList(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(User::class);
        $users=$repository->findAll();
        return $this->render('admin/users/students.html.twig',[
            'users'=>$users,
        ]);

    }
    //gestion des classes de cours
    /**
     * @Route("/room/list", name="admin_room_list")
     */
    public function roomList(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(Room::class);
        $rooms=$repository->findAll();
        return $this->render('admin/rooms/index.html.twig',[
            'rooms'=>$rooms,
        ]);

    }

    /**
     * @Route("/room/list/update", name="admin_room_list_update")
     */
    public function roomListUpdate(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $repository= $em->getRepository(Room::class);
        $rooms=$repository->findAll();
        return $this->render('admin/rooms/indexLite.html.twig',[
            'rooms'=>$rooms,
        ]);
    }
    
    /**
     * @Route("/room/edit", name="admin_room_edit")
     */
    public function roomEdit(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(Room::class);
        $room=$repository->findOneById($request->get('id'));
        if($room)
        {
            $room->setDesignation(htmlspecialchars($request->get('designation')));
            $em->flush();
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'message'=>'modification effectuée avec succes',
            ]]);
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'impossible de trouver la classe correspondante à l\'id',
            ]]);
        }         
    }

    /**
     * @Route("/room/delete", name="admin_room_delete")
     */
    public function roomDelete(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(Room::class);
        $room=$repository->findOneById($request->get('id'));
        if($room)
        {
            $room->setDeleted(true);
            $em->flush();
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'message'=>'modification effectuée avec succes',
            ]]);
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'impossible de trouver la classe correspondante à l\'id',
            ]]);
        }           
    }

    /**
     * @Route("/user/room/list", name="admin_user_room_list")
     */
    public function roomUserList(Request $request)
    {
        $repository=$this->getDoctrine()->getRepository(RoomUser::class);
        $uRepository=$this->getDoctrine()->getRepository(User::class);
        $user=$uRepository->findOneById($request->get('id'));
        $roomUsers=$repository->findByUser($user);
        return $this->render('admin/roomUsers/listByUser.html.twig',[
            'roomUsers'=>$roomUsers,
        ]);
    }
    /**
     * @Route("/room/add", name="admin_room_add")
     */
    public function roomAdd(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository(Room::class);
        $room= new Room();
        $room->setDesignation(htmlspecialchars($request->get('designation')));
        $em->persist($room);
        $em->flush();
        return new JsonResponse(['data'=>[
            'statut'=>200,
            'message'=>'modification effectuée avec succes',
        ]]);
    }

    /**
     * @Route("/user/validate", name="admin_user_validate_or_not")
     */
    public function userValidateOrNot(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $message="";
        $repository= $em->getRepository(User::class);
        if($request->get('id'))
        {
            $user=$repository->findOneById($request->get('id'));
            if($user)
            {
                if($user->getValidated()==true)
                {
                    $user->setValidated(false);
                    $message="utilisateur désactivé avec success";
                }
                else
                {
                    $user->setValidated(true);
                    $message="utilisateur activé avec success";
                }
                $em->flush();
                return new JsonResponse(['data'=>[
                    'statut'=>200,
                    'message'=>$message,
                    'validated'=>$user->getValidated(),
                ]]);
            }
            else
            {
                return new JsonResponse(['data'=>[
                    'statut'=>500,
                    'message'=>'pas d\'entité correspondant au id reçu',
                ]]);    
            }

        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'veuillez précisez l\'id du cours',
            ]]);
        }
    }

    /**
     * @Route("/user/list/update", name="admin_user_list_update")
     */
    public function userListUpdate(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $repository= $em->getRepository(User::class);
        $users=$repository->findAll();
        return $this->render('admin/users/indexLite.html.twig',[
            'users'=>$users,
        ]);
    }
}
