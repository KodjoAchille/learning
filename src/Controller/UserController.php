<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Account;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use App\Entity\Room;
use App\Entity\RoomUser;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\MailSender;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/user")
 * @Security("is_granted('ROLE_USER')")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/new", name="user_new")
     */
    public function new(Request $request)
    {
        //chargement des données depuis la base de donnée
        $em = $this->getDoctrine()->getManager();
        $roomRepository=$em->getRepository(Room::class);
        //création de objet account
        $account=$this->getUser();
        $user=new User();
        if($request->get('name'))
        {
            $user->setName(htmlspecialchars($request->get('name')));
            $user->setFirstName(htmlspecialchars($request->get('firstName')));
            $user->setPhone(htmlspecialchars($request->get('phone')));
            $rooms=$request->get('rooms');
            foreach($rooms as $roomId)
            {
                $room=$roomRepository->findOneById($roomId);
                $roomUser=new RoomUser();
                $roomUser->setUser($user);
                $roomUser->setRoom($room);
                $em->persist($roomUser);
            }
            $account->addRole(htmlspecialchars($request->get('type')));
            $user->setAccount($account);
            $em->persist($user);
            $em->flush();
            //connexion automatique
            $token= new UsernamePasswordToken($account,null,'main',$account->getRoles());
            $this->container->get('security.token_storage')->setToken($token);
            $this->container->get('session')->set('_security_main', serialize($token));
            return $this->redirectToRoute('index');
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'veuillez remplir tous les champs du formulaire',
                //'message'=>'Cet email est déja abonnée au newsletter',
            ]]);
        }
    }


    /**
     * @Route("/show", name="user_show")
     */
    public function show(Request $request)
    {
        $repository=$this->getDoctrine()->getRepository(User::class);
        $account=$this->getUser();
        $user=$repository->findOneByAccount($account);
        if($user)
        {
            dump($user->getRoomUsers());
            return $this->render('user/show.html.twig',[
                'user'=>$user
            ]);
        }
        else
        {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * @Route("/edit", name="user_edit")
     */
    public function edit(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository(User::class);
        $roomRepository=$em->getRepository(Room::class);
        $account=$this->getUser();
        $user=$repository->findOneByAccount($account);
        if($user)
        {
            if($request->get('name'))
            {
                $user->setName(htmlspecialchars($request->get('name')));
            }
            if($request->get('firstName'))
            {
                $user->setFirstName(htmlspecialchars($request->get('firstName')));
            }
            if($request->get('phone'))
            {
                $user->setPhone(htmlspecialchars($request->get('phone')));
            }
            if($request->get('type'))
            {
                $account->addRole(htmlspecialchars($request->get('type')));
            }
            if($request->get('rooms'))
            {
                $rooms=$request->get('rooms');
                foreach($rooms as $roomId)
                {
                    $room=$roomRepository->findOneById($roomId);
                    $roomUser=new RoomUser();
                    $roomUser->setUser($user);
                    $roomUser->setRoom($room);
                    $em->persist($roomUser);
                }
            }
            
            $em->flush();
            return $this->render('user/showLite.html.twig',[
                'user'=>$user
            ]);
        }
        else
        {
            return $this->redirectToRoute('index');
        }
    }

}
