<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\MailSender;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;


class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")    
     */
    public function index(Request $request)
    {   
        return $this->render('base-home.html.twig');
    }
}

