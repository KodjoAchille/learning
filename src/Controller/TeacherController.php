<?php

namespace App\Controller;

use App\Entity\UserCourse;
use App\Entity\CourseRoom;
use App\Entity\RoomUser;
use App\Entity\Course;
use App\Entity\Room;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\FileUploader;

use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/teachers")
 * @Security("is_granted('ROLE_TEACHER')")     
 */
class TeacherController extends AbstractController
{
    /**
     * @Route("/", name="teacher_index")
     */
    public function index(Request $request)
    {
        return $this->render('teacher-base.html.twig');
    }
    /**
     * @Route("room/display/name", name="teacher_room_display_name")    
     */
    public function roomDisplayName(Request $request)
    {   
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository(User::class);
        $roomUserRepository=$em->getRepository(RoomUser::class);
        $account=$this->getUser();
        $user=$repository->findOneByAccount($account);
        $roomUsers=0;
        $roomUsers=$roomUserRepository->findByUser($user);
        return $this->render('teacher/rooms/displayName.html.twig',[
            'roomUsers'=>$roomUsers,
        ]);
        // dump()
    }
    /**
     * @Route("/course/byRoom/{id}", name="teacher_course_by_room")      
     */
    public function courseByRoom(Request $request,Room $room)
    {   
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository(User::class);
        $account=$this->getUser();
        $user=$repository->findOneByAccount($account);
        $courseRoomRepository=$em->getRepository(CourseRoom::Class);
        $courseRooms=$courseRoomRepository->findByRoom($room);
        return $this->render('teacher/courses/byRoom.html.twig',[
            'courseRooms'=>$courseRooms,
            'room'=>$room,
        ]);
    }

    /**
     * @Route("/course/delete", name="teacher_course_delete")
     */
    public function courseDelete(Request $request)
    {
        $fs=new Filesystem();
        $em=$this->getDoctrine()->getManager();
        $courseRepo=$em->getRepository(Course::class);
        $annexeRepo=$em->getRepository(Annexe::class);
        $course=$courseRepo->findOneById(htmlspecialchars($request->get('id')));
        if($course)
        {
            $course->setDeleted(true);
            $em->flush();
            $courses=$courseRepo->findAll();
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'message'=>'le cours est supprimé avec sucess',
                'courses'=>$courses,
            ]]);
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'impossible de trouver le cours correspondant à l\'id',
            ]]);
        } 
    }

    /**
     * @Route("/course/{id}/show", name="teacher_course_show")    
     */
    public function courseShow(Request $request,Course $course)
    {   
        return $this->render('teacher/courses/show.html.twig',[
            'course'=>$course,
        ]);
    }

    /**
     * @Route("/course/{id}/editForm", name="teacher_course_edit_form")
     */
    public function courseEditForm(Request $request, Course $course)
    {
        return $this->render('teacher/courses/edit.html.twig',[
            'course'=>$course,
        ]);
    }

    /**
     * @Route("/course/list/update", name="teacher_course_list_update")
     */
    public function courseListUpdate(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $repository= $em->getRepository(Course::class);
        $courses=$repository->findAll();
        return $this->render('teacher/courses/indexLite.html.twig',[
            'courses'=>$courses,
        ]);
    }

    /**
     * @Route("/room/{id}/user/list", name="teacher_room_user_list")
     */
    public function roomUserlist(Request $request,Room $room)
    {
        $em=$this->getDoctrine()->getManager();
        $repository= $em->getRepository(RoomUser::class);
        $roomUsers=$repository->findByRoom($room);
        return $this->render('teacher/rooms/userList.html.twig',[
            'roomUsers'=>$roomUsers,
            'room'=>$room,
        ]);
    }
}
