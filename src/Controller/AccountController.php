<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Account;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\MailSender;

use Symfony\Component\HttpFoundation\Request;


class AccountController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        //chargement des données depuis la base de donnée
        $em = $this->getDoctrine()->getManager();
        $repository=$em->getRepository(Account::class);
        //création de objet account
        $account = new Account();
        if($request->get('email'))
        {
            $accountEmailExist=$repository->findOneByEmail(htmlspecialchars($request->get('email')));
            if($accountEmailExist)
            {
                return new JsonResponse(['data'=>[
                    'statut'=>500,
                    'message'=>'Email déjà enrégistré',
                ]]);    
            }
            $accountUsernameExist=$repository->findOneByUsername(htmlspecialchars($request->get('username')));
            if($accountUsernameExist)
            {
                return new JsonResponse(['data'=>[
                    'statut'=>500,
                    'message'=>'Nom utilisateur déjà enrégistré',
                ]]);    
            }
            
            $account->setEmail(htmlspecialchars($request->get('email')));
            $account->setUsername(htmlspecialchars($request->get('username')));
            $account->setPassword($encoder->encodePassword($account,htmlspecialchars($request->get('password'))));
            //ajouter le mot de de passe encode
            $em->persist($account);
            $em->flush();
            //connexion automatique
            $token= new UsernamePasswordToken($account,null,'main',$account->getRoles());
            $this->container->get('security.token_storage')->setToken($token);
            $this->container->get('session')->set('_security_main', serialize($token));
            return new JsonResponse(['data'=>[
                'statut'=>200,
                'message'=>'Inscription effectuée avec succes',
            ]]);
            
        }
        else
        {
            return new JsonResponse(['data'=>[
                'statut'=>500,
                'message'=>'veuillez remplir tous les champs du formulaire',
                //'message'=>'Cet email est déja abonnée au newsletter',
            ]]);
            //envoie de message d'erreur
        }
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $utils)
    {
        $session= new Session();
        $em = $this->getDoctrine()->getManager();
        $error = $utils->getLastAuthenticationError();//recuperer l'erreur de la derniere tentative de connexion
        $lastUserName = $utils->getLastUsername();//recuperer l'utilisateur dernierement connecte 
        return $this->render('account/login.html.twig', [
            'error' => $error,
            'lastUserName' => $lastUserName
        ]); 
    }

    // /**
    //  * @Route("/prelogin", name="prelogin")
    //  */
    // public function prelogin(Request $request, AuthenticationUtils $utils)
    // {
    //     $session= new Session();
    //     $em = $this->getDoctrine()->getManager();
    //     $error = $utils->getLastAuthenticationError();//recuperer l'erreur de la derniere tentative de connexion
    //     $lastUserName = $utils->getLastUsername();//recuperer l'utilisateur dernierement connecte 
        
    //     return $this->render('account/prelogin.html.twig', [
    //         'error' => $error,
    //         'lastUserName' => $lastUserName
    //     ]); 
    // }
    
    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
         //la deconnexion s'effectue dans le fichier config/packages/security.yaml
    }


}
